<?php

return [
    'baseUrl' => 'http://devsite.com:3000',
    'production' => false,
    'site_name' => 'Maverick Dental Laboratory',
    'collections' => [
        'products' => [
            'path' => '/products/'
        ],
        'all-ceramic' => [
            'path' => '/products/all-ceramic/'
        ],
        'removables' => [
            'path' => '/products/removables/'
        ],
        'implants' => [
            'path' => '/products/implants/'
        ],
        'services' => [
            'path' => '/products/services/'
        ],
        'metal-based' => [
            'path' => '/products/metal-based/'
        ],
        'sendcase' => [
            'path' => '/send-case/'
        ],
        'contact' => [
            'path' => '/contact/'
        ],
        'blog' => [
            'path' => '/'
        ],
        'about' => [
            'path' => '/about/'
        ]
    ]
];
