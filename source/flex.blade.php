@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Itero Element Flex™ promo',
    'meta_description' => 'Maverick Dental Laboratories is staffed by customer service professionals who will gladly answer any question concerning your case.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'START YOUR DIGITAL JOURNEY'])
    <div class="text-center">
        <p style="color: #ba9959" class="mb-0 pb-0">With a special offer from Align Technology & Maverick Dental Lab </p>
    </div>
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img class="mb-5" src="/img/Hero_Flex_Scannr-banner.png" alt="Itero Element Flex™ promo Page Header">
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <h1 style="color: black;font-style: italic;font-size: 3rem;">SPECIAL OFFER</h1>
                <h2 style="font-size: 3.2rem;">FIRST 15 DIGITAL <br> CROWNS FREE </h2>
                <p class="bold">from Maverick Dental Lab with an iTero Element FlexTM Foundation Scanner <br>
                purchase through Align Technology</p>
                <p>Your choice of: <span style="font-style: italic;">Full Contour Zirconia Crown, IPS e.max or PFM Non-Precious</span></p>
                <p>SIGN UP WITH THE FORM BELOW FOR MORE INFORMATION </p>
            </div>
            <div class="col-12">
                <ul style="padding-left: 0">
                    <li>$17,000 USD, includes 2 -year warranty and no ongoing service fee commitment required* </li>
                    <li>1st 15 crowns free from Maverick Dental Lab </li>
                    <li>Full restorative scanning workflow, including scanning for crown and bridge, implants, partials, nightguards, sleep appliances, and more </li>
                    <li>Tools to enhance patient education and increase case acceptance, including the iTero Viewer tool, Occlusogram, and iTero TimeLapse </li>
                    <li>Open STL file with unlimited downloads for additional restorative and orthodontic needs </li>
                    <li>Seamless integration with your dental lab</li>
                </ul>
                <p><small>*Speak to iTero rep for more details <br>
                    **Laptop sold separately and must meet minimum requirements <br>
                    **Flex Foundation does not have Invisalign connectivity <br>
                    </small></p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-container">
                        <form id="request-supplies-form" action="">
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="doctor-name-suppliesform" placeholder="Doctor's Name" required="required" type="text" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="practice-name-suppliesform" placeholder="Practice Name" required="required" type="text" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="phone-suppliesform" placeholder="Phone Number" required="required" type="text" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="email-suppliesform" placeholder="Email" required="required" type="text" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="address-suppliesform" placeholder="Address" required="required" type="text" />
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-label-group mb-3" style="margin: auto;">
                                        <input class="form-control" id="city-suppliesform" placeholder="City" required="required" type="text" />
                                    </div>
                                </div>
                                <div class="col-lg-2 px-lg-0">
                                    <div class="form-label-group mb-3" style="margin: auto;">
                                        <input class="form-control" id="state-suppliesform" placeholder="State" required="required" type="text" />
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-label-group mb-3" style="margin: auto;">
                                        <input class="form-control" id="zip-suppliesform" placeholder="Zipcode" required="required" type="text" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <textarea id="message-suppliesform" class="form-control" placeholder="Questions or Comments"></textarea>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <span><input type="checkbox" id="requested-supplies" name="requested-supplies" value="I want to purchase the iTero Flex Foundation">&nbsp;I want to purchase the iTero Flex Foundation  </span>  <br>
                                    <span><input type="checkbox" id="requested-supplies" name="requested-supplies" value="I want to schedule a demo">&nbsp;I want to schedule a demo</span> <br>
                                    <span><input type="checkbox" id="requested-supplies" name="requested-supplies" value="I want Align to contact me with more information">&nbsp;I want Align to contact me with more information </span> <br>
                                    <span><input type="checkbox" id="requested-supplies" name="requested-supplies" value="I want Maverick to contact me with more information">&nbsp;I want Maverick to contact me with more information</span>
                                </div>
                            </div>
                            <input type="hidden" id="public_id" value="7804aba8ed8d4374bc9a997ee47ff53e" />
                            <div class="g-recaptcha" data-sitekey="6LcJX6YUAAAAAPZJgl_qtg2bxp1ZVdBHLzqugYYK"></div>
                            <div class="info-btn">
                                <button type="submit">
                                    <div class="info-btn-bx"> @include('_partials.mail-icon')</div>
                                    <div class="info-btn-txt">Submit Now</div>
                                </button>
                            </div>
                        </form>
                        <div class="loader">Loading...</div>
                    </div>
                </div>
            </div>
        </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var formPending = false;
        var checkbox_value = "";
        $('#request-supplies-form').submit(function(event) {
            event.preventDefault();
            
            //Checkbox Values
            $(":checkbox").each(function () {
                var ischecked = $(this).is(":checked");
                if (ischecked) {
                    if (checkbox_value == "") {
                        checkbox_value += $(this).val();
                    } else {
                        checkbox_value += ", " + $(this).val();
                    }
                    
                }
            });
            
            if (formPending)
                return;
            formPending = true;
            $(this).hide();
            $('#request-supplies-form .alert').remove();
            $('.loader').show();
            $.ajax({
                url: 'https://sheikah.amgservers.com/api/contact/7804aba8ed8d4374bc9a997ee47ff53e/a388a2af36de453ea3bda16694832c26',
                method: 'post',
                data: {
                    'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
                    id: $('#public_id').eq(0).val(),
                    practice: $('#practice-name-suppliesform').eq(0).val(),
                    name: $('#doctor-name-suppliesform').eq(0).val(),
                    phone: $('#phone-suppliesform').eq(0).val(),
                    address: $('#address-suppliesform').eq(0).val(),
                    city: $('#city-suppliesform').eq(0).val(),
                    state: $('#state-suppliesform').eq(0).val(),
                    email: $('#email-suppliesform').eq(0).val(),
                    zip: $('#zip-suppliesform').eq(0).val(),
                    interest: checkbox_value,
                    message: $('#message-suppliesform').eq(0).val()
                },
                success: function(data) {
                    $('.loader').hide();
                    $('#request-supplies-form').after('<p>Thanks for your request! We\'ll get in touch with you as soon as possible!</p>');
                },
                error: function(data, status, err) {
                    $('.loader').hide();
                    $('#request-supplies-form').show();
                    formPending = false;
                    $('#request-supplies-form button[type="submit"]').before('<div class="alert alert-danger" role="alert">Please fill out all of the fields</div>');
                }
            });
        });
    });
</script>
@endsection