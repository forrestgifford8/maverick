@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Screw-Retained Crown',
    'meta_description' => 'Maverick has abutments and crowns that are available in a variety of high-quality materials that ensure esthetics and longevity for every case.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Implants'])
</section>
<section id="subNav">
    @include('_partials.implants-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                    <img src="/img/Screw-Retained-Implant-Solution.png" alt="Screw-Retained Crown">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 mt-3">
                <h1>Screw-Retained Crown</h1>
                <p>The screw-retained crowns from Maverick Dental Laboratories are a safe and predictable implant solution. They ensure success for every case by eliminating the primary cause of implant failure, which is excess cement left below the margin during oral surgery. These innovative crowns utilize a screw, instead of wet cement, to be placed during surgery. They are easily retrievable for maintenance and offer a number of clinical benefits for both clinicians and patients. Our screw-retained crowns are fabricated utilizing the latest technology and equipment to have extreme precision and reliable consistency. </p>
                <p class="text-italic">Our screw-retained crowns are available in EZ Anterior Zirconia, PZ Posterior Zirconia, IPS e.max®, and PFM with a stock attachment.</p>
                <h3>Features:</h3>
                <ul>
                    <li>Available with Your Choice of Crown</li>
                    <li>Reduces Implant Failure</li>
                    <li>No Wet Cement in Operatory</li>
                    <li>Easily Retrievable</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $page = window.location.pathname;
        // console.log('Current Page = ' + $page);
        if(!$page) {
            $page = 'index.html';
        }
        $('#productNav-menu div a').each(function(){
            var $href = $(this).attr('href');
            // console.log('URL = ' + $href);
            if ( ($href == $page) || ($href == '') ) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    });
</script>
@endsection