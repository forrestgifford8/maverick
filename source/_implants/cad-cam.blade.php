@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Implant Solutions',
    'meta_description' => 'Maverick has abutments and crowns that are available in a variety of high-quality materials that ensure esthetics and longevity for every case.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Implants'])
</section>
<section id="subNav">
    @include('_partials.implants-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
    <div class="row">
        <div class="col-12">
            <h1>Implant Solutions</h1>
            <p>The Maverick implant department team offers flat-pricing solutions for CAD/CAM cases. Our abutments and crowns are available in a variety of high-quality materials that ensure esthetics and longevity for every case. Our abutments are compatible with numerous implant manufacturers. These abutments provide anatomical emergence profiles, ideal margins, and gingival contours for cement-retained restorations. These flat-priced packages include abutment and screw, soft tissue model with analog, and the crown of your choice. </p>
        </div>
    </div>
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/Custom-Abutment_tiPosterior.png" alt="Implant Solutions">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 d-flex">
                <div class="align-self-center">
                    <h3>CAD/CAM Titanium Abutment & Choice of Crown</h3>
                    <ul>
                        <li>IPS e.max®</li>
                        <li>Zirconia</li>
                        <li>PFM</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/Zr-Abutment.png" alt="CAD/CAM Zirconia Abutment">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 d-flex">
                <div class="align-self-center">
                    <h3>CAD/CAM Zirconia Abutment & Choice of Crown</h3>
                    <ul>
                        <li>IPS e.max®</li>
                        <li>Zirconia</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/Gold-Ti-Abutment.png" alt="CAD/CAM Gold Anodized Titanium Abutment">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 d-flex">
                <div class="align-slef-center">
                    <h3>CAD/CAM Gold Anodized Titanium Abutment & Choice of Crown</h3>
                    <ul>
                        <li>IPS e.max®</li>
                        <li>Zirconia</li>
                        <li>PFM</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $page = window.location.pathname;
        // console.log('Current Page = ' + $page);
        if(!$page) {
            $page = 'index.html';
        }
        $('#productNav-menu div a').each(function(){
            var $href = $(this).attr('href');
            // console.log('URL = ' + $href);
            if ( ($href == $page) || ($href == '') ) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    });
</script>
@endsection