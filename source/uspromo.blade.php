@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Send Cases... Get Diamonds',
    'meta_description' => 'Don’t miss out! This quarter only, Unified Smiles Members get the opportunity to earn FREE high-speed diamonds from Maverick Dental Lab.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Exclusive Unified Smiles Q2 Promotion'])
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <img class="mb-5" style="width: 80%" src="/img/Unified-Smiles-Header.png" alt="Maverick DDS Synergy Header">
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="text-center mb-3">
                    <p>Don’t miss out! This quarter only, Unified Smiles Members get the opportunity to earn FREE high-speed diamonds from MaverickDental Lab.</p>
                    <h3>REGISTER BELOW TO PARTICIPATEAND REQUEST SUPPLIES</h3>
                </div>
                <p><strong>HOW IT WORKS:</strong> Receive a 20% product credit for all lab purchases made with Maverick from April-June 2021, redeemable for German-made high-speed diamonds from Maverick’s Rotary line.</p>

                <p><strong>Example:</strong> Sending $1,000/month in lab work to Maverick Dental Lab this quarter ($3,000 total) would result in a $600 credit for high-speed diamonds.</p>

                <p>To view Maverick’s high-speed diamonds, visit our online Rotary store <u><a href="https://maverickrotary.com/product-category/diamonds/maverick-diamonds/" target="_blank">here</a></u>. </p>
                
                <p>Credits will be distributed in July 2021 to Unified Smiles Members that register and send lab work to Maverick between April-June 2021.</p>
                
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-container">
                    <h4>Fill out the form below to register for the Q2 Unified Smiles promotion:</h4>
                    <form id="local-pickup-form" action="">
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="doctor-name-pickupform" placeholder="Doctor Name" required="required" type="text" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="practice-name-pickupform" placeholder="Practice Name" required="required" type="text" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="address-pickupform" placeholder="Address" required="required" type="text" />
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-label-group mb-3" style="margin: auto;">
                                        <input class="form-control" id="city-pickupform" placeholder="City" required="required" type="text" />
                                    </div>
                                </div>
                                <div class="col-lg-2 px-lg-0">
                                    <div class="form-label-group mb-3" style="margin: auto;">
                                        <input class="form-control" id="state-pickupform" placeholder="State" required="required" type="text" />
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-label-group mb-3" style="margin: auto;">
                                        <input class="form-control" id="zip-pickupform" placeholder="Zipcode" required="required" type="text" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input id="email-contactform" class="form-control" placeholder="Email Address" required="required" type="email" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="phone-pickupform" placeholder="Phone Number" required="required" type="text" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="DI-system-pickupform" placeholder="Digital Impression System" type="text" />
                            </div>
                            <div class="form-label-group mb-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="sendStarter" id="sendStarter" value="Please send me a starter kit" />
                                    <label class="form-check-label" for="sendStarter">
                                        Please send me a starter kit with supplies to send a case
                                    </label>
                                </div>
                            </div>
                            <input type="hidden" id="public_id" value="7804aba8ed8d4374bc9a997ee47ff53e" />
                            <div class="g-recaptcha" data-sitekey="6LcJX6YUAAAAAPZJgl_qtg2bxp1ZVdBHLzqugYYK"></div>
                            <div class="info-btn">
                                <button type="submit">
                                    <div class="info-btn-bx"> @include('_partials.mail-icon')</div>
                                    <div class="info-btn-txt">Submit Now</div>
                                </button>
                            </div>
                        </form>
                    </div>
                <div class="loader">Loading...</div>
            </div>
        </div>
    </div>
</section>
<div id="sub-footer" style="margin-top: 3rem !important">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 text-center">
                <a href="/send-case/new-doctor/" class="btn-white">Send a Case</a>
            </div>
            <div class="col-12 col-md-6 text-center">
                <a href="/send-case/di-submission/" class="btn-white">Digital Dentistry</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#local-pickup-form').submit(function(e) {
            e.preventDefault();
            $(this).hide();
            $('#contactForm .alert').remove();
            $('#local-pickup-form ~ .loader').show();
            $.ajax({
                method: 'POST',
                url: 'https://sheikah.amgservers.com/api/contact/7804aba8ed8d4374bc9a997ee47ff53e/4224704936054f91999137ac11ac4ac1',
                data: {
                    'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
                    id: $('#public_id').eq(0).val(),
                    practice: $('#practice-name-pickupform').eq(0).val(),
                    name: $('#doctor-name-pickupform').eq(0).val(),
                    address: $('#address-pickupform').val(),
                    diSystem: $('#DI-system-pickupform').val(),
                    city: $('#city-pickupform').eq(0).val(),
                    state: $('#state-pickupform').eq(0).val(),
                    zip: $('#zip-pickupform').eq(0).val(),
                    phone: $('#phone-pickupform').eq(0).val(),
                    email: $('#email-contactform').eq(0).val(),
                    sendStarter: $('input[name="sendStarter"]').eq(0).val()
                },
                success: function(data) {
                    $('#local-pickup-form ~ .loader').hide();
                    $('#btn-delivery-next').show();
                    $('#local-pickup-form').after('<p>Thanks for your request, we will be in touch with you shortly.</p>');
                }, 
                error: function() {

                }
            });
        });
    });
</script>
@endsection