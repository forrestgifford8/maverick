@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Full-Service Excellence',
    'meta_description' => 'Never settle for less when you can benefit from the high aesthetics afforded by our expert technicians and state-of-the-art facility.'
    ])
@endsection

@section('header')
<section id="home-header">
    <div id="home-header-video">
        <video class="d-none d-md-block" autoplay="" loop="" muted="" style="min-width: 100%;min-height: 100%;position: absolute;z-index: 0;"> 
            <source src="/video/Maverick-Hompage-Vid-Web.mp4" type="video/mp4">
        </video>
    </div>
    @include('_partials.nav-menu')
    <div id="home-banner" class="container">
        <div class="row">
            <div class="col-12 col-md-10">
                <h2>Full-Service Excellence</h2>
                <h1>Experience the <br> Maverick Difference</h1>
                <p>The Maverick Dental Laboratory team is proud to provide full-service dental lab services to discerning dentists throughout the United States. Never settle for less when you can benefit from the high aesthetics afforded by our expert technicians and state-of-the-art facility. </p>
                <a href="/send-case/new-doctor/" class="btn-white">Get Started</a>
            </div>
        </div>
    </div>
</section>
@endsection

@section('body')
<section id="home-cta">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="home-cta-img" data-aos="fade-up" data-aos-offset="350" data-aos-duration="500">
                    <a href="/products/all-ceramic/">
                        <img class="wrap" src="/img/8365-All-Ceramic-Tall-Thumb.png" alt="All Ceramic">
                        <div class="home-cta-hover">
                            <div>
                                <img src="/img/Triangle-Icon-Green.svg" alt=""><br>
                                <span>All-Ceramic <br> Restorations</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="home-cta-img mt-5rem" data-aos="fade-right" data-aos-offset="150" data-aos-duration="500">
                    <a href="/products/removables/">
                        <img class="wrap" src="/img/8365-Removables-Tall-Thumb.png" alt="Removable">
                        <div class="home-cta-hover">
                            <div>
                                <img src="/img/Triangle-Icon-Green.svg" alt=""><br>
                                <span>Removable <br> Restorations</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="cta-txt text-center d-none d-md-block">
                    <div>
                        <img src="/img/Maverick-Logo-Icon.png" alt="Maverick Dental Lab Icon Logo" >
                    </div>
                    <h1>Providing Superior Restorations <br> Since 2003</h1>
                </div>
                <div class="home-cta-img mt-5rem mb-4" data-aos="fade-up" data-aos-offset="350" data-aos-duration="500">
                    <a href="/products/implants/">
                        <img class="wrap" src="/img/8365-Implants-Tall-Thumb.png" alt="Implant Services">
                        <div class="home-cta-hover">
                            <div>
                                <img src="/img/Triangle-Icon-Green.svg" alt=""><br>
                                <span>Implant <br> Restorations</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="home-cta-img" data-aos="fade-up" data-aos-offset="350" data-aos-duration="500">
                    <a href="/products/metal-based/">
                        <img class="wrap" src="/img/8365-Metal-Based-Tall-Thumb.png" alt="Metal Based">
                        <div class="home-cta-hover">
                            <div>
                                <img src="/img/Triangle-Icon-Green.svg" alt=""><br>
                                <span>Metal-Based <br> Restorations</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="home-cta-img mt-5rem" data-aos="fade-left" data-aos-offset="150" data-aos-duration="500">
                    <a href="/products/services/">
                        <img class="wrap" src="/img/8365-Additional-Services-Tall-Thumb.png" alt="Additional Services">
                        <div class="home-cta-hover">
                            <div>
                                <img src="/img/Triangle-Icon-Green.svg" alt=""><br>
                                <span>Additional <br> Restorations</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="home-send-case" class="parallax-window" data-parallax="scroll" data-image-src="/img/8365-Homepage-LG-Image-Get-Started.png">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <h1>Send Your 1st Case Today!</h1>
                <p>We understand you prioritize the health of your patients and the productivity of your practice. At Maverick Dental Lab, we provide a streamlined case submission process that allows you to spend more time chairside and less time at your computer or shipping cases. Through our send a case feature, you can print Rx forms, generate shipping labels, request a local pickup, view our digital protocols, and more. </p>
                <a href="/send-case/new-doctor/" class="btn-white">Send a Case</a>
            </div>
            <div class="col-12 col-md-6 gold-txt d-none d-sm-block">
                <h1>Get Started <br> with <br> Maverick</h1>
            </div>
        </div>
    </div>
</section>
<section id="home-info">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="info-box mb-4" data-aos="fade-right" data-aos-offset="350" data-aos-duration="500">
                    <img src="/img/8365-VIP-Small-Thumb.png" alt="VIP Services">
                    <h3>VIP Services</h3>
                    <p>The Maverick VIP team utilizes their high expertise to ensure extreme aesthetics and craftsmanship. When you are faced with a patient in need of unparalleled beauty or a solution to a complex case, we invite you to contact our VIP team. </p>
                    <div class="info-btn">
                        <a href="/vip/">
                            <div class="info-btn-bx">@include('_partials.vip-icon')</div>
                            <div class="info-btn-txt">Learn More</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="info-box mb-4" data-aos="fade-up" data-aos-offset="350" data-aos-duration="500">
                    <img src="/img/8365-MVP-Small-Thumb.png" alt="MVP DSO Opportunities">
                    <h3>MVP DSO Opportunities</h3>
                    <p>At Maverick, we understand DSOs are faced with different problems and have broader needs than the typical dental practice. We are happy to be a resource that can help manage the workflow and metrics of a multi-practice DSO. </p>
                    <div class="info-btn">
                        <a href="/dso-services/">
                            <div class="info-btn-bx">@include('_partials.mvp-icon')</div>
                            <div class="info-btn-txt">Learn More</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="info-box mb-4" data-aos="fade-left" data-aos-offset="350" data-aos-duration="500">
                    <img src="/img/8365-Rotary-Instruments-Small-Thumb.png" alt="Rotary Instruments">
                    <h3>Rotary Instruments</h3>
                    <p>Whether you are in need of one-piece operative carbides or zirconia removal diamonds, you will be able to find everything at Maverick Rotary. We are proud to provide you with every resource to fulfill your clinical rotary needs. </p>
                    <div class="info-btn">
                        <a href="https://maverickrotary.com" target="_blank">
                            <div class="info-btn-bx">@include('_partials.rotary-icon')</div>
                            <div class="info-btn-txt">Learn More</div>
                        </a>
                    </div>
                </div>               
            </div>
        </div>
    </div>
</section>
<section id="home-save-25" class="parallax-window" data-parallax="scroll" data-image-src="/img/8365-Homepage-LG-Image-CTA-Bar.png">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6 gold-txt d-none d-sm-block">
                <h1>Save $20 <br> on Your First Case <br> with Maverick!</h1>
            </div>
            <div class="col-sm-12 col-md-6">
                <h2>Partner with Our Trusted Team</h2>
                <p>The Maverick team wants to make the decision to choose our laboratory easy, which is why we will gladly take $20 off your first case. We offer a wide variety of full service products, including full-contour zirconia crowns, full dentures, screw-retained and CAD/CAM abutments, and so much more!</p>
                <a href="/send-case/new-doctor/" class="btn-white">Save $20 Today!</a>
            </div>
        </div>
    </div>
</section>
<section id="home-vid-tour">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 align-self-center">
                <h1>Your Pennsylvania-Based Dental Laboratory</h1>
                <p>Founded by Joe Fey and Larry Albensi in 2003, Maverick Dental Laboratories has been providing dental practices and DSOs with exceptional restorations. Our goal since the beginning has been to build strong and mutually beneficial relationships with clinicians.</p>
                <a href="/about/about-maverick/" class="btn">Learn More</a>
            </div>
            <div class="col-12 col-md-6">
                <iframe src="https://player.vimeo.com/video/92862299" allowfullscreen="allowfullscreen" style="width: 100%;max-width: 640px;height: 337px;" frameborder="0"></iframe>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection