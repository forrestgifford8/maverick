@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Cast Metal Partials',
    'meta_description' => 'IPS e.max® ensures these veneers offer beautiful esthetics, precise fit, and a high flexural strength.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Removables'])
</section>
<section id="subNav">
    @include('_partials.removable-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Cast Metal Partials</h1>
                <p>Maverick Dental Laboratories offers a wide selection of cast partials to fit your patient's oral and economic needs. Cast metal partials are biocompatible, strong, and long-lasting. Your patients will enjoy the comfort and function of these restorations, available in CoCr and Vitallium® alloys.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/WironiumPartial.png" alt="Economy Partials">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 align-self-center">
                <h2>Economy</h2>
                <p>Our economy partial is a sturdy option that your patient can depend on for years. Fabricated out of a high-quality cobalt-chromium alloy, which is biocompatible and superbly strong. </p>
                <h3>Features:</h3>
                <ul>
                    <li>Fabricated Out of CoCr</li>
                    <li>Includes Set-Up, Stock Teeth, and Finish</li>
                    <li>Dependable Strength</li>
                    <li>Economically Priced</li>
                </ul>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/Partial-With-Cast.png" alt="Standard Partial">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 align-self-center">
                <h2>Standard</h2>
                <p>The standard cast partial option from Maverick Dental Laboratories offers increased esthetics and comfort. Crafted out of Vitallium 2000®, these partials are both durable and lightweight for increased flexibility. </p>
                <h3>Features:</h3>
                <ul>
                    <li>Fabricated out of Vitallium 2000® </li>
                    <li>Includes Set-Up, Dentsply TruExpression™ Teeth, and Ivoclar Ivobase® Finish</li>
                    <li>Lightweight Framework</li>
                    <li>Predictable Chair Side Adjustments</li>
                </ul>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/Partial-With-Cast-Full-View.png" alt="Deluxe Partial">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 align-self-center">
                <h2>Deluxe</h2>
                <p>When your patient requires the best cast metal partial available, choose the deluxe option from Maverick. Our removable team fabricates the deluxe partial out of Vitallium 2000 Plus® for unparalleled esthetics and comfort.</p>
                <h3>Features:</h3>
                <ul>
                    <li>Includes Set-Up, Dentsply TruExpression™ Teeth, and Ivoclar Ivobase® Finish</li>
                    <li>Resistant to Deformation and Fracture</li>
                    <li>Adjustable Chair Side</li>
                    <li>Twice the Elongation of Regular Vitallium</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer-Removable')
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $page = window.location.pathname;
        // console.log('Current Page = ' + $page);
        if(!$page) {
            $page = 'index.html';
        }
        $('#productNav-menu div a').each(function(){
            var $href = $(this).attr('href');
            // console.log('URL = ' + $href);
            if ( ($href == $page) || ($href == '') ) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    });
</script>
@endsection