@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Dentures',
    'meta_description' => 'Maverick\'s removable department team utilizes the latest digital advancements in the fabrication of our range of full denture prosthetics.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Removables'])
</section>
<section id="subNav">
    @include('_partials.removable-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Dentures</h1>
                <p>Maverick's removable department team utilizes the latest digital advancements in the fabrication of our range of full denture prosthetics. The use of digital innovation ensures a perfectly fitting denture no matter the price range. We offer economy, standard, and premium dentures that will fulfill your patient's needs and fit in with their budget. In addition, we also offer immediate or transitional dentures to tide your edentulous patient over while they wait for their final prosthetic.</p> 
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/Full-Generic-Denture.png" alt="Economy Denture">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 align-self-center">
                <h2>Economy Denture</h2>
                <p>Our economy denture still meets our high standards for perfect fit and quality craftsmanship. Your patient will be highly satisfied by the lifelike function and comfortable suction afforded by our economy dentures. Through the use of the latest equipment, we can ensure your patient will never suffer from slippage.</p>
                <h3>Features:</h3>
                <ul>
                    <li>Includes Set-Up, Stock Teeth, and Finish</li>
                    <li>Lifelike Function</li>
                    <li>Odor- and Stain-Resistant Acrylic</li>
                    <li>Comfortable Fit</li>
                </ul>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/Avadent_Denture.png" alt="Standard Denture">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 align-self-center">
                <h2>Standard Denture</h2>
                <p>When esthetics are of a higher priority for your patient, our standard dentures offer increased lifelike appearance than their economical counterparts. Our removable technicians are artists in their own right and will skillfully craft a removable prosthetic that is lifelike in both function and appearance.</p>
                <h3>Features:</h3>
                <ul>
                    <li>Includes Set-Up, Dentsply TruExpression™ Teeth, and Ivoclar Ivobase® Finish</li>
                    <li>Increased Esthetics</li>
                    <li>Odor- and Stain-Resistant Acrylic</li>
                    <li>Lifelike Fit and Function</li>
                </ul>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/FullDenture.png" alt="Premium Denture">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 align-self-center">
                <h2>Premium Denture</h2>
                <p>Give your edentulous patients the very best option with our premium denture. Expertly crafted by one of our removable artists, every premium denture is nearly indistinguishable from natural dentition. Your patient's smile impacts their confidence and happiness, which is why they will be ecstatic with the highest quality denture available.</p>
                <h3>Features:</h3>
                <ul>
                    <li>Includes Set-Up and Ivoclar Ivobase® Finish.</li>
                    <li>Characterized with Festooning and Two-Tissue Toning with Color Variations</li>
                    <li>Available with Your Choice of Teeth</li>
                    <li>Unparalleled Fit, Function, and Esthetic</li>
                </ul>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/Economy-Denture.png" alt="Immediate/Transitional Denture">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 align-self-center">
                <h2>Immediate/Transitional Denture</h2>
                <p>Our immediate or transitional denture is an excellent choice for patients in need of a cost-effective and temporary removable prosthetic. We fabricate all our dentures out of high-quality acrylic, which ensures esthetics and durability for the tenure of the restoration.</p> 
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer-Removable')
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $page = window.location.pathname;
        // console.log('Current Page = ' + $page);
        if(!$page) {
            $page = 'index.html';
        }
        $('#productNav-menu div a').each(function(){
            var $href = $(this).attr('href');
            // console.log('URL = ' + $href);
            if ( ($href == $page) || ($href == '') ) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    });
</script>
@endsection