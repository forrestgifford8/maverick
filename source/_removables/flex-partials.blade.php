@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Flexible Partials',
    'meta_description' => 'The flexible partials available from Maverick Dental Laboratories offer superior comfort for partially edentulous patients.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Removables'])
</section>
<section id="subNav">
    @include('_partials.removable-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Flexible Partials</h1>
                <p>The flexible partials available from Maverick Dental Laboratories offer superior comfort for partially edentulous patients. Unlike their stiff, acrylic or cast counterparts, flexible partials rest more comfortably within a patient's mouth and move with the slightly shifting nature of the partially edentulous arch. If you are in search of high-quality flexible partial, we invite you to check out our two options listed below.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/Duraflex-Partial.png" alt="Flexible Partials">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 align-self-center">
                <h2>Valplast®</h2>
                <p>This flexible partial denture is fabricated out of a nylon thermoplastic that allows for unlimited design versatility. In addition to patient-specific esthetics, Valplast® also provide dependable strength and retention. </p>
                <h3>Features:</h3>
                <ul>
                    <li>Metal-Free Design</li>
                    <li>Reduced Chair Time</li>
                    <li>Noninvasive Procedure</li>
                    <li>Unlimited Design Versatility</li>
                </ul>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/Duraflex-Partial-2.png" alt="Duraflex®">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 align-self-center">
                <h2>Duraflex®</h2>
                <p>Duraflex® is crafted out of a polyolefin thermoplastic polymer, which is an excellent alternative for patients allergic to acrylic monomers. Unlike its acrylic and flexible alternatives, Duraflex® is denser and highly resistant to stains and odors.</p>
                <h3>Features:</h3>
                <ul>
                    <li>Stain- and Odor-Resistant</li>
                    <li>Thin, Lightweight, and Flexible</li>
                    <li>Clinically Unbreakable</li>
                    <li>Easily Repaired, Rebased, or Altered with Additional Teeth</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer-Removable')
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $page = window.location.pathname;
        // console.log('Current Page = ' + $page);
        if(!$page) {
            $page = 'index.html';
        }
        $('#productNav-menu div a').each(function(){
            var $href = $(this).attr('href');
            // console.log('URL = ' + $href);
            if ( ($href == $page) || ($href == '') ) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    });
</script>
@endsection