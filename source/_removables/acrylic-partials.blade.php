@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Acrylic Partials',
    'meta_description' => 'The acrylic partials from Maverick Dental Laboratories are expertly fabricated by our removable department technicians.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Removables'])
</section>
<section id="subNav">
    @include('_partials.removable-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/FRS-Partial.png" alt="Acrylic Partials">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 align-self-center">
                <h2>Acrylic Partials</h2>
                <p>The acrylic partials from Maverick Dental Laboratories are expertly fabricated by our removable department technicians. Crafted out of the best material on the market, these acrylic partials provide durability and esthetics for partially edentulous patients. Our technicians pay careful attention to every acrylic partial they craft to ensure lifelike esthetics that will blend in perfectly with your patient's natural dentition.</p>
                <h3>Features:</h3>
                <ul>
                    <li>Lifelike Esthetics</li>
                    <li>Dependable durability</li>
                    <li>Expertly Designed and Crafted</li>
                    <li>Comfortable Fit</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer-Removable')
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $page = window.location.pathname;
        // console.log('Current Page = ' + $page);
        if(!$page) {
            $page = 'index.html';
        }
        $('#productNav-menu div a').each(function(){
            var $href = $(this).attr('href');
            // console.log('URL = ' + $href);
            if ( ($href == $page) || ($href == '') ) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    });
</script>
@endsection