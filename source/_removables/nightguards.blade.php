@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Appliances & Nightguards',
    'meta_description' => 'The acrylic partials from Maverick Dental Laboratories are expertly fabricated by our removable department technicians.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Removables'])
</section>
<section id="subNav">
    @include('_partials.removable-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
    <div class="row">
        <div class="col-12">
            <h1>Appliances & Nightguards</h1>
            <p>Maverick Dental Laboratory is your resource for every restoration your patient may need, including appliances and nightguards. The nightguards available from our team ensure protection of natural dentition, as well as your patients' restorative investments. Fabricated out of high-quality materials, our appliances and nightguards have long-lasting durability to perfectly fit your patient's needs.</p>
        </div>
    </div>
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/NightGuard.png" alt="Night Guard">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 align-self-center">
                <h2>Comfort™ H/S</h2>
                <p>The Comfort™ H/S bite splint is designed with two layers to provide reliable protection throughout the night for patients suffering from bruxism. It alleviates the common issues typically associated with grinding and clenching, including jaw muscle aches and damage to dentition. The outer layer is fabricated out of hard copolyester to ensure durability. The inner layer is a 1 mm soft polyurethane that comfortably cushions teeth and gums for a restful night.</p>
                <h3>Features:</h3>
                <ul>
                    <li>Protects Against Bruxism</li>
                    <li>Available in Blue, Green, Pink, and Clear</li>
                    <li>Crafted Out of Two Layers</li>
                    <li>High Comfort and Durability</li>
                </ul>
                <h2>Additional Appliances and Nightguards</h2>
                <ul>
                    <li>Hard Bite Splint</li>
                    <li>Functional TMJ Splint - Hard</li>
                    <li>Athletic Mouthguard</li>
                    <li>Bleaching Tray</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer-Removable')
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $page = window.location.pathname;
        // console.log('Current Page = ' + $page);
        if(!$page) {
            $page = 'index.html';
        }
        $('#productNav-menu div a').each(function(){
            var $href = $(this).attr('href');
            // console.log('URL = ' + $href);
            if ( ($href == $page) || ($href == '') ) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    });
</script>
@endsection