@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Case Scheduler',
    'meta_description' => 'Maverick Dental Laboratories is happy to provide an easy-to-use case scheduling calendar which will give you the date when you can expect your case to be returned.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Case Scheduler'])
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img class="mb-5" src="/img/8365-Case-Scheduler-Page-Header.png" alt="Case Scheduler Page Header">
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p>Maverick Dental Laboratories is happy to provide an easy-to-use case scheduling calendar which will give you the date when you can expect your case to be returned.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                 <div class="form-container">
                    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                    <form id="caseCal-Form" method="post" name="CalForm" action="">
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="date-calendarform" placeholder="Select a Ship Date" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <select class="form-control" name="product" id="product-calendarform" placeholder="Select a Product">
                                <option value="" style="background-color: #f6f6f6;" disabled selected>Select a Product</option>
                                <option value="10">Crown & Bridge</option>
                                <option value="15">Implant</option>
                                <option value="2">Custom Tray</option>
                                <option value="2">Baseplate & Bite</option>
                                <option value="10">Denture/Partial Set-up</option>
                                <option value="10">Denture/Partial Finish</option>
                                <option value="3">Resets</option>
                                <option value="3">Flipper</option>
                                <option value="2">Repairs</option>
                                <option value="3">Relines</option>
                                <option value="2">Soft Reline</option>
                                <option value="3">Rebases</option>
                                <option value="3">Occlusal Guards cured</option>
                                <option value="3">Proform Nightguard</option>
                                <option value="3">Proform Mouthguard</option>
                                <option value="2">Bleaching Trays</option>
                                <option value="3">Temporary Bridges</option>
                                <option value="10">Framework</option>
                            </select>
                        </div>
                        <div class="info-btn">
                                <button type="submit">
                                    <div class="info-btn-bx" style="padding-top: 5px;padding-bottom: 5px;"> @include('_partials.clock-icon')</div>
                                    <div class="info-btn-txt">Get Turnaround Time</div>
                                </button>
                            </div>
                    </form>
                    <div class="loader">Loading...</div>
                </div> 
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">
      $(document).ready(function() {
        $('#caseCal-Form').submit(function(e) {
            e.preventDefault();
            $('#caseCal-Form ~ .loader').show();
            $('.case-calendar').remove();
            $.ajax({
                method: 'POST',
                url: 'https://sheikah.amgservers.com/api/case-calendar-new',
                data: {
                    date: $('#date-calendarform').eq(0).val(),
                    product: $('#product-calendarform').eq(0).val(),
                    shiptime: "1",
                    processingtime: "0",
                    holidays: '["2018-11-21", "2018-11-23", "2018-12-26", "2018-12-31"]'
                },
                success: function(data) {
                    $('#caseCal-Form ~ .loader').hide();
                    $('#caseCal-Form').parent('.form-container').after(atob(data.calendar));
                    $('#delivery-disclaimer').show();
                }, 
                error: function() {

                }
            });
        });

        $( "#date-calendarform" ).datepicker();
    });
</script>
@endsection