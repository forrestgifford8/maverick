@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Request Supplies',
    'meta_description' => 'Maverick Dental Laboratories is staffed by customer service professionals who will gladly answer any question concerning your case.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Request Supplies'])
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img class="mb-5" src="/img/8365-Request-Supplies-Page-Header.png" alt="Request Supplies Page Header">
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p>The Maverick team is happy to provide a variety of supplies for your practice, including dental lab prescription forms, bio-bags, UPS labels, and case boxes. </p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-container">
                        <form id="request-supplies-form" action="">
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="doctor-name-suppliesform" placeholder="Doctor's Name" required="required" type="text" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="practice-name-suppliesform" placeholder="Practice Name" required="required" type="text" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="phone-suppliesform" placeholder="Phone Number" required="required" type="text" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="address-suppliesform" placeholder="Address" required="required" type="text" />
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-label-group mb-3" style="margin: auto;">
                                        <input class="form-control" id="city-suppliesform" placeholder="City" required="required" type="text" />
                                    </div>
                                </div>
                                <div class="col-lg-2 px-lg-0">
                                    <div class="form-label-group mb-3" style="margin: auto;">
                                        <input class="form-control" id="state-suppliesform" placeholder="State" required="required" type="text" />
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-label-group mb-3" style="margin: auto;">
                                        <input class="form-control" id="zip-suppliesform" placeholder="Zipcode" required="required" type="text" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <span><input type="checkbox" id="requested-supplies" name="requested-supplies" value="Maverick-Starter-Kit">&nbsp;Maverick Starter Kit (includes all supplies listed below) </span> <br>
                                    <span><input type="checkbox" id="requested-supplies" name="requested-supplies" value="Maverick-Rx">&nbsp;Maverick Rx </span>  <br>
                                    <span><input type="checkbox" id="requested-supplies" name="requested-supplies" value="VIP-Rx">&nbsp;VIP Rx </span> <br>
                                    <span><input type="checkbox" id="requested-supplies" name="requested-supplies" value="Maverick-Bio-Bags">&nbsp;Maverick Bio-Bags </span> <br>
                                    <span><input type="checkbox" id="requested-supplies" name="requested-supplies" value="Maverick-UPS-Labels">&nbsp;Maverick UPS Labels </span> <br>
                                    <span><input type="checkbox" id="requested-supplies" name="requested-supplies" value="Maverick-Boxes">&nbsp;Maverick Boxes </span> <br>
                                    <span><input type="checkbox" id="requested-supplies" name="requested-supplies" value="Turnaround-Calendar">&nbsp;Turnaround Calendar </span> <br>
                                    <span><input type="checkbox" id="requested-supplies" name="requested-supplies" value="Price-List">&nbsp;Price List </span> <br>
                                    <span><input type="checkbox" id="requested-supplies" name="requested-supplies" value="VIP-Price-List">&nbsp;VIP Price List </span> <br>
                                </div>
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <textarea id="message-suppliesform" class="form-control" placeholder="Message or list quantity if applicable."></textarea>
                            </div>
                            <input type="hidden" id="public_id" value="7804aba8ed8d4374bc9a997ee47ff53e" />
                            <div class="g-recaptcha" data-sitekey="6LcJX6YUAAAAAPZJgl_qtg2bxp1ZVdBHLzqugYYK"></div>
                            <div class="info-btn">
                                <button type="submit">
                                    <div class="info-btn-bx"> @include('_partials.mail-icon')</div>
                                    <div class="info-btn-txt">Submit Now</div>
                                </button>
                            </div>
                        </form>
                        <div class="loader">Loading...</div>
                    </div>
                </div>
            </div>
        </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var formPending = false;
        var checkbox_value = "";
        $('#request-supplies-form').submit(function(event) {
            event.preventDefault();
            
            //Checkbox Values
            $(":checkbox").each(function () {
                var ischecked = $(this).is(":checked");
                if (ischecked) {
                    if (checkbox_value == "") {
                        checkbox_value += $(this).val();
                    } else {
                        checkbox_value += ", " + $(this).val();
                    }
                    
                }
            });
            
            if (formPending)
                return;
            formPending = true;
            $(this).hide();
            $('#request-supplies-form .alert').remove();
            $('.loader').show();
            $.ajax({
                url: 'https://sheikah.amgservers.com/api/contact/7804aba8ed8d4374bc9a997ee47ff53e/12f41a39f3c94c5cb276c8e6205dc6d7',
                method: 'post',
                data: {
                    'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
                    id: $('#public_id').eq(0).val(),
                    practice: $('#practice-name-suppliesform').eq(0).val(),
                    name: $('#doctor-name-suppliesform').eq(0).val(),
                    phone: $('#phone-suppliesform').eq(0).val(),
                    address: $('#address-suppliesform').eq(0).val(),
                    city: $('#city-suppliesform').eq(0).val(),
                    state: $('#state-suppliesform').eq(0).val(),
                    zip: $('#zip-suppliesform').eq(0).val(),
                    supplies: checkbox_value,
                    message: $('#message-suppliesform').eq(0).val()
                },
                success: function(data) {
                    $('.loader').hide();
                    $('#request-supplies-form').after('<p>Thanks for your request! We\'ll get in touch with you as soon as possible!</p>');
                },
                error: function(data, status, err) {
                    $('.loader').hide();
                    $('#request-supplies-form').show();
                    formPending = false;
                    $('#request-supplies-form button[type="submit"]').before('<div class="alert alert-danger" role="alert">Please fill out all of the fields</div>');
                }
            });
        });
    });
</script>
@endsection