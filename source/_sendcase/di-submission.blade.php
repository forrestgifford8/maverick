@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Send a Digital Case',
    'meta_description' => 'Maverick accepts files from all major intraoral scanners. Please find your preferred scanner in the list below and follow the protocols to get your case to our lab.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Send a Digital Case'])
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img class="mb-5" src="/img/8365-Submit-Digital-Case-Page-Header.png" alt="DI Submission Page Header">
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p>Maverick Dental Laboratories accepts files from all major intraoral scanners. Please find your preferred scanner in the list below and follow the protocols to get your case to our lab.</p>
                <p>Scanner not listed? Please call us at <strong>866-294-7444</strong> or email <u><a href="mailto:info@maverickdental.com">info@maverickdental.com</a></u> for assistance</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div id="accordion">
                    <h3>Dentsply Primescan & Cerec</h3>
                    <div>
                        <ol>
                            <li>Create or Login to your CEREC Connect account</li>
                            <li>Select “MY CEREC CONNECT”</li>
                            <li>Type “Maverick Dental Lab” in the box for Company Name</li>
                            <li>Click FIND</li>
                            <li>Check the box to the bottom left for “Maverick Dental Lab”</li>
                            <li>Click ADD to complete the registration</li>
                            <li>When sending cases, select Maverick Dental Lab</li>
                        </ol>
                    </div>
                    <h3>Trios by 3Shape</h3>
                    <div>
                        <ol>
                            <li>Go to us.3shapecommunicate.com in your web browser</li>
                            <li>Connect with Maverick Dental Lab as Lab by searching impressions@maverickdental.com</li>
                            <li>After connecting to us as a Lab, select Maverick Dental Lab when sending files</li>
                        </ol>
                    </div>
                    <h3>Medit</h3>
                    <div>
                        <ol>
                            <li>Visit https://www.meditlink.com/, login and click on “don’t have an account? sign up”</li>
                            <li>Click on “administrator” under whichever category fits your business</li>
                            <li>Fill in your information, and check your email for the verification email / instructions to download the software</li>
                            <li>Once completed, click “Add New Partner” and search for “Maverick Dental Laboratories” to begin sending cases to our lab</li>
                        </ol>
                    </div>
                    <h3>Itero</h3>
                    <div>
                        <ol>
                            <li>Call 800-577-8767</li>
                            <li>Select Option 1</li>
                            <li>Request that Maverick Dental Lab is added to your scanner and Identify our lab using our phone number: 866-294-7444 or account #8445</li>
                            <li>After Maverick Dental Lab has been added, restart your scanner</li>
                            <li>After connecting to us as a lab, select Maverick Dental Lab on your scanner when sending files</li>
                        </ol>
                    </div>
                    <h3>Carestream</h3>
                    <div>
                        <ol>
                            <li>Select “Find a Lab” option on your scanner</li>
                            <li>Search for either Maverick Dental Lab or impressions@maverickdental.com</li>
                            <li>Add Maverick Dental Lab</li>
                            <li>Select Maverick Dental Lab when submitting scans</li>
                        </ol>
                    </div>
                    <h3>Planmeca®</h3>
                    <div>
                        <ol>
                            <li>Visit ddxdental.com</li>
                            <li>Sign up as a Dental Practice</li>
                            <li>In the “add lab” section, search Maverick Dental Lab and request account</li>  
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection