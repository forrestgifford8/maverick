@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Local Pickup',
    'meta_description' => 'Our delivery drivers pickup and delivery cases from practices within our local area.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Local Pickup'])
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img class="mb-5" src="/img/8365-Local-Pickup-Page-Header.png" alt="Local Pickup Page Header">
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p>Maverick Dental Laboratories provides free pickup and delivery to all clinicians local to our Monroeville, PA location. If you are located in our Maverick delivery driver area, please fill out the request form to schedule a pickup. If you are unsure if your practice is in our Maverick delivery driver area, please call us at 866-294-7444.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-container">
                    <form id="local-pickup-form" action="">
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="doctor-name-pickupform" placeholder="Doctor's Name" required="required" type="text" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="practice-name-pickupform" placeholder="Practice Name" required="required" type="text" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="phone-pickupform" placeholder="Phone Number" required="required" type="text" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="address-pickupform" placeholder="Address" required="required" type="text" />
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-label-group mb-3" style="margin: auto;">
                                        <input class="form-control" id="city-pickupform" placeholder="City" required="required" type="text" />
                                    </div>
                                </div>
                                <div class="col-lg-2 px-lg-0">
                                    <div class="form-label-group mb-3" style="margin: auto;">
                                        <input class="form-control" id="state-pickupform" placeholder="State" required="required" type="text" />
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-label-group mb-3" style="margin: auto;">
                                        <input class="form-control" id="zip-pickupform" placeholder="Zipcode" required="required" type="text" />
                                    </div>
                                </div>
                            </div>
                            {{-- <p>Ready today (Must submit by 7:30 am on day of pick-up. After 7:30 am, please call to request a pick-up)</p> --}}
                            <div class="form-label-group mb-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="ready" id="ready-tomorrow" value="Tomorrow" />
                                    <label class="form-check-label" for="ready-tomorrow">
                                        Ready for pickup tomorrow
                                    </label>
                                </div>
                            </div>
                            <input type="hidden" id="public_id" value="7804aba8ed8d4374bc9a997ee47ff53e" />
                            <div class="g-recaptcha" data-sitekey="6LcJX6YUAAAAAPZJgl_qtg2bxp1ZVdBHLzqugYYK"></div>
                            <div class="info-btn">
                                <button type="submit">
                                    <div class="info-btn-bx"> @include('_partials.mail-icon')</div>
                                    <div class="info-btn-txt">Request Pickup</div>
                                </button>
                            </div>
                        </form>
                        <div class="loader">Loading...</div>
                    </div>
                </div>
            </div>
        </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#local-pickup-form').submit(function(e) {
            e.preventDefault();
            $(this).hide();
            $('#contactForm .alert').remove();
            $('#local-pickup-form ~ .loader').show();
            $.ajax({
                method: 'POST',
                url: 'https://sheikah.amgservers.com/api/contact/7804aba8ed8d4374bc9a997ee47ff53e/5a5b14ebde50426689cc0fced815b3e0',
                data: {
                    'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
                    id: $('#public_id').eq(0).val(),
                    practice: $('#practice-name-pickupform').eq(0).val(),
                    name: $('#doctor-name-pickupform').eq(0).val(),
                    address: $('#address-pickupform').val(),
                    city: $('#city-pickupform').eq(0).val(),
                    state: $('#state-pickupform').eq(0).val(),
                    zip: $('#zip-pickupform').eq(0).val(),
                    phone: $('#phone-pickupform').eq(0).val(),
                    ready: $('input[name="ready"]').eq(0).val()
                },
                success: function(data) {
                    $('#local-pickup-form ~ .loader').hide();
                    $('#btn-delivery-next').show();
                    $('#local-pickup-form').after('<p>Thanks for your request! We\'ll process your pickup shortly.</p>');
                }, 
                error: function() {

                }
            });
        });
    });
</script>
@endsection