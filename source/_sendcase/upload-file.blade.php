@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Upload Case Photos',
    'meta_description' => 'To ensure a streamlined workflow at your practice, Maverick allows fast, easy, and direct to our lab file uploading.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Upload Case Photos'])
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                {{-- <img class="mb-5" src="/img/8365-Upload-File-Page-Header.png" alt="File Uploader Page Header"> --}}
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p>Submit shade match photos and any other collaborative materials related to your case through the drag-and-drop uploader below.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div id="file-upload"><p style="color: red"><small>The file uploader application could not load.<br />Please make sure you're using the latest version of a supported web browser (Chrome, Firefox, Edge, Safari), with javascript enabled.</small></p></div><script type="text/javascript">var loader=function(){var e=document.createElement("script"),t=document.getElementsByTagName("script")[0];e.src="https://goron.amgservers.com/embed/EFB61ECC/uploader.js?v=1.0.4",t.parentNode.insertBefore(e,t)};window.addEventListener?window.addEventListener("load",loader,!1):window.attachEvent("onload",loader);</script>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection