@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'New Doctor',
    'meta_description' => 'Maverick Dental Laboratories is ready to provide you and your patient with a superior dental restoration. Sending us any case is easy with our simple process.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'New Doctor'])
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12 d-none d-md-flex">
                <div class="col">
                    <div id="step-1-icon" class="caseStep active">
                        @include('_partials.caseIcon')
                        <h4>Case Type</h4>
                    </div>
                </div>
                <div class="col">
                    <div id="step-2-icon" class="caseStep">
                        @include('_partials.downloadIcon')
                        <h4>Download</h4>
                    </div>
                </div>
                <div class="col">
                    <div id="step-3-icon" class="caseStep">
                        @include('_partials.deliveryIcon')
                        <h4>Delivery</h4>
                    </div>
                </div>
                <div class="col">
                    <div id="step-4-icon" class="caseStep">
                        @include('_partials.addIcon')
                        <h4>Additional</h4>
<!--
                        @include('_partials.scheduleIcon')
                        <h4>Schedule</h4>
-->
                    </div>
                </div>
                <!-- <div class="col">
                    <div id="step-5-icon" class="caseStep">
                        @include('_partials.addIcon')
                        <h4>Additional</h4>
                    </div>
                </div> --> 
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-12">
                <p>Maverick Dental Laboratories is ready to provide you and your patient with a superior dental restoration. We have made sending us any case easy with our hassle-free and simple send a case process. With streamlined case submission, getting your traditional or digital case to our laboratory takes only a few minutes out of your busy day. </p>
            </div>
        </div> -->
    </div>
</section>
<section class="pt-0">
    <div class="container stepWrap">
        <div class="row">
            <div id="step-1-wrap" class="col-12 text-center">
                <h2 class="mb-5">Choose your Preferred Type of Case</h2>
                <h3>Send a Traditional Case</h3>
                <p>With our step-by-step process, you can get your traditional cases to our laboratory right away. Rx forms, shipping labels, local pickup request, and a case scheduler are all located within the following steps. </p>
                <div id="step-1-btn" class="btn">Get Started</div>
                <h3>Send a Digital Case</h3>
                <p>Maverick is proud to be part of the digital dentistry innovation taking place in the industry today. We accept scans from most major intraoral scanners. </p>
                <p><a href="/send-case/di-submission/" class="btn">Get Started</a></p>
            </div>
            <div id="step-2-wrap" class="col-12 text-center d-none">
                <h2 class="mb-5">Download Rx Form</h2>
                <p>We provide a printable Rx form for all your cases. Simply download the form and include a completed version with your traditional impression. </p>
                <p><a href="/img/Maverick-Combined-RX.pdf" class="btn" target="_blank">Download Rx Form</a><br>
                <a href="/img/Maverick-VIP-RX.pdf" class="btn" target="_blank">Download VIP Rx Form</a></p>
                <div id="step-2-btn" class="btn">Continue&nbsp;&nbsp;&nbsp;<i class="fas fa-arrow-right"></i></div>
            </div>
            <div id="step-3-wrap" class="col-12 text-center d-none">
                <h2 class="mb-5">Choose Your Delivery Method</h2>
                <p>We want to ensure your case gets to our lab quickly and securely, without causing unnecessary stress to you or your team. We provide free local pickup and delivery, as well as shipping labels to any clinicians who fall outside our local zone. Provide us with your Zip Code to see if you qualify.</p>
                <div id="formContainer" class="zipField">
                    <form method="post" name="Form" id="FORM" action="">
                        <input name="zipcode" type="var" placeholder="Enter Zip Code Here" id="zipcode_Field"><br>
                        <input class="btn mt-3" value="Submit" type="submit" style="width: 250px">
                    </form>
                    <div class="placeholder"></div>   
                </div>
                <div id="PickupForm" class="form-container d-none">
                    <h3>Pick Up Form</h3>
                    <form id="local-pickup-form" action="">
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="doctor-name-pickupform" placeholder="Doctor's Name" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="practice-name-pickupform" placeholder="Practice Name" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="phone-pickupform" placeholder="Phone Number" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="address-pickupform" placeholder="Address" required="required" type="text" />
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="city-pickupform" placeholder="City" required="required" type="text" />
                                </div>
                            </div>
                            <div class="col-lg-2 px-lg-0">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="state-pickupform" placeholder="State" required="required" type="text" />
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="zip-pickupform" placeholder="Zipcode" required="required" type="text" />
                                </div>
                            </div>
                        </div>
                        <div class="form-label-group mb-3">
                            <div class="form-check mb-3" style="float: left;">
                                <input class="form-check-input" type="radio" name="ready" id="ready-tomorrow" value="Tomorrow" />
                                <label class="form-check-label" for="ready-tomorrow">
                                    Ready for pickup tomorrow
                                </label>
                            </div>
                        </div>
                        <input type="hidden" id="public_id" value="7804aba8ed8d4374bc9a997ee47ff53e" />
                        <div class="g-recaptcha" data-sitekey="6LcJX6YUAAAAAPZJgl_qtg2bxp1ZVdBHLzqugYYK"></div>
                        <div class="info-btn">
                            <button type="submit">
                                <div class="info-btn-bx"> @include('_partials.mail-icon')</div>
                                <div class="info-btn-txt">Request Pickup</div>
                            </button>
                        </div>
                    </form>
                </div>
                <div id="PrintLabel" class="form-container d-none">
                    <h3>Print UPS Shipping Label Form</h3>
                    <form id="shipping-label-form" action="">
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="doctor-name-labelform" placeholder="Doctor's Name" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="practice-name-labelform" placeholder="Practice Name" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="phone-labelform" placeholder="Phone Number" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="email-labelform" placeholder="Email Address" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="address-labelform" placeholder="Address" required="required" type="text" />
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="city-labelform" placeholder="City" required="required" type="text" />
                                </div>
                            </div>
                            <div class="col-lg-2 px-lg-0">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="state-labelform" placeholder="State" required="required" type="text" maxlength="2" />
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="zip-labelform" placeholder="Zipcode" required="required" type="text" maxlength="5" />
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="service" value="02">
                        <input type="hidden" id="public_id" value="7804aba8ed8d4374bc9a997ee47ff53e" />
                        <div class="info-btn">
                            <button type="submit">
                                <div class="info-btn-bx"> @include('_partials.mail-icon')</div>
                                <div class="info-btn-txt">Create Label</div>
                            </button>
                        </div>
                    </form>
                    <div class="loader">Loading...</div>
                </div>
                <div id="step-4-btn" class="btn d-none">Continue &nbsp;&nbsp;&nbsp;<i class="fas fa-arrow-right"></i></div>
            </div>
            <div id="step-4-wrap" class="col-12 text-center d-none">
            <!--  <h2 class="mb-5">Optional: Schedule Your Case</h2>
                <p>Our team prides ourselves on punctual cases. We know punctuality is a key factor in patient satisfaction and that a late case can greatly interfere with your practice's workflow. We offer a convenient case scheduling calendar, which will calculate when your case will be returned to your practice. Simply provide us with your case information and we will let you know when you can expect your restoration to arrive. </p>
                <div class="form-container">
                    <h3>Schedule Your Case</h3>
                    <p>Please, fill out all necessary information below and discover when your case will be returned to your practice. </p>
                    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                    <form id="caseCal-Form" method="post" name="CalForm" action="">
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="date-calendarform" placeholder="Select a Ship Date" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <select class="form-control" name="product" id="product-calendarform" placeholder="Select a Product">
                                <option value="" style="background-color: #f6f6f6;" disabled selected>Select a Product</option>
                                <option value="10">Crown & Bridge</option>
                                <option value="15">Implant</option>
                                <option value="2">Custom Tray</option>
                                <option value="2">Baseplate & Bite</option>
                                <option value="10">Denture/Partial Set-up</option>
                                <option value="10">Denture/Partial Finish</option>
                                <option value="3">Resets</option>
                                <option value="3">Flipper</option>
                                <option value="2">Repairs</option>
                                <option value="3">Relines</option>
                                <option value="2">Soft Reline</option>
                                <option value="3">Rebases</option>
                                <option value="3">Occlusal Guards cured</option>
                                <option value="3">Proform Nightguard</option>
                                <option value="3">Proform Mouthguard</option>
                                <option value="2">Bleaching Trays</option>
                                <option value="3">Temporary Bridges</option>
                                <option value="10">Framework</option>
                            </select>
                        </div>
                        <div class="info-btn">
                                <button type="submit">
                                    <div class="info-btn-bx" style="padding-top: 5px;padding-bottom: 5px;"> @include('_partials.clock-icon')</div>
                                    <div class="info-btn-txt">Get Turnaround Time</div>
                                </button>
                            </div>
                    </form>
                    <div class="loader">Loading...</div>
                </div>
                <div id="step-5-btn" class="btn">Continue &nbsp;&nbsp;&nbsp;<i class="fas fa-arrow-right"></i></div> -->
                <h2>You're Finished!</h2>
                <p>We hope you found this case submission process easy and stress-free. The next time you send your case to Maverick Dental Laboratories we invite you to pick and choose the resources you need from the Send a Case menu above. There you can print Rx forms, shipping labels, and schedule local pickup anytime you need.</p>
            </div>
<!--        <div id="step-5-wrap" class="col-12 text-center d-none">
                <h2>You're Finished!</h2>
                <p>We hope you found this case submission process easy and stress-free. The next time you send your case to Maverick Dental Laboratories we invite you to pick and choose the resources you need from the Send a Case menu above. There you can print Rx forms, shipping labels, and schedule local pickup anytime you need.</p>
            </div> -->
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function() {
    //Navigation
    $('#step-1-btn').click(function() {
        step_Two();
    });
    $('#step-2-btn').click(function() {
        step_Three();
    });
    $('#step-3-btn').click(function() {
        step_Four();
    });
    $('#step-4-btn').click(function() {
        step_Four();
    });
    $('#step-5-btn').click(function() {
        step_Five();
    });
    
    function step_Two() {
        $('#step-1-icon').removeClass('active');
        $('#step-2-icon').addClass('active');
        $('#step-3-icon').removeClass('active');
        $('#step-1-wrap').addClass('d-none');
        $('#step-2-wrap').removeClass('d-none');
    }
    function step_Three() {
        $('#step-1-icon').removeClass('active');
        $('#step-2-icon').removeClass('active');
        $('#step-3-icon').addClass('active');
        $('#step-1-wrap').addClass('d-none');
        $('#step-2-wrap').addClass('d-none');
        $('#step-3-wrap').removeClass('d-none');
    }
    function step_Four() {
        $('#step-1-icon').removeClass('active');
        $('#step-2-icon').removeClass('active');
        $('#step-3-icon').removeClass('active');
        $('#step-4-icon').addClass('active');
        $('#step-1-wrap').addClass('d-none');
        $('#step-2-wrap').addClass('d-none');
        $('#step-3-wrap').addClass('d-none');
        $('#step-4-wrap').removeClass('d-none');
    }
    function step_Five() {
        $('#step-1-icon').removeClass('active');
        $('#step-2-icon').removeClass('active');
        $('#step-3-icon').removeClass('active');
        $('#step-4-icon').removeClass('active');
        $('#step-5-icon').addClass('active');
        $('#step-1-wrap').addClass('d-none');
        $('#step-2-wrap').addClass('d-none');
        $('#step-3-wrap').addClass('d-none');
        $('#step-4-wrap').addClass('d-none');
        $('#step-5-wrap').removeClass('d-none');
    }

    //Zip Code Checker
    var zipCodes= ['15146','15235','15139','15217','15065','15068','15218','15239','15221','15656','15690','15084','15232','15024','15208','15147','15206','15203','15144','15207','15668','16066','15613','15017','15243','15120','15216','15241','15135','15228','15106','15227','15102','15317','15220','15332','15122','15236','15234','15025','15037','15129','15068','15133','15211','15210','15234','15061','15642','15636','15644','15666','15650','15132','15085','15601','15668','15683','15146','15131','15717','15035','15235','15229','15626','15239','15085','15678','15001','15044','16066','16001','15222','15220','15108','16046','15238','15136','15009','15061','15123','15233','15205','16046','15106','15126','15108','15101','15223','16045','15090','15212','15220','15212','15215','15237','15213','15209','15218','15229','15116','15143','15222','15136','15202','15068','15219','15044','15348','15261'];

    $('#print-label-btn').hide();
    $('#local-pickup-btn').hide();

    function validateForm_Zip(zipcode) {
        if (zipcode==null || zipcode=="" || zipcode.length != 5) {
            window.alert("Please Enter a Valid Zipcode");
            return false;
        } else {
            return true;
        }
    }
    function isInArray(value, array) {
        return array.includes(value);
    }
    $('#FORM').submit(function(e){
        var zipcode=document.forms["Form"]["zipcode"].value;
        e.preventDefault();

        if( validateForm_Zip(zipcode) == false ){
            return;
        }
        if( isInArray(zipcode, zipCodes) == false ){
            $('.placeholder').empty()
            $('.placeholder').append('<div class="row"><div class="col-sm-12"><p style="margin: 15px 0px;">Your practice does not qualify for local pickup and delivery. Instead, you can print a complimentary shipping label, so your case can get to our lab right away. </div></div>')
            $('#PickupForm').addClass('d-none');
            $('#PrintLabel').removeClass('d-none');
            $('#step-4-btn').removeClass('d-none');
        } else {
            $('.placeholder').empty()
            $('.placeholder').append('<div class="row"><div class="col-sm-12"><p style="margin: 15px 0px;">Hello, Neighbor! Your practice qualifies for free pickup and delivery. We invite you to schedule a local pickup by providing us with your practice\'s location. If you would prefer a complimentary shipping label, this resource can be found beneath Send a Case in the menu above.</div></div>')
            $('#PickupForm').removeClass('d-none');
            $('#PrintLabel').addClass('d-none');
            $('#step-4-btn').removeClass('d-none');
        } 
    });

    //Local Pickup Form
    $('#local-pickup-form').submit(function(e) {
        e.preventDefault();
        $(this).hide();
        $('#contactForm .alert').remove();
        $('#local-pickup-form ~ .loader').show();
        $.ajax({
            method: 'POST',
            url: 'https://sheikah.amgservers.com/api/contact/7804aba8ed8d4374bc9a997ee47ff53e/5a5b14ebde50426689cc0fced815b3e0',
            data: {
                'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
                id: $('#public_id').eq(0).val(),
                practice: $('#practice-name-pickupform').eq(0).val(),
                name: $('#doctor-name-pickupform').eq(0).val(),
                address: $('#address-pickupform').val(),
                city: $('#city-pickupform').eq(0).val(),
                state: $('#state-pickupform').eq(0).val(),
                zip: $('#zip-pickupform').eq(0).val(),
                phone: $('#phone-pickupform').eq(0).val(),
                ready: $('input[name="ready"]').eq(0).val()
            },
            success: function(data) {
                $('#local-pickup-form ~ .loader').hide();
                $('#btn-delivery-next').show();
                $('#local-pickup-form').after('<p>Thanks for your request! We\'ll process your pickup shortly.</p>');
            }, 
            error: function() {

            }
        });
    });

    //UPS Label Generator
    $('#shipping-label-form ~ .loader').hide();
    $('#shipping-label-form').submit(function(e) {
        e.preventDefault();
        $(this).hide();
        $('#shipping-label-form ~ .loader').show();
        $('p.form-error').remove();
        $.ajax({
            method: 'POST',
            url: 'https://sheikah.amgservers.com/api/label/create',
            data: {
                id: $('#public_id').eq(0).val(),
                company: $('#practice-name-labelform').eq(0).val(),
                name: $('#doctor-name-labelform').eq(0).val(),
                address: $('#address-labelform').val(),
                city: $('#city-labelform').eq(0).val(),
                state: $('#state-labelform').eq(0).val(),
                zip: $('#zip-labelform').eq(0).val(),
                phone: $('#phone-labelform').eq(0).val(),
                service: $('input[name="service"]').eq(0).val()
            },
            success: function(data) {
                $('#shipping-label-form ~ .loader').hide();
                $('#shipping-label-form').after('<div style="display: flex; justify-content: center;"><a href="https://sheikah.amgservers.com/api/label/get/'+data.id+'.pdf" target="_blank" class="btn btn-primary" download="UPS-Shipping-Label.pdf">Download Shipping Label</a></div><embed src="https://sheikah.amgservers.com/api/label/get/'+data.id+'.pdf" style="width:100%;min-height:400px;" />');
                $('#btn-delivery-next').show();
            }, 
            error: function(data) {
                var error = "An unknown error occured";
                if (data.responseJSON.data.errors) {
                    error = data.responseJSON.data.errors[0].description;
                }
                $('#shipping-label-form ~ .loader').hide();
                $('#shipping-label-form').show();
                $('#shipping-label-form button[type="submit"]').before('<p class="form-error" style="color: #b90f0f;">'+error+'</p>');
            }
        });
    });

    //Case Calendar
    $('#caseCal-Form').submit(function(e) {
        e.preventDefault();
        $('#caseCal-Form ~ .loader').show();
        $('.case-calendar').remove();
        $.ajax({
            method: 'POST',
            url: 'https://sheikah.amgservers.com/api/case-calendar-new',
            data: {
                date: $('#date-calendarform').eq(0).val(),
                product: $('#product-calendarform').eq(0).val(),
                shiptime: "1",
                processingtime: "0",
                holidays: '["2018-11-21", "2018-11-23", "2018-12-26", "2018-12-31"]'
            },
            success: function(data) {
                $('#caseCal-Form ~ .loader').hide();
                $('#caseCal-Form').parent('.form-container').after(atob(data.calendar));
                $('#delivery-disclaimer').show();
            }, 
            error: function() {

            }
        });
    });
    $( "#date-calendarform" ).datepicker();
});
</script>
@endsection