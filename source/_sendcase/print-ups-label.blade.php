@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Shipping Label Generator',
    'meta_description' => 'Please provide all necessary information to generate a complimentary UPS shipping label, which will ensure your case gets to our lab quickly and securely.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Shipping Label Generator'])
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img class="mb-5" src="/img/8365-Local-Pickup-Page-Header.png" alt="Shipping Label Generator Page Header">
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p>Please provide all necessary information to generate a complimentary UPS shipping label, which will ensure your case gets to our lab quickly and securely.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-container">
                    <form id="shipping-label-form" action="">
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="doctor-name-labelform" placeholder="Doctor's Name" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="practice-name-labelform" placeholder="Practice Name" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="phone-labelform" placeholder="Phone Number" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="email-labelform" placeholder="Email Address" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="address-labelform" placeholder="Address" required="required" type="text" />
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="city-labelform" placeholder="City" required="required" type="text" />
                                </div>
                            </div>
                            <div class="col-lg-2 px-lg-0">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="state-labelform" placeholder="State" required="required" type="text" maxlength="2" />
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="zip-labelform" placeholder="Zipcode" required="required" type="text" maxlength="5" />
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="service" value="02">
                        <input type="hidden" id="public_id" value="7804aba8ed8d4374bc9a997ee47ff53e" />
                        <div class="info-btn">
                            <button type="submit">
                                <div class="info-btn-bx"> @include('_partials.mail-icon')</div>
                                <div class="info-btn-txt">Create Label</div>
                            </button>
                        </div>
                </form>
                <div class="loader">Loading...</div>
            </div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function() {
    $('#shipping-label-form ~ .loader').hide();
    $('#shipping-label-form').submit(function(e) {
        e.preventDefault();
        $(this).hide();
        $('#shipping-label-form ~ .loader').show();
        $('p.form-error').remove();
        $.ajax({
            method: 'POST',
            url: 'https://sheikah.amgservers.com/api/label/create',
            data: {
                id: $('#public_id').eq(0).val(),
                company: $('#practice-name-labelform').eq(0).val(),
                name: $('#doctor-name-labelform').eq(0).val(),
                address: $('#address-labelform').val(),
                city: $('#city-labelform').eq(0).val(),
                state: $('#state-labelform').eq(0).val(),
                zip: $('#zip-labelform').eq(0).val(),
                phone: $('#phone-labelform').eq(0).val(),
                service: $('input[name="service"]').eq(0).val()
            },
            success: function(data) {
                $('#shipping-label-form ~ .loader').hide();
                $('#shipping-label-form').after('<div style="display: flex; justify-content: center;"><a href="https://sheikah.amgservers.com/api/label/get/'+data.id+'.pdf" target="_blank" class="btn btn-primary" download="UPS-Shipping-Label.pdf">Download Shipping Label</a></div><embed src="https://sheikah.amgservers.com/api/label/get/'+data.id+'.pdf" style="width:100%;min-height:400px;" />');
                $('#btn-delivery-next').show();
            }, 
            error: function(data) {
                var error = "An unknown error occured";
                if (data.responseJSON.data.errors) {
                    error = data.responseJSON.data.errors[0].description;
                }
                $('#shipping-label-form ~ .loader').hide();
                $('#shipping-label-form').show();
                $('#shipping-label-form button[type="submit"]').before('<p class="form-error" style="color: #b90f0f;">'+error+'</p>');
            }
        });
    });
});
</script>
@endsection