@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Rotary Instruments',
    'meta_description' => 'Maverick proudly offers the finest in German rotary instruments for clinicians and technicians.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Rotary Instruments'])
</section>
@endsection

@section('body')
<section>
    <div class="container text-center">
        <div class="row">
            <div class="col-12">
                <img class="mb-5" src="/img/Rotary-Header-2.png" alt="Rotary Instruments Header">
            </div>
            <div class="col-12">
                <p>Maverick Burs, a division of Maverick Dental Laboratories, proudly offers a robust line of German-made rotary instruments for all your clinical needs. Our extensive selection includes carbides, diamonds, lab cutters, discs, polishers, procedure-specific kits, and more. Sold direct, our combination of quality and below-market pricing represents an extraordinary value. Laboratory customers of Maverick get access to exclusive discounts on all products.</p>
                <a href="https://maverickrotary.com/" class="btn" target="_blank">Shop Now</a>
            </div>
        </div>
    </div>
</section>
<section id="dig-Thumbs" class="pt-1">
   <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-3">
                <a class="prodLink text-center" href="https://maverickburs.com/product-category/carbides/" target="_blank">
                    <div class="prodThumb">
                        <img src="/img/Operative-Carbides-Thumb.png" alt="Operative Carbides">
                    </div>
                    <div class="prodTxt">
                        <h3>Operative Carbides</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <a class="prodLink text-center" href="https://maverickburs.com/product-category/carbides/finishing-carbides/" target="_blank">
                    <div class="prodThumb">
                        <img src="/img/Finishing-Carbides.png" alt="Finishing Carbides">
                    </div>
                    <div class="prodTxt">
                        <h3>Finishing Carbides</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <a class="prodLink text-center" href="https://maverickburs.com/product-category/diamonds/" target="_blank">
                    <div class="prodThumb">
                        <img src="/img/Diamond-Disk-Thumb.png" alt="Diamonds">
                    </div>
                    <div class="prodTxt">
                        <h3>Diamonds</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <a class="prodLink text-center" href="https://maverickburs.com/product-category/carbides/lab-carbides/" target="_blank">
                    <div class="prodThumb">
                        <img src="/img/Lab-Carbides-Thumb.png" alt="Lab Carbides">
                    </div>
                    <div class="prodTxt">
                        <h3>Lab Carbides</h3>
                    </div>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-6 col-md-3 offset-md-2">
                <a class="prodLink text-center" href="https://maverickburs.com/product-category/diamond-discs/" target="_blank">
                    <div class="prodThumb">
                        <img src="/img/Diamond-Disk-Thumb.png" alt="Diamond Discs">
                    </div>
                    <div class="prodTxt">
                        <h3>Diamond Discs</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <a class="prodLink text-center" href="https://maverickburs.com/product-category/polishers/" target="_blank">
                    <div class="prodThumb">
                        <img src="/img/Polishers.png" alt="Polishers">
                    </div>
                    <div class="prodTxt">
                        <h3>Polishers</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <a class="prodLink text-center" href="https://maverickburs.com/product-category/kits/" target="_blank">
                    <div class="prodThumb">
                        <img src="/img/Kits-Thumb.png" alt="Kits">
                    </div>
                    <div class="prodTxt">
                        <h3>Kits</h3>
                    </div>
                </a>
            </div>
        </div>
   </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection