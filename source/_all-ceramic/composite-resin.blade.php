@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Sinfony Composite Resin',
    'meta_description' => 'Sinfony Indirect Lab Composite Resin is a free-flowing composite used in the direct layering technique.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'All-Ceramic'])
</section>
<section id="subNav">
    @include('_partials.allCeramic-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/composit-resin.png" alt="Sinfony Composite Resin">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 align-self-center">
                <h2>Sinfony Composite Resin</h2>
                <p>Maverick Dental Laboratories offers Sinfony Composite Resin. Sinfony Indirect Lab Composite Resin is a free-flowing composite used in the direct layering technique. It is particularly easy to work with and easy to polish because it's a ready-to-use material with high stability. </p>
                <h3>Features:</h3>
                <ul>
                    <li>Free-Flowing</li>
                    <li>Easy to Work With</li>
                    <li>Polishable</li>
                    <li>Ready-to-Use</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer-MB')
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $page = window.location.pathname;
        // console.log('Current Page = ' + $page);
        if(!$page) {
            $page = 'index.html';
        }
        $('#productNav-menu div a').each(function(){
            var $href = $(this).attr('href');
            // console.log('URL = ' + $href);
            if ( ($href == $page) || ($href == '') ) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    });
</script>
@endsection