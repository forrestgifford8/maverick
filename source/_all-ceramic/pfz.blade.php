@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Porcelain-Fused-to-Zirconia',
    'meta_description' => 'When your patient is in search of superior esthetics for the anterior, Maverick offers lifelike porcelain-fused-to-zirconia restorations.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'All-Ceramic'])
</section>
<section id="subNav">
    @include('_partials.allCeramic-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/PFZ.png" alt="Porcelain-Fused-to-Zirconia">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 align-self-center">
                <h2>Porcelain-Fused-to-Zirconia</h2>
                <p>When your patient is in search of superior esthetics for the anterior, Maverick offers lifelike porcelain-fused-to-zirconia restorations. PFZ restorations are an excellent alternative to PFM crowns. Crafted by layering esthetic porcelain over a zirconia substructure, they provide the strength of PFM crowns while eliminating esthetic challenges, such as discoloration or the dark line that can appear along the gingiva. They are also a good option for ceramic veneers. Through the use of advanced CAD/CAM technology, they provide consistently accurate fit. Due to zirconia's natural translucency, these crowns are warmer and provide superior lifelike coloration when compared to their PFM counterparts.</p>
                <h3>Features:</h3>
                <ul>
                    <li>Hypoallergenic & Biocompatible</li>
                    <li>No Esthetics Challenges Like PFMs</li>
                    <li>Unparalleled Substructure Strength</li>
                    <li>CAD/CAM Fabricated</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div id="accordion">
                    <h3>Indications</h3>
                        <div>
                            <p>A CAD/CAM substitute for traditional PFM, our porcelain-fused-to-zirconia can be used for anterior and posterior crowns, crowns over implants, and bridges of up to fourteen units.</p>
                        </div>
                        <h3>Contraindications</h3>
                        <div>
                            <ul>
                                <li>Attachment cases</li>
                                <li>Cases with less than 1 mm clearance</li>
                                <li>Bruxism</li>
                                <li>Patients who have broken a PFM crown</li>
                                <li>Cases that require bonding</li>
                            </ul>
                        </div>
                        <h3>Preparation</h3>
                        <div>
                            <p>The ideal preparation for PFZs is a chamfer margin preparation. If a porcelain labial margin is prescribed, then a shoulder margin preparation is required.</p>
                        </div>
                        <h3>Cementation</h3>
                        <div>
                            <ul>
                                <li>Resin Ionomer cement (RelyX or RelyX Unicem, 3M ESPE)</li>
                                <li>Maxcem Elite (Kerr)</li>
                                <li>Panavia F 2.0 (Kuraray) - ideal for short, tapered preparations</li>
                                <li>Glass ionomer cement (GC Fuji, GC America)</li>
                            </ul>
                        </div>
                        <h3>Tech Notes</h3>
                        <div>
                            <p>If an adjustment is required on the ceramic, use a fine diamond with water and air to keep the crown cool. To contour the ceramic, polish with a pink rubber wheel and diamond polishing paste (Maverick Rotary).</p>
                        </div>
                        <h3>Codes</h3>
                        <div>
                            <ul>
                                <li>D2740 Crown - porcelain / ceramic substrate</li>
                            </ul>
                        </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer-MB')
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $page = window.location.pathname;
        // console.log('Current Page = ' + $page);
        if(!$page) {
            $page = 'index.html';
        }
        $('#productNav-menu div a').each(function(){
            var $href = $(this).attr('href');
            // console.log('URL = ' + $href);
            if ( ($href == $page) || ($href == '') ) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    });
</script>
@endsection