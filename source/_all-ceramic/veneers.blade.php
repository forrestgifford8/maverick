@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Veneers',
    'meta_description' => 'IPS e.max® ensures these veneers offer beautiful esthetics, precise fit, and a high flexural strength.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'All-Ceramic'])
</section>
<section id="subNav">
    @include('_partials.allCeramic-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/Veneer.png" alt="Veneers">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 align-self-center">
                <h2>Veneers</h2>
                <p>The veneers fabricated at Maverick Dental Laboratories are made of IPS e.max®. IPS e.max® ensures these veneers offer beautiful esthetics, precise fit, and a high flexural strength. Virtually fracture-resistant, lithium disilicate veneers are long-lasting. Veneers are a non-invasive method to rectify common cosmetic issues, including chipped, gapped, or stained teeth.</p>
                <h3>Features:</h3>
                <ul>
                    <li>Non-Invasive</li>
                    <li>Fabricated Out of IPS e.max®</li>
                    <li>Fracture-Resistant</li>
                    <li>Beautiful Esthetics</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div id="accordion">
                    <h3>Indications</h3>
                    <div>
                        <p>Changed color of anterior teeth, Incorrect shape of tooth or position in dental arch, Enamel defects such as enamel hypoplasia, attrition of teeth as a consequence of trauma, wide interproximal spaces like diastema.</p>
                    </div>
                    <h3>Contraindications</h3>
                    <div>
                        <p>Bruxism and parafunction, Pathology of bite, more than 50% of enamel affected by pathology.</p>
                    </div>
                    <h3>Tech Notes</h3>
                    <div>
                        <p>For facial reduction three wheel diamond depth cutter should be used for orientation grooves. For proximal reduction - round end tapered diamond bur is used as an extension for facial reduction.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer-MB')
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $page = window.location.pathname;
        // console.log('Current Page = ' + $page);
        if(!$page) {
            $page = 'index.html';
        }
        $('#productNav-menu div a').each(function(){
            var $href = $(this).attr('href');
            // console.log('URL = ' + $href);
            if ( ($href == $page) || ($href == '') ) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    });
</script>
@endsection