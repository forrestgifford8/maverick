@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'EZ Esthetic Anterior Zirconia',
    'meta_description' => 'The EZ Esthetic Anterior Zirconia from Maverick Dental Laboratories is ideal for anterior units, including up to 3-unit anterior or posterior bridges, and in some cases veneers.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'All-Ceramic'])
</section>
<section id="subNav">
    @include('_partials.allCeramic-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                    <img src="/img/EZ_Anterior-Single.png" alt="EZ Esthetic Anterior Zirconia">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 align-self-center">
                <h2>EZ Esthetic Anterior Zirconia</h2>
                <p>The EZ Esthetic Anterior Zirconia from Maverick Dental Laboratories is ideal for anterior units, including up to 3-unit anterior or posterior bridges, and in some cases veneers. The Maverick fixed department team utilizes the highest quality, most esthetic zirconia available to ensure your team receives superior results for every case. The zirconia we use not only has the expected high strength of monolithic zirconia, but it also perfectly mimics the appearance of natural tooth enamel, dentin, and cervical shades. It will seamlessly blend in with your patient's surrounding dentition thanks to its natural shade progression, which will eliminate any unflattering shade edges. </p>
                <h3>Features:</h3>
                <ul>
                    <li>650 MPa Flexural Strength</li>
                    <li>Translucency up to 49%</li>
                    <li>Eliminates Propagation of Cracks</li>
                    <li>Multilayered Esthetics</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div id="accordion">
                    <h3>Indications</h3>
                    <div>
                        <p>Full-contour zirconia is so versatile, it can be used in almost any situation from singles, bridges with any combination of abutments and pontics, inlay bridges and screw-retained implants. Also an esthetic alternative to a PFM with metal occlusion due to limited space.</p>
                    </div>
                    <h3>Contraindications</h3>
                    <div>
                        <p>When esthetic expectations are high and it is important that the restorations match surrounding natural dentition or other existing restorations. If bonding is necessary to retain the restoration, bond strength is weaker and less predictable than other ceramics.</p>
                    </div>
                    <h3>Preparation</h3>
                    <div>
                        <p>Shoulder preparation not needed. A mild chamfer or a feather-edge margin is good. 1mm buccal, lingual, and occlusal reduction is ideal, but can go to .5mm in some areas when reduction is limited. Minimum occlusal reduction of 0.5 mm; 1 mm is ideal. Adjustments and polishing: Adjust full-contour zirconia crowns and bridges using water and air spray to keep the restoration cool and to avoid microfractures with a fine grit diamond. If using air only, use the lightest touch possible when making adjustments. A football-shaped bur is the most effective for occlusal and lingual surfaces (on anterior teeth); a tapered bur is the ideal choice for buccal and lingual surfaces. Polish full-contour zirconia restorations with the porcelain polishing system of your choice.</p>
                    </div>
                    <h3>Cementation</h3>
                    <div>
                        <p>It is recommended that full-contour zirconia be cemented using a zirconia primer like Z-Prime from Bisco or Clearfil Ceramic Primer from Kuraray. Alternatively, a resin reinforced glass ionomer such as RelyX Luting cement can also be used. When a greater bond is needed due to the lack of a retentive preparation, use a resin cement like RelyX Unicam or RelyX Ultimate. Before cementing all full-contour zirconia crowns, the interior surface of the crown needs to be cleaned with Ivoclean (Ivoclar Vivadent - Amherst, NY). This is critical in assuring maximum bond strength.</p>
                    </div>
                    <h3>Tech Notes</h3>
                    <div>
                        <p>Solid zirconia requires a cast gold type preparation. If adjustments are needed, use zirconia specific diamonds and rubber wheels polishing with diamond paste.</p>
                    </div>
                    <h3>Codes</h3>
                    <div>
                        <ul>
                            <li>D2740 Crown – Porcelain/Ceramic Substrate</li>
                            <li>D6245 Pontic Porcelain/Ceramic</li>
                            <li>D6740 Abutment Crown Porcelain/Ceramic</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer-MB')
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $page = window.location.pathname;
        // console.log('Current Page = ' + $page);
        if(!$page) {
            $page = 'index.html';
        }
        $('#productNav-menu div a').each(function(){
            var $href = $(this).attr('href');
            // console.log('URL = ' + $href);
            if ( ($href == $page) || ($href == '') ) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    });
</script>
@endsection