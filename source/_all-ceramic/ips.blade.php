@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'IPS e.max®',
    'meta_description' => 'Whether crafted as a full-contour monolithic or layered with porcelain, IPS e.max® offers the exact same esthetics and strength it has come to be known for.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'All-Ceramic'])
</section>
<section id="subNav">
    @include('_partials.allCeramic-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/IPS-emax-Anterior.png" alt="IPS e.max®">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 align-self-center">
                <h2>IPS e.max®</h2>
                <p>Maverick Dental Laboratories is proud to offer IPS e.max®, a remarkable lithium disilicate restoration. Lithium disilicate has a unique crystalline structure, which gives this all-ceramic unbelievable esthetics and superior, monolithic strength. It is a customizable solution that can easily work for any patient. Whether it is crafted as a full-contour monolithic or cut-back and layered with porcelain, IPS e.max® offers the exact same esthetics and strength it has come to be known for. IPS e.max® has three times the strength of alternative all-ceramic options. It is virtually fracture-resistant and has four levels of translucency. </p>
                <h3>Features:</h3>
                <ul>
                    <li>4 Levels of Translucency</li>
                    <li>Fracture-Resistant</li>
                    <li>500 MPa Flexural Strength</li>
                    <li>Highly Customizable</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div id="accordion">
                    <h3>Indications</h3>
                    <div>
                        <p>IPS e.max is the premium lithium disilicate, glass-ceramic restoration. Combining lifelike materials and fracture resistant properties, IPS e.max is as durable as it is lifelike. The flexibility of IPS e.max makes it an excellent restorative method for anterior esthetics or posterior function.</p>
                    </div>
                    <h3>Contraindications</h3>
                    <div>
                        <p>Bridges which include molars, Maryland style bridges, and bridges which have a short vertical height that does not allow for adequate connector height.</p>
                    </div>
                    <h3>Preparation</h3>
                    <div>
                        <p>Anterior full-coverage crowns require a chamfer or shoulder margin. A circular shoulder is prepared with rounded inner edges or a chamfer at an angle of 10-30°: the width of the shoulder/chamfer is approx. 1 mm. Facial reduction is 1.5 – 2 mm; 1 – 1.5 mm lingual contact clearance. Incisal reduction is 1.5 – 2 mm with rounded internal line angles, and an incisal edge at least 1mm wide to permit optimum milling of the incisal edge during CAD/CAM processing.</p>

                        <p>Posterior full-coverage crown requires a chamfer or shoulder margin. A circular shoulder is prepared with rounded inner edges or a chamfer at an angle of 10-30°: the width of the shoulder/chamfer is approx. 1 mm. Occlusal reduction is 1.5 – 2 mm: axial reduction (buccal, lingual, and interproximal) is 1.5 mm with rounded internal line angles.</p>
                    </div>
                    <h3>Cementation</h3>
                    <div>
                        <p>IPS e.max layered – can be cemented using a resin reinforced glass ionimer such as RelyX Luting cement. Or bonded using a resin cement when extra strength is needed due to lack of retention on the prep, use a resin cement such as RelyX Unicem or RelyX Ultimate.</p>
                    </div>
                    <h3>Tech Notes</h3>
                    <div>
                        <p>If adjustments are needed, use fine diamonds with water and light pressure. Always remove the crown when adjusting or bond/cement crown before adjustments are made.</p>
                    </div>
                    <h3>Codes</h3>
                    <div>
                        <ul>
                            <li>D2740 Crown</li>
                            <li>D2610 Inlay for 1 surface</li>
                            <li>D2620 Inlay for 2 surfaces</li>
                            <li>D2630 Inlay for 3 surfaces</li>
                            <li>D2962 Labial Veneer</li>
                            <li>D2783 Crown 3/4 Porcelain Ceramic (does not include veneers)</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer-MB')
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $page = window.location.pathname;
        // console.log('Current Page = ' + $page);
        if(!$page) {
            $page = 'index.html';
        }
        $('#productNav-menu div a').each(function(){
            var $href = $(this).attr('href');
            // console.log('URL = ' + $href);
            if ( ($href == $page) || ($href == '') ) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    });
</script>
@endsection