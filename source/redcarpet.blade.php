@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Red Carpet Promotion',
    'meta_description' => 'Receive the average net spend of two consecutive months as a credit toward the third month’s statement!'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Red Carpet Promotion'])
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p>Receive the average net spend of two consecutive months as a credit toward the third month’s statement!</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-5">
               <h3>Rules and Restrictions:</h3>
                <ul>
                    <li>Registration Required</li>
                    <li>By invitation only – Exclusive to new Smile Source Members</li>
                    <li>Credit applies to one month’s invoice only – cannot be carried forward</li>
                    <li>Alloy, Shipping, Implant Parts excluded</li>
                    <li>Maverick VIP Studio included</li>
                </ul>
                <img src="/img/Red-Carpet-Promotion.jpg" alt="Red Carpent Thumbnail">
            </div>
            <div class="col-12 col-md-7">
               <h3>Registration Form</h3>
                <div class="form-container">
                    <form id="redCarpetForm" action="">
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="doctor-name-pickupform" placeholder="Doctor's Name" required="required" type="text" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="phone-pickupform" placeholder="Phone Number" required="required" type="text" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="address-pickupform" placeholder="Address" required="required" type="text" />
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-label-group mb-3" style="margin: auto;">
                                        <input class="form-control" id="city-pickupform" placeholder="City" required="required" type="text" />
                                    </div>
                                </div>
                                <div class="col-lg-2 px-lg-0">
                                    <div class="form-label-group mb-3" style="margin: auto;">
                                        <input class="form-control" id="state-pickupform" placeholder="State" required="required" type="text" />
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-label-group mb-3" style="margin: auto;">
                                        <input class="form-control" id="zip-pickupform" placeholder="Zipcode" required="required" type="text" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                   <div class="form-label-group mb-3" style="margin: auto;">
                                       <p>Select Your Start Month:</p>
                                        <select name="startMonth" id="startMonth" class="form-control"  placeholder="Select Your Start Month" required="required">
                                            <option value="" selected="selected" class="gf_placeholder">—</option><option value="January">January</option><option value="February">February</option><option value="March">March</option><option value="April">April</option><option value="May">May</option><option value="June">June</option><option value="July">July</option><option value="August">August</option><option value="September">September</option><option value="October">October</option><option value="November">November</option><option value="December">December</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <textarea id="message-redCarpetForm" class="form-control" placeholder="Questions or Comments:"></textarea>
                            </div>
                            <input type="hidden" id="public_id" value="7804aba8ed8d4374bc9a997ee47ff53e" />
                            <div class="g-recaptcha" data-sitekey="6LcJX6YUAAAAAPZJgl_qtg2bxp1ZVdBHLzqugYYK"></div>
                            <div class="info-btn">
                                <button type="submit">
                                    <div class="info-btn-bx"> @include('_partials.mail-icon')</div>
                                    <div class="info-btn-txt">Register for Promotion!</div>
                                </button>
                            </div>
                        </form>
                        <div class="loader">Loading...</div>
                    </div>
                </div>
            </div>
        </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#redCarpetForm').submit(function(e) {
            e.preventDefault();
            $(this).hide();
            $('#redCarpetForm .alert').remove();
            $('#redCarpetForm ~ .loader').show();
            $.ajax({
                method: 'POST',
                url: 'https://sheikah.amgservers.com/api/contact/7804aba8ed8d4374bc9a997ee47ff53e/afd0d3371480430db4ea8839aff9a9b2',
                data: {
                    'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
                    id: $('#public_id').eq(0).val(),
                    practice: $('#practice-name-pickupform').eq(0).val(),
                    name: $('#doctor-name-pickupform').eq(0).val(),
                    address: $('#address-pickupform').val(),
                    city: $('#city-pickupform').eq(0).val(),
                    state: $('#state-pickupform').eq(0).val(),
                    zip: $('#zip-pickupform').eq(0).val(),
                    phone: $('#phone-pickupform').eq(0).val(),
                    startMonth: $('#startMonth').eq(0).val(),
                    message: $('#message-redCarpetForm').eq(0).val()
                },
                success: function(data) {
                    $('#redCarpetForm ~ .loader').hide();
                    $('#btn-delivery-next').show();
                    $('#redCarpetForm').after('<p>Thanks for your request! We\'ll process your application shortly.</p>');
                }, 
                error: function() {

                }
            });
        });
    });
</script>
@endsection