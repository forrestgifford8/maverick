@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Full-Service Excellence',
    'meta_description' => 'Never settle for less when you can benefit from the high aesthetics afforded by our expert technicians and state-of-the-art facility.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
@endsection

@section('body')
<div id="home-header-video" class="d-flex">
    <video class="d-none d-sm-block align-self-center" autoplay="" loop="" muted="" playsinline=""> 
        <source src="/video/Maverick-Home-Vid-Compressed.mp4" type="video/mp4">
    </video>
    <div id="header-mobile" class="d-block d-sm-none"></div>
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex">
                <div class="d-none d-sm-block text-wrap text-center">
                    <h2>THE RIGHT TIME <br> THE RIGHT LAB</h2>
                </div>
                <div class="d-block d-sm-none text-wrap-mobile text-center">
                    <h2>THE RIGHT TIME <br> THE RIGHT LAB</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="world-class">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h1>World-Class Restorations</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-4">
                <a class="home-CTA-Link" href="/products/metal-based/">
                    <div class="thumb">
                        <img src="/img/Fixed-Home-Thumb.png" alt="Fixed Restorations">
                        <div class="overlay">
                            <div class="align-self-center w-100">Fixed</div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-12 col-lg-4">
                <a class="home-CTA-Link" href="/products/removables/">
                    <div class="thumb">
                        <img src="/img/Removable-Home-Thumb.png" alt="Removables">
                        <div class="overlay">
                            <div class="align-self-center w-100">Removable</div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-12 col-lg-4">
                <a class="home-CTA-Link" href="/products/implants/">
                    <div class="thumb">
                        <img src="/img/Implants-Home-Thumb.png" alt="Implants">
                        <div class="overlay">
                            <div class="align-self-center w-100">Implant</div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <a href="/send-case/new-doctor/" class="btn">Send a Case</a>
            </div>
        </div>
    </div>
</div>
<div id="our-value">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6 offset-sm-3">
                <h1>Our Value</h1>
                <p>As a privately owned, mid-size dental <br>
                laboratory, we are built to serve a <br> 
                national clientele by keeping promises <br>
                and holding tight to our core values. First <br> 
                in class facilities, technology, service and <br> 
                talent allow us to move mountains so <br>
                that you can be a hero for your patients.</p>
                <a href="/about/about-maverick/" class="btn">Read More</a>
            </div>
        </div>
    </div>
</div>
<div id="digital-cta">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6">
                <h1>Digital Done Right</h1>
                <p>Maximize the results of your IOS investment <br> with a lab partner that will take your scans and create restorations exactly to your specifications, each and every time.</p>
                <a href="/about/digital-dentistry/" class="btn">Learn More</a><a href="/send-case/new-doctor/" class="btn">Send a Case</a>
            </div>
        </div>
    </div>
</div>
<div id="full-spectrum">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h1>A FULL SPECTRUM</h1>
                <h2>of Products, Services and Capabilities</h2>
            </div>
        </div>
        <div class="mt-5 home-grid">
            <div>
                <a href="/about/about-maverick/" class="home-cta-icons">
                    <img src="/img/8365-Maverick-Small-Logo-Thumb.png" alt="Full Service Lab">
                    <h3>FULL SERVICE LABORATORY</h3>
                    <p>With Strong Fixed, Removable &amp; Implant capabilities to handle the majority of <br> your cases</p>
                </a>
            </div>
            <div>
                <a href="/vip/" class="home-cta-icons">
                    <img src="/img/8365-VIP-Studio-Small-Logo-Thumb.png" alt="VIP Studio">
                    <h3>BOUTIQUE <br> SERVICES</h3>
                    <p>For highly aesthetic or complex reconstructive cases requiring the most talented and experienced hands</p>
                </a>
            </div>
            <div>
                <a href="/dso-services/" class="home-cta-icons">
                    <img src="/img/8365-MVP-DSO-Services-Small-Logo-Thumb.png" alt="DSO Services">
                    <h3>MOST VALUABLE PARTNER</h3>
                    <p>Serving the specific <br> needs of the DSO <br> &amp; Group Practice segment of the <br>dental market</p>
                </a>
            </div>
            <div>
                <a href="/rotary-instruments/" class="home-cta-icons">
                    <img src="/img/8365-Rotary-Small-Logo-Thumb.png" alt="Rotary Instruments">
                    <h3>ROTARY INSTRUMENTS</h3>
                    <p>A complete line of German-made Carbides, Diamonds, Polishers and related products for clinical and laboratory use</p>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" style="top: 25%">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Maverick Phone Notice</h3>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
				<p>Global hacking and ransoms of companies are becoming more prevalent every day. Recently our phone provider (one of the largest in the world) experienced an attack that caused phone issues on a global scale.</p>
                <p>In the event you cannot reach us with our toll free number, please contact us on this non-VoIP number as a backup: <a href="tel:724-396-1750">724-396-1750</a> or email: <a href="mailto:lynn@maverickdental.com">lynn@maverickdental.com</a>.</p>
                <p>Thank you for your patience and understanding.</p>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
{{-- <script type="text/javascript">
    $(document).ready(function(){
		$("#myModal").modal('show');
	});
</script> --}}
@endsection