@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'VIP Studio',
    'meta_description' => 'Maverick Dental Laboratories offers VIP Services to all our clients who are faced with a case requiring unparalleled esthetics and attention.  '
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
@endsection

@section('body')
<section id="vip-header">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <img src="/img/VIP_STUDIO_BADGE-rgb.svg" alt=""><br>
                <p>For the cases that require very sensitive handling by the most talented and experienced hands, our clients triage them to our VIP team. We have assembled a team of the most experienced and talented technicians to satisfy the most demanding patient or the most intricate of cases. </p>
                <p>Blending science and artistry, our VIP technicians will collaborate closely with you on each case. We seek to clearly understand your vision of the outcome and will consult closely with you regarding the outcome and obstacles presented in these more challenging cases. </p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 offset-md-2 col-md-4 text-center text-lg-right">
                <a href="#contact" class="btn">Contact</a>
            </div>
            <div class="col-12 col-md-4 text-center text-lg-left">
                <a href="/send-case/new-doctor/" class="btn">Send a Case</a>
            </div>
        </div>
    </div>
</section>
<section id="artwork">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="title">
                    <h1>Artwork, not Lab Work</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-6">
                <h2>Talent</h2>
                <p>Maverick VIP Studio is staffed with some of the most talented and gifted technicians in the industry. Decades of commitment to learning and improvement ensure that only the most skilled hands work on your VIP cases. </p> 
            </div>
            <div class="col-12 col-sm-6">
               <h2>Service</h2>
               <p>VIP Studio clients experience enhanced service,interaction and expertise with the brightest members of our team. From case planning to patient consultations,our VIP staff become a great extension to your immediate team.</p>
            </div>
            <div class="col-12 col-sm-6">
               <h2>Experience</h2>
               <p>In addition to experience, VIP Studio uses the highest esthetic material on the market. This material ensures the look and strength that VIP patients desire.</p>
            </div>
            <div class="col-12 col-sm-6">
               <h2>Result</h2>
               <p>Let’s develop a partnership of collaboration to work through some of the challenging cases that present themselves. Together we can create phenomenal results.</p>
            </div>
        </div>
    </div>
</section>
<section id="VI-patient">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6 d-flex">
                <div class="align-self-center">
                    <h1>Very Important</h1>
                    <h2>Patient</h2>
                    <p>Every Clinician has VIP patients. Family members, other dentists, local celebrities, these are all VIP’s that inherently elevate the intensity of treatment. Partner with our most-talented technicians to get it right, the first time.</p>
                </div>
            </div>
            <div class="col-12 col-sm-6 text-center">
                <img src="/img/8365-VIP-Patient-Thumb.png" alt="VI-Patient Thumb">
            </div>
        </div>
    </div>
</section>
<section id="VI-prosthetic">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6 text-center">
                <img src="/img/VI-Prosthetic-Thumb.png" alt="VI-Prosthetic Thumb">
            </div>
            <div class="col-12 col-sm-6 d-flex">
                <div class="align-self-center">
                    <h1>Very Important</h1>
                    <h2>Prosthetic</h2>
                    <p>Some patients just do not present with a textbook-like case. Complications can lead to a very challenging path forward for both the clinician and the technician. Withd ecades of experience our master technicians can work through the intricacies and options with each case to find the optimal path forward for a successful finish.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="VI-practice">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6 d-flex">
                <div class="align-self-center">
                    <h1>Very Important</h1>
                    <h2>Practice</h2>
                    <p>We have clients that use our VIP Services for all of their cases. They prefer the enhanced service, interaction and overall synergy brought to all of their cases. These clients like the boutique quality and feel of working with 2-3 technicians all the time. VIP becomes a close partner with them in delivering the consistent quality they demand.</p>
                </div>
            </div>
            <div class="col-12 col-sm-6 text-center">
                <img src="/img/8365-VIP-Practice-Thumb.png" alt="VI-Patient Thumb">
            </div>
        </div>
    </div>
</section>
<section>
    <a name="contact"></a>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="form-container">
                    <h4>Contact Us!</h4>
                    <form id="contact-form" action="">
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input id="name-contactform" class="form-control" placeholder="Doctor's Name" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input id="practice-name-contactform" class="form-control" placeholder="Practice Name" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input id="phone-contactform" class="form-control" placeholder="Phone Number" required="required" type="tel" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input id="email-contactform" class="form-control" placeholder="Email Address" required="required" type="email" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <textarea id="message-contactform" class="form-control" placeholder="What can we do for you?"></textarea>
                        </div>
                        <input type="hidden" id="public_id" value="7804aba8ed8d4374bc9a997ee47ff53e" />
                        <div class="g-recaptcha" data-sitekey="6LcJX6YUAAAAAPZJgl_qtg2bxp1ZVdBHLzqugYYK"></div>
                        <div class="info-btn">
                            <button type="submit">
                                <div class="info-btn-bx"> @include('_partials.mail-icon')</div>
                                <div class="info-btn-txt">Submit Now</div>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="loader">Loading...</div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var formPending = false;
        $('#contact-form').submit(function(event) {
            event.preventDefault();
            if (formPending)
                return;
            formPending = true;
            $(this).hide();
            $('#contactForm .alert').remove();
            $('.loader').show();
            $.ajax({
                url: 'https://sheikah.amgservers.com/api/contact/7804aba8ed8d4374bc9a997ee47ff53e/137e29d9a81a466f82d7f1a3e666c3e7',
                method: 'post',
                data: {
                    'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
                    id: $('#public_id').eq(0).val(),
                    practice: $('#practice-name-contactform').eq(0).val(),
                    name: $('#name-contactform').eq(0).val(),
                    phone: $('#phone-contactform').eq(0).val(),
                    email: $('#email-contactform').eq(0).val(),
                    message: $('#message-contactform').eq(0).val()
                },
                success: function(data) {
                    $('.loader').hide();
                    $('#contact-form').after('<p>Thank you for contacting us! We\'ll get in touch with you as soon as possible!</p>');
                },
                error: function(data, status, err) {
                    $('.loader').hide();
                    $('#contact-form').show();
                    formPending = false;
                    $('#contact-form button[type="submit"]').before('<div class="alert alert-danger" role="alert">Please fill out all of the fields</div>');
                }
            });
        });
    });
</script>
@endsection