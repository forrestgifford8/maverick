@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Blog',
    'meta_description' => ''
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
@endsection

@section('body')
<section id="blog">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="blog-list">
                    <h2>Solving the open vs closed tray impression dilemma. When to use open and closed tray impressions</h2>
                    <div class="blog-img">
                        <img src="/img/Open-Closed-Tray-Impressions-538x218.png" alt="Solving the open vs closed tray impression dilemma.">
                    </div>
                    <h5>Dec 12</h5>
                    <p>“Be a Dentist,” they said. “It’s easy. No stress at all…”</p>

                    <p>Your patient is in the chair, tray in place, material surrounding the closed tray impression transfer posts. Time is up, material has set and now you attempt to dislodge the tray.  Great!  It’s mechanically locked into place.  Now your patient is wide eyed and you’re reaching for your handpiece.  “No stress at all.”   What happened?</p>

                    <p>The following is a quick reference on choosing when to use the Open Tray and Closed Tray Impression Techniques:</p>
                    
                    <h3>Closed Tray Impressions</h3>

                    <p>Closed tray impressions are ideal for single unit implant restorations.   This technique can be used for multiple units, provided they are parallel.  Stock impression trays are used for the closed tray technique. If implants are not parallel, the closed tray technique can cause the impression to lock into the patient’s mouth. Closed tray can be inaccurate if the impression coping is not placed back into the impression properly before fabricating models for the case.</p>
                    
                    <h3>Open Tray Impressions</h3>

                    <p>The open tray technique is indicated when multiple implants are being restored.  Using a stock or custom tray with corresponding holes to the implant sites, this technique allows for diverging angled implants.  While this technique is slightly more involved, it helps avoid mechanical locking and ultimately saves your time and time and stress.  Clinicians can also splint the impression copings together with resin before impression taking to improve accuracy.  The open tray technique is more accurate when done properly because the coping locks into the impression material and is not removed before fabricating a model.</p>

                    <p>When questioning which type of impression coping to use we find, from our experience, the open tray technique to be the most accurate. For all implant impressions, it is best to use a full arch tray whether it is open or closed, custom or stock tray.</p>
                </div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection