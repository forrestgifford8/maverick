@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Blog',
    'meta_description' => ''
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
@endsection

@section('body')
<section id="blog">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="blog-list">
                    <h2>Changing Lives with Donated Dentistry</h2>
                    <div class="blog-img">
                        <img src="/img/Donated-Dentistry-Maverick-Dental-Laboratories-Mission-of-Mercy-Pittsburgh-538x218.png" alt="Changing Lives with Donated Dentistry">
                    </div>
                    <h5>July 11th</h5>
                    <p>In a single weekend over 1,000 individuals received charitable dental care at the PPG Paints Arena in Pittsburgh, PA. Over the course of two full days, dentists, hygienists, assistants and lab technicians volunteered their time to help those in need at the Mission of Mercy dental care event.</p>

                    <p>Over 1,000 patients were treated by 133 dentists and 1,200 volunteers.</p>

                    <p>Patients included working families without dental insurance and individuals hit by hard times, in serious need of dental attention. Mission of Mercy was a godsend made possible by volunteers with exceptional clinical skill and a willingness to help. For the second year in a row, Maverick was pleased to contribute the time and talent of our technicians and staff to assist with the prosthetic needs of this event.</p>

                    <p>Maverick Dental Laboratories has a big heart for people in need and is heavily involved with a free dental clinic at Catholic Charities in Pittsburgh. Additionally, we partner with many of our clients nationwide when charitable cases are brought to our attention.</p>

                    <p>Should you find yourself involved with a charitable case that requires fixed or removable prosthetics, we invite you to contact us for assistance. Depending on the circumstances of the case and our capacity, we will do our best to help.</p>

                    <p>View video and images from the “Mission of Mercy” event below.</p>
                    <iframe src="https://player.vimeo.com/video/278503891" allowfullscreen="allowfullscreen" width="652" height="360" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection