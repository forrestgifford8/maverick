@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Blog',
    'meta_description' => ''
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
@endsection

@section('body')
<section id="blog">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="blog-list">
                    <h2>Driving Patients & Sales</h2>
                    <div class="blog-img">
                        <img src="/img/car-fixed-538x218.png" alt=">Driving Patients & Sales">
                    </div>
                    <h5>Dec 7</h5>
                    <p>Last June when Maverick Dental Lab’s President and Co-Owner Joe Fey and CFO Dale Chandler were number crunching to cut costs, they realized the laboratory could save 20% per year by purchasing its own delivery cars rather than reimbursing its drivers for using their own cars. Owning their own vehicles also gave Chandler–who had a fresh “consumer” perspective thanks to his background in public relations and advertising–another idea. He saw an opportunity to create a marketing campaign that raises dental health awareness and drives patients to the lab’s clients. Through that, the “Follow Me to a Great Dentist” campaign was born.</p>

                    <p>Serving southwestern Pennsylvania, the 35-person laboratory purchased four black Scion xBs and wrapped them completely with humorous images and messages designed to pique the dental consumer’s attention. For example, one car features a photo of a man with green teeth and the tagline, “Not All GREEN is Good. Follow me to a great dentist.” The cars have Maverick’s phone number and website address so consumers can get free referrals to dentists in their area. The laboratory sends the patient contact information about three dentist-clients and lets each one know his/her name was given out as a referral. “Many of the inquiries we receive are from consumers who aren’t currently seeing a dentist and have an issue that requires attention,” says Larry Albensi, co-owner.</p>

                    <p>In addition to the cost of the cars, the lab invested about $10,000 on the campaign, which covered the cost of hiring a professional photographer and actors/models for a day-long photo shoot in Pittsburgh, graphic design firm services and printing the adhesive wraps for the cars.</p>

                    <p>The investment has been worth it: since getting the vehicles out on the road in December, the lab gets at least three calls a week and, due to Maverick’s overall regional marketing campaigns this past year–which included direct mailings, seminars with nationally recognized speakers and one full-time sales rep–the lab has seen substantial growth. In fact, it just added another delivery car and route to its fleet.</p>

                    <p>“Whether it makes people think more about their dental situation or gets them into the chair, we are definitely raising awareness,” says Chandler. “It’s also been a lot of fun. There isn’t always a lot of humor in the dental industry and we want people to smile when they think of the name Maverick.” And consumers in their area won’t stop smiling anytime soon. “Harry the Denture Guy” is making his appearance on the newest delivery vehicle, kicking off the lab’s marketing campaign for its new next-day reline and repair service.</p>
                </div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection