@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Blog',
    'meta_description' => ''
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
@endsection

@section('body')
<section id="blog">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="blog-list">
                    <h2>6 Techniques That Turn Dentists Into Shade Matching Masters</h2>
                    <div class="blog-img">
                        <img src="/img/Dental-Shade-Matching-Techniques.png" alt="6 Techniques That Turn Dentists Into Shade Matching Masters">
                    </div>
                    <h5>Jan 5th</h5>
                    <p>Each year, over half of the remake cases that come back to our laboratory are due to an improper shade match.  This is frustrating for you and your patients, and certainly is costly for all of us.  But we have good news! By adding a minimal amount of effort we can achieve a substantial improvement and drastically reduce these disappointments.</p>

                    <p>Color and Shade are subjective values and involve many variables. Shade eludes exactness due to its layered quality. There are essentially three elements to shade; hue is the “color,” chroma is the concentration or saturation of the hue, and value is the lightness or darkness of a hue.</p>

                    <p>While shade tabs provide an effective start, most dentists will agree that a “perfect match” is a combination of several shade tabs and not just one.</p>

                    <p>Here are some often overlooked basics to reincorporate to improve success:</p>
                    
                    <h3>1. Measure from the same stick</h3>
                    <p>When completing the prescription, please note the exact shade guide used (specify manufacturer). We have every shade guide available, however, since small variations exist between manufacturers, it is enormously helpful to have this information.</p>
                    
                    <h3>2. Take the shade at the beginning of the appointment</h3>
                    <p>Please remember to take the shade before preparation.Teeth tend to lighten with dehydration and this slight nuance can lessen the accuracy of the shade communication.</p>
                    
                    <h3>3. Stump shades</h3>
                    <p>Often overlooked, the inclusion of the shade of prepared teeth is extremely helpful for ensuring the correct final shade of all-ceramic restorations.</p>
                    
                    <h3>4. Operatory lighting</h3>
                    <p>Most practices have replaced their lights with Natural Daylight bulbs measuring approximately 5500 Kelvin. It makes all the difference.</p>
                    
                    <h3>5. Photography</h3>
                    <p>We love photos! Please include the shade tabs in your images with the etched identification of the shade clearly visible. While the shades may not be perfect, having the objective shade tab in the photo give us a great starting point. Inclusion of multiple shade tabs is also very helpful. Photos are key to communicating texture, characterization, translucency, size and shape. Photos can be sent to photos@maverickdental.com after they have been taken.</p>
                    
                    <h3>6. Five specific angles</h3>
                    <h4>1. Patient: Please include a full face photo of the patient smiling</h4>
                    <p>Dental Patient Shade Facial</p>
                    <img class="mb-3" src="/img/Dental-Patient-Shade-Facial.png" alt="Five specific angles">
                    
                    <h4>2. Shade tab: Include the incisal edge of both tooth and shade tab</h4>
                    <p>Dental Shade Match Shade Tab</p>
                    <img class="mb-3" src="/img/Dental-Shade-Match-Shade-Tab.png" alt="Include the incisal edge of both tooth and shade tab">
                    
                    <h4>3. Facial view: Provide a full facial view of tooth and those adjacent</h4>
                    <p>Dental Shade Match Facial View</p>
                    <img class="mb-3" src="/img/Dental-Shade-Match-Facial-View.png" alt="Provide a full facial view of tooth and those adjacent">
                    
                    <h4>4. Distal/Mesial: Please capture both aspects of the tooth</h4>
                    <p>Dental Shade Match Distal Mesial View</p>
                    <img class="mb-3" src="/img/Dental-Shade-Match-Distal-Mesial-View.png" alt="Please capture both aspects of the tooth">
                    
                    <h4>5. Incisal edge: By using a dark background, translucency can best be captured and communicated</h4>
                    <img class="mb-3" src="/img/Dental-Shade-Incisal-Edge-View.png" alt="By using a dark background, translucency can best be captured and communicated">
                    
                    <p>Dental Shade Incisal Edge ViewWe are committed to helping you keep your promises to your patients the first time, every time. With a bit more information properly and consistently communicated, we can all vastly improve our chances of immediate success.</p>
                    
                </div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection