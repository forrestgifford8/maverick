@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Blog',
    'meta_description' => ''
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
@endsection

@section('body')
<section id="blog">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="blog-list">
                    <h2>Model-less Digital Restorations: Are they faster, easier, better, and preferably less expensive?</h2>
                    <div class="blog-img">
                        <img src="/img/Model-Less-Digital-Restorations.png" alt="Model Less Digital Restorations">
                    </div>
                    <h5>July 27</h5>
                    <p>Yes, yes, yes and yes</p>

                    <p>Purchasing is almost always a study in compromise. Rarely can you satisfy all four of the aforementioned criteria with any choice, however, model-less monolithic crowns manufactured from digital scans score well with each.</p>

                    <p>The taking of high quality impressions has always been a challenge for clinicians and, given the many variations in levels of accuracy and quality, manufacturing in the laboratory, can be difficult with inaccurate and hard-to-read impressions.  Digital impressions eliminate inaccuracies and standardize the quality of the digital records, thus allowing greater success with final restoration insertions.</p>

                    <p>Faster: Digital scanning is fast and accurate.  Many of our clients delegate scanning to their team members while using that time more productively.  Working with an emailed digital scan, the case is in the lab in seconds and can be put into production immediately.  Complete fabrication time is typically 3-5 days in-lab, but can be as quick as 24 hours.  Digitally designed and manufactured restorations from accurate scans fit better and require minimal, if any, adjustment.  Working with a competent laboratory that takes the time to dial in your design preferences makes seating times fast and predictable.</p>

                    <p>Easier: After getting through the learning curve of working with a scanner and the associated software, the actual process is easy.  It is certainly much easier for the patient who now has an alternative to distasteful impression materials.  Another benefit is being able to see the “positive” image of your preparation on a large monitor and immediately address undercuts, inadequate reduction or any other aspect that requires attention prior to sending the impression to the laboratory.</p>

                    <p>Better: There are many useful materials available today because of digital dentistry such as e.max, zirconia, layered zirconia and milled PMMA temporaries.  Material selection aside, digitally manufactured restorations from scans remove concerns of distortion and margin inaccuracy.  On the lab side, variations in model fabrications (expansion rates, material tears, manual die ditching) that serve as potential sources of inaccuracy are eliminated.  This drastically reduces fit issues thus cutting down the time, cost and aggravation most common associated with remakes.</p>

                    <p>Maverick accepts scans from all systems and we offer this information from a recent ADA Study (2015 Volume 10 Issue 4)</p>

                    <p>Trueness (accuracy):</p>
                    <ol>
                        <li>TRIOS (6.9 ± 0.9 µm) (Black & White model)</li>
                        <li>CS 3500 (9.8 ± 0.8 µm)</li>
                        <li>iTero (9.8 ± 2.5 µm)</li>
                        <li>True Definition (10.3 ± 0.9 µm)</li>
                        <li>PlanScan (30.9 ± 10.8 µm),</li>
                        <li>CEREC OmniCam (45.2 ± 17.1 µm) (Fig. 2).</li>
                    </ol>

                    <p>Precision (consistency):</p>
                    <ol>
                        <li>TRIOS (4.5 ± 0.9 µm) (Black & White model)</li>
                        <li>True Definition (6.1 ± 1.0 µm)</li>
                        <li>iTero (7.0 ± 1.4 µm)</li>
                        <li>CS3500 (7.2 ± 1.7 µm)</li>
                        <li>CEREC OmniCam (16.2 ± 4.0 µm)</li>
                        <li>PlanScan (26.4 ± 5.0 µm)</li>
                    </ol>

                    <p>Less Expensive: Model-less restorations save shipping costs and model fabrication.  Maverick discounts shipping fees by 50% for non-local accounts and offers a $10 discount off retail price for a savings of $15.95 per case.  Capital Equipment purchases should be made deliberately and after careful consideration.  ROI calculations need to be carefully validated and reviewed to ensure that the purchase of a digital scanning system for impressions will make sense in your practice.  Consider intangible benefits such as positioning your practice as cutting edge, and delegation of scanning to your team, thus allowing you to perform more valuable tasks.</p>

                    <p>From the laboratory perspective, we highly encourage our clients to become involved.  We have seen the great benefits in faster workflow, fewer remakes and happier clients since joining over ten years ago with digital manufacturing.</p>

                </div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection