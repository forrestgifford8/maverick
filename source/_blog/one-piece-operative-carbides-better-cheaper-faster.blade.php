@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Blog',
    'meta_description' => ''
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
@endsection

@section('body')
<section id="blog">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="blog-list">
                    <h2>Better, Cheaper, Faster. One-Piece Operative Carbides are all that.</h2>
                    <div class="blog-img">
                        <img src="/img/One-Piece-Operative-Carbides.png" alt="Solving the open vs closed tray impression dilemma." alt="Better, Cheaper, Faster. One-Piece Operative Carbides are all that.">
                    </div>
                    <h5>Oct 2</h5>
                    <p>Caught between an important end-of-the-day family commitment and the realization that you are half an hour behind, it just makes sense to focus, push through and catch up.  Your last patient has a large failing restoration to replace, when half way through the procedure, suddenly the business end of your 557 is gone, separated from the shank and there’s no telling where it went.  It’s frustrating, maddening and a touch worrisome as you conclude it disappeared with the suction.  Does this sound familiar?</p>

                    <p>While rare, patients have swallowed pieces of rotary cutting instruments and worse, have aspirated them.</p>

                    <p>Maverick offers One-Piece operative carbides made from a solid piece of carbide.  The manufacturing process starts with a blank of carbide that is then ground into a cutting tool – shank, neck and cutting head are all part of this monolithic blank.  This produces a substantially more robust cutting tool that is highly resistant to breakage.</p>

                    <p>This innovation, coupled with enhanced blade geometry, greatly enhances the performance of our carbide burs.  Substantial testing has been performed to refine the tooth geometry that balances the highest cutting efficiency possible with significantly reduced vibrational chatter.</p>

                    <p>Most operative carbides are manufactured with a two-piece process.  Since carbide is expensive, it costs less to use a smaller amount of carbide for the cutting head and weld or solder that carbide to a steel shank.  This inherent design flaw results in breakage when too much force is applied and the bur is placed under too much unsustainable pressure.</p>

                    <p>Whatever the workhorse operative carbide of choice (330, 557, 701, 1157) in your practice, consider upgrading to premium One Piece carbides available at the same, or better, price you pay for inferior two-piece carbides.</p>
                    
                    <p><span class="font-weight-bold">Maverick’s One-Piece Operative Carbides retail at $16.90 for a pack of 10.</span> View our Rotary Instrument catalog for more information!</p>
                    
                    <a href="/img/Maverick-Rotary-Catalog.pdf" class="btn" target="_blank">View Catalog</a>
                </div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection