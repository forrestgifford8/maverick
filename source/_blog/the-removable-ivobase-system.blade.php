@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Blog',
    'meta_description' => ''
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
@endsection

@section('body')
<section id="blog">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="blog-list">
                    <h2>Make Costly Chairside Adjustments a Thing of the Past with The Removable IvoBase System</h2>
                    <div class="blog-img">
                        <img src="/img/the-removable-ivobase-system.jpg" alt="Make Costly Chairside Adjustments a Thing of the Past with The Removable IvoBase System">
                    </div>
                    <h5>July 11th</h5>
                    <p>Two years ago Maverick invested in Ivoclar’s Ivobase System for processing our Standard and Premium dentures. By integrating this system we were able to instantaneously improve our finished prosthetics in both fit and finish.</p>

                    <p>This improvement, coupled with our highly experienced staff (averaging 52 years old with 28 years of experience), has allowed us to become a trusted prosthetics partner for dentists nationwide.</p>
                    
                    <img class="mt-2 mb-2" src="/img/IvoBase-System.jpg" alt="">
                    
                    <h3>Our Pricing Sweet Spot</h3>

                    <p>Most laboratories that own the Ivobase System reserve this costly processing for their most expensive dentures. While Maverick certainly processes Premium Dentures with Ivobase, we also process our Standard IvoBase Dentures using this system. For $279.95 we include Dentsply TruExpression™ teeth and Ivobase processing. This vastly improves quality while controlling your lab expenses.</p>
                    
                    <h3>No Shrinkage/Better Fit</h3>
                    
                    <img class="mt-2 mb-2" src="/img/IvoBase-Denture-Fit.jpg" alt="">
                    
                    <p>With a traditional flask and pack method, an upper palate will pull away from the model leaving a slight gap resulting in a poor fit. With an automated injection process using factory pre-mixed acrylic being pumped under constant heat and pressure, shrinkage is eliminated.</p>
                    
                    <h3>Dense Material results in Odor-Free Dentures</h3>
                    
                    <img class="mt-2 mb-2" src="/img/Odor-Free-IvoBase-Dentures.jpg" alt="">
                    
                    <p>Due to the continuous, pressurized injection process dentures processed with the Ivobase System are denser, and bubble free resisting, staining and plaque. This results in reduced odor and will not attract bacteria.</p>
                </div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection