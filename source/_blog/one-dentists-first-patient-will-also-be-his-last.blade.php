@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Blog',
    'meta_description' => ''
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
@endsection

@section('body')
<section id="blog">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="blog-list">
                    <h2>Customers for life. One Dentist’s first patient will also be his last.</h2>
                    <div class="blog-img">
                        <img src="/img/Dental-Customers-For-Life-538x218.png" alt="Customers for life.">
                    </div>
                    <h5>July 11th</h5>
                    <p>The dentist-dental lab relationship is comprised of many moving parts and Maverick is constantly communicating with our clinicians. Our typical conversations range from case turnaround times and impression questions, to material considerations and technical evaluations.</p>

                    <p>Earlier this month Maverick got a special request from a long-time customer, Dr. Jack Spencer. He asked if we could do a quick turnaround time on a crown that would be his last to insert before retiring. We were honored to do this for him and elated when he told us who the crown was for…</p>

                    <p>Dr. Jack Spencer’s very first patient to walk through his practice 35 years ago will also be his last.</p>

                    <p>When Dr. Spencer first started his practice, a woman who lived across the street called to make an appointment. Judy Alvarez was surprised when Dr. Spencer said he could see her immediately. Dr. Spencer was happy to have his first patient.</p>

                    <p>Fast forward to present time. Maverick was able to hand deliver Judy’s crown to Dr. Spencer on his last day of practicing dentistry. “It has been a privilege and an honor to work with Dr. Spencer all of these years. We pride ourselves on long-term relationships here at Maverick,” says co-owner Larry Albensi.</p>

                    <p>Like you, we want upstanding relationships and customers for life.  We keep your patient’s dental quality at the forefront of all that we do. See Dr. Spencer and Judy tell their story in the video below.</p>
                    <iframe src="https://player.vimeo.com/video/273950204" allowfullscreen="allowfullscreen" width="652" height="360" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection