@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Blog',
    'meta_description' => ''
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
@endsection

@section('body')
<section id="blog">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="blog-list">
                    <h2>Dream Lab: Maverick Dental Transforms Car Dealership into Spacious, Modern Laboratory</h2>
                    <div class="blog-img">
                        <img src="/img/Maverick-Dental-2013-299-56-538x218.jpg" alt="Solving the open vs closed tray impression dilemma.">
                    </div>
                    <h5>Oct 16</h5>
                    <p>Joseph Fey and Larry Albensi, Co-Owners of Maverick Dental Laboratories in Monroeville, PA, never dreamed of moving their lab into a former car dealership, but as soon as they saw the space, they knew it could be repurposed into an ideal laboratory.</p>

                    <p>The 16,000-sq-ft facility offered four main areas to accommodate the various functions of the laboratory and provide ample room for future growth: a huge open garage that would be ideal as the main production room; a spacious showroom with floor-to-ceiling windows for the ceramics, quality control and sales/customer service departments; and management offices and a shipping/receiving area that would be used for those same functions.</p>

                    <p>Although the space was ideal, the building needed significant renovations to make it the streamlined showplace they desired. Before making an offer, the owners invested in a two-month architecture and construction study to be sure the property was sound and the project feasible. A contractor conducted thorough inspections – including an environmental study to be sure the property wasn’t contaminated with petroleum or lubricants that would require costly and time-consuming cleanup – while Fey and Albensi worked with an architect to develop an ideal design. “Based on the study and the contractor’s bid, we knew we had to essentially gut the building and put it back together again, so we used that valuable information to negotiate a better price,” says Fey.</p>

                    <p>The renovation project took four months to complete and included electrical and HVAC upgrades; framing/masonry work; and a host of other projects like creating ADA-compliant, handicap-accessible bathrooms, entrances and parking areas.</p>

                    <p>During the process, the owners sought input from their 50-person staff in a variety of areas. For instance, the lab hired a cabinet maker to build custom benches and had a test model constructed so technicians could try it out and give feedback on the height, ergonomics and even where they wanted the suction holes.</p>

                    <p>They also consulted with lighting manufacturer Sylvania to conduct a lighting study; the company worked with the staff to determine the exact number of lumens and type of light bulbs to use in each area of the lab. And, since the lab wanted to tint the large front windows in the facility’s former showroom for privacy but didn’t want to affect the shading of restorations, it worked with a window-coatings manufacturer to select an ideal tint that would lower the amount of sunlight transmitted without negatively affecting color accuracy.</p>

                    <p>In May 2013, the renovation was complete and the laboratory moved into its spacious new dream home that offers room for a 98-person staff and additional manufacturing equipment. In July, to celebrate the opening as well as the lab’s 10-year anniversary, Maverick held a catered open house for 300 clients, vendors and staff. “We received rave reviews and I think our new building conveys our commitment to the future of Maverick,” says Albensi. “We love what we do and we hope to continue for years to come.”</p>
                </div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection