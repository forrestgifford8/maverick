@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Blog',
    'meta_description' => ''
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
@endsection

@section('body')
<section id="blog">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="blog-list">
                    <h2>Crowns for a Cure</h2>
                    <div class="blog-img">
                        <img src="/img/506afa4573f90-620_465-538x218.png" alt="Crowns for a Cure.">
                    </div>
                    <h5>Mar 7</h5>
                    <p>Maverick Dental Laboratories is Tickled Pink. Maverick is launching a “Crowns for a Cure” campaign during the month of October to help raise awareness and find a cure for breast cancer. As part of the campaign, Maverick has turned their marketing and operating materials pink to coincide with the campaign’s message. Prescriptions, case boxes, invoices, informative box stuffers and even delivery driver’s wardrobes have all transitioned to the familiar campaign color.</p>

                    <p>During the month of October, a portion of the proceeds from every case sent to Maverick will be donated to the Kristy Lasch Miracle Foundation. Founded in 2005, the foundation’s mission is to provide financial assistance to women under 30 living with breast cancer. Before she died, Kristy envisioned creating an organization to help younger breast cancer patients, says Tom Lasch, Kristy’s father and founder of the Kristy Lasch Miracle Foundation. “She knew too well how financially cumbersome the disease could become, and the unique challenges of having the disease at such a young age. The Kristy Lasch Miracle Foundation was created in her memory, and honors her dream.” More information about the foundation can be found by visiting their website at www.kristylasch.org.</p>

                    <p>“We are always looking for a way to give back to the community and feel that contributing to the Kristy Lasch Foundation will make a significant difference”, says Joe Fey, co-owner of Maverick Dental Laboratories. “We all know somebody who has been affected by breast cancer. During our research on the issue, we were shocked to find that one in eight women will be diagnosed with breast cancer. This has become a very serious health concern worldwide and we are happy to help in any way we can.” If you would like more information about the campaign, please contact Stephanie Bergman at 724-733-7444.</p>
                </div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection