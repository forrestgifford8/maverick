@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'DSO Services',
    'meta_description' => 'Maverick Dental Laboratories offers VIP Services to all our clients who are faced with a case requiring unparalleled esthetics and attention.  '
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'DSO Services'])
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img class="mb-5" src="/img/DSO-Header-2.png" alt="DSO Services Page Header">
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <p>While working with large Enterprise laboratories seems to make sense to most Group Practice and DSO Organizations, they can be much better served by partnership with a mid-size laboratory that can out-service corporate conglomerates.</p> 

                <p>Lacking a legacy succession plan or alternative exit strategy, many mid-sized laboratories have been absorbed by corporate chain laboratories because the owners had no exit strategy or legacy succession plan. The remaining strong independent laboratories that have the critical mass required to attract key talent and acquire cutting-edge technology and business systems can be the best option for Group Practice and DSO Organizations.</p> 

                <p>Labs like Maverick can be more flexible in meeting their client’s needs and excel in forging excellent relationships with the Ownership, Clinical Directors, Associate Directors and staff.</p>
            </div>
            <div class="col-12 col-md-6">
                <img src="/img/VEN_DIAGRAM-rgb.png" alt="DSO Ven Diagram">
            </div>
        </div>
    </div>
</section>
<section class="container d-none d-md-block">
    <div class="row">
        <div class="col-12 d-flex">
            <div>
                <h1 style="white-space: nowrap;line-height: .3;font-style: italic;font-size: 3rem;">How WE Compare.</h1>
            </div>
            <div style="height: 4px; width: 100%;background-color: #ba9959;align-self: flex-end;margin-left: 1rem;"></div>
        </div>
    </div>
</section>
<section class="container d-none d-md-block">
    <div class="row">
            <div class="col-12">
                <table style="width: 100%;" >
                    <tbody id="dso-table">
                        <tr>
                            <td>&nbsp;</td>
                            <td class="text-center" valign="bottom">Small Local Lab</td>
                            <td class="text-center" valign="bottom"><img style="max-width: 200px;" src="/img/Maverick_Black_logo.png" alt="Maverick Black logo"></td>
                            <td class="text-center" valign="bottom">Corporate Large Chain Lab</td>
                        </tr>
                        <tr>
                            <td class="gray-back top border-left-0">Privately Owned</td>
                            <td class="gray-back top text-center"><i class="fas fa-check"></i></td>
                            <td class="gray-back top text-center"><i class="fas fa-check"></i></td>
                            <td class="gray-back top text-center border-left-0">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="gray-back border-left-0">Full Service</td>
                            <td class="gray-back text-center">&nbsp;</td>
                            <td class="gray-back text-center"><i class="fas fa-check"></i></td>
                            <td class="gray-back text-center border-right-0"><i class="fas fa-check"></i></td>
                        </tr>
                        <tr>
                            <td class="gray-back border-left-0">Robust Metrics &amp; Reporting</td>
                            <td class="gray-back text-center">&nbsp;</td>
                            <td class="gray-back text-center"><i class="fas fa-check"></i></td>
                            <td class="gray-back text-center border-right-0"><i class="fas fa-check"></i></td>
                        </tr>
                        <tr>
                            <td class="gray-back border-left-0">Full Digital Capabilities</td>
                            <td class="gray-back text-center">&nbsp;</td>
                            <td class="gray-back text-center"><i class="fas fa-check"></i></td>
                            <td class="gray-back text-center border-right-0"><i class="fas fa-check"></i></td>
                        </tr>
                        <tr>
                            <td class="gray-back border-left-0">Deep Technical Team</td>
                            <td class="gray-back text-center">&nbsp;</td>
                            <td class="gray-back text-center"><i class="fas fa-check"></i></td>
                            <td class="gray-back text-center border-right-0"><i class="fas fa-check"></i></td>
                        </tr>
                        <tr>
                            <td class="gray-back border-left-0">Excellent Customer Service</td>
                            <td class="gray-back text-center"><i class="fas fa-check"></i></td>
                            <td class="gray-back text-center"><i class="fas fa-check"></i></td>
                            <td class="gray-back text-center border-right-0">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="gray-back border-left-0">Small Lab Feel</td>
                            <td class="gray-back text-center"><i class="fas fa-check"></i></td>
                            <td class="gray-back text-center"><i class="fas fa-check"></i></td>
                            <td class="gray-back text-center border-right-0">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="gray-back border-left-0">Well Capitalized for Expansion</td>
                            <td class="gray-back text-center">&nbsp;</td>
                            <td class="gray-back text-center"><i class="fas fa-check"></i></td>
                            <td class="gray-back text-center border-right-0"><i class="fas fa-check"></i></td>
                        </tr>
                        <tr>
                            <td class="gray-back bottom border-left-0">Business &amp; Technical Flexibility</td>
                            <td class="gray-back bottom text-center">&nbsp;</td>
                            <td class="gray-back bottom text-center"><i class="fas fa-check"></i></td>
                            <td class="gray-back bottom text-center border-right-0">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-12 d-flex text-align-right;">
            <div style="height: 4px; width: 100%;background-color: #ba9959;align-self: flex-end;margin-right: 1rem;"></div>
            <div>
                <h1 style="white-space: nowrap;line-height: .3;font-style: italic;font-size: 3rem;">Maverick… Just Right.</h1>
            </div>
        </div>
    </div>
</section>
<section class="container">
    <div class="form-container">
        <h4>Contact Us Today</h4>
        <form id="contact-form" action="">
            <div class="form-label-group mb-3" style="margin: auto;">
                <input id="name-contactform" class="form-control" placeholder="Your Name" required="required" type="text" />
            </div>
            <div class="form-label-group mb-3" style="margin: auto;">
                <input id="DSO-name-contactform" class="form-control" placeholder="DSO Name" required="required" type="text" />
            </div>
            <div class="form-label-group mb-3" style="margin: auto;">
                <input id="phone-contactform" class="form-control" placeholder="Phone Number" required="required" type="tel" />
            </div>
            <div class="form-label-group mb-3" style="margin: auto;">
                <input id="email-contactform" class="form-control" placeholder="Email Address" required="required" type="email" />
            </div>
            <div class="form-label-group mb-3" style="margin: auto;">
                <input id="address-contactform" class="form-control" placeholder="Address" required="required" type="text" />
            </div>
            <div class="form-label-group mb-3" style="margin: auto;">
                <textarea id="message-contactform" class="form-control" placeholder="Comments"></textarea>
            </div>
            <input type="hidden" id="public_id" value="7804aba8ed8d4374bc9a997ee47ff53e" />
            <div class="g-recaptcha" data-sitekey="6LcJX6YUAAAAAPZJgl_qtg2bxp1ZVdBHLzqugYYK"></div>
            <div class="info-btn">
                <button type="submit">
                    <div class="info-btn-bx"> @include('_partials.mail-icon')</div>
                    <div class="info-btn-txt">Submit Now</div>
                </button>
            </div>
        </form>
    </div>
    <div class="loader">Loading...</div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var formPending = false;
        $('#contact-form').submit(function(event) {
            event.preventDefault();
            if (formPending)
                return;
            formPending = true;
            $(this).hide();
            $('#contactForm .alert').remove();
            $('.loader').show();
            $.ajax({
                url: 'https://sheikah.amgservers.com/api/contact/7804aba8ed8d4374bc9a997ee47ff53e/45e01b916a914fb8b1bb353a7cfd23ff',
                method: 'post',
                data: {
                    'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
                    id: $('#public_id').eq(0).val(),
                    name: $('#name-contactform').eq(0).val(),
                    DSOname: $('#DSO-name-contactform').eq(0).val(),
                    address: $('#address-contactform').eq(0).val(),
                    phone: $('#phone-contactform').eq(0).val(),
                    email: $('#email-contactform').eq(0).val(),
                    message: $('#message-contactform').eq(0).val()
                },
                success: function(data) {
                    $('.loader').hide();
                    $('#contact-form').after('<p>Thank you for contacting us! We\'ll get in touch with you as soon as possible!</p>');
                },
                error: function(data, status, err) {
                    $('.loader').hide();
                    $('#contact-form').show();
                    formPending = false;
                    $('#contact-form button[type="submit"]').before('<div class="alert alert-danger" role="alert">Please fill out all of the fields</div>');
                }
            });
        });
    });
</script>
@endsection