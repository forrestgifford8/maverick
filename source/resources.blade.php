@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Resources',
    'meta_description' => 'Maverick Dental Laboratories offers VIP Services to all our clients who are faced with a case requiring unparalleled esthetics and attention.  '
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Resources'])
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img class="mb-5" src="/img/8365-Resources-Page-Header.png" alt="Resources Page Header">
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p>Maverick Dental Laboratories offers numerous resources for our clinicians to make work with our laboratory stress-free and simple. Below you will find our library of downloads, which includes a torque spec guide, Rx forms, and more.</p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/img/Maverick-Combined-RX.pdf" target="_blank">
                    <div class="prodThumb">
                        <img src="/img/Rx-Form-Resource-PDF-IMG_Cutout.png" alt="Rx Form Download">
                    </div>
                    <div class="prodTxt">
                        <h3>Rx Form</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/img/Maverick-VIP-RX.pdf" target="_blank">
                    <div class="prodThumb">
                        <img src="/img/Rx-Form-Resource-PDF-IMG_Cutout.png" alt="Rx Form Download">
                    </div>
                    <div class="prodTxt">
                        <h3>VIP Rx Form</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/img/Maverick-2020-Turnaround-Calendar-1-Day-Delivery.pdf" target="_blank">
                    <div class="prodThumb">
                        <img src="/img/Basic-Download-Resource-PDF-IMG_Cutout.png" alt="DI Impressions Download">
                    </div>
                    <div class="prodTxt">
                        <h3>Turnaround Calendar 1 Day</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/img/Maverick-2020-Turnaround-Calendar-2-Day-Delivery.pdf" target="_blank">
                    <div class="prodThumb">
                        <img src="/img/Basic-Download-Resource-PDF-IMG_Cutout.png" alt="DI Impressions Download">
                    </div>
                    <div class="prodTxt">
                        <h3>Turnaround Calendar 2 Day</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/img/DI-Comparison-Chart.pdf" target="_blank">
                    <div class="prodThumb">
                        <img src="/img/DI-Impression-Guide-Resource-PDF-Horizontal-IMG_cutout.png" alt="DI Impressions Download">
                    </div>
                    <div class="prodTxt">
                        <h3>DI Impressions</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/img/Torque-Spec-Sheet.pdf" target="_blank">
                    <div class="prodThumb">
                        <img src="/img/Torque-Spec-Guide-Resource-PDF-IMG_cutout.png" alt="Torque Spec Guide Download">
                    </div>
                    <div class="prodTxt">
                        <h3>Torque Spec Guide</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/img/ADA-Code-List-Product-Breakdown.pdf" target="_blank">
                    <div class="prodThumb">
                        <img src="/img/ADA-Code-Product-Breakdown-List-Guide-Resource-PDF-IMG_Cutout.png" alt="ADA Insurance Codes Download">
                    </div>
                    <div class="prodTxt">
                        <h3>ADA Insurance Codes</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/img/cementation-guide.pdf" target="_blank">
                    <div class="prodThumb">
                        <img src="/img/Cementation-Guide-Resource-PDF-Horizontal-IMG_cutout.png" alt="Cementaion Guide Download">
                    </div>
                    <div class="prodTxt">
                        <h3>Cementation Guide </h3>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection