@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Contact',
    'meta_description' => 'Ultimate Dental Lab is committed to helping your practice achieve success. '
    ])
@endsection

@section('body')
@include('_partials.page-header', ['img_src' => '8211-Contact-Page-header.jpg', 'img_alt' => 'Contact Page Header', 'img_class' => 'w-border'])
<section>
    <div class="container">
        <div class="row">
            <div class="col-12 intro-txt">
                <h2>404 - Page Not Found</h2>
                <p>The page you have requested could not be found.</p>
                <a href="/#" class="btn">Home Page</a>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection