@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Testimonials',
    'meta_description' => 'Discover why Maverick is a premier national laboratory. Our clients have provided us with testimonials that give you an insight into what it\'s like working with us.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Testimonials'])
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img class="mb-5" src="/img/8365-Testimonial-Page-Header.png" alt="Testimonials Page Header">
            </div>
        </div>
        <div class="row">
            <div class="col-12 mb-5">
               <h2>Discover Why Maverick Is a Premier National Laboratory</h2>
               <script src="https://apps.elfsight.com/p/platform.js" defer></script>
                <div class="elfsight-app-8a790f53-a1c7-44ff-af44-b6165269868e"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 testimonial">
                <p>"I have used many dental labs during my dental career. Without a doubt, Maverick Dental Lab and its people have been the best. From the absolutely great fit and esthetics of their crowns and bridges to the fantastic customer service and care that goes into every case, Maverick is in a league of its own. The most amazing part of working with Maverick is that I'm actually saving hundreds of dollars per month on my lab bill while getting great service and outstanding lab work! Thank you Maverick!"</p>
                <h4 class="text-right">–Rollyn Lee, DDS, Amery, WI</h4>
                <hr>
            </div>
            <div class="col-12 testimonial">
                <p>"My 1st time using your lab so I allowed extra time to seat crown. WOW! You blew my socks off! Absolutely beautiful! No adjustments. Spent most of my time cleaning up cement. Thank you- Thank you- Thank you!"</p>
                <h4 class="text-right">–Ann Aduddell, DDS, Canton, TX</h4>
                <hr>
            </div>
            <div class="col-12 testimonial">
                <p>"Thank you, Maverick Dental, for making my job less stressful. Only minor adjustments, if any, are required for my crown and bridge cases. This is the best lab I have used in my twenty-nine years of practice."</p>
                <h4 class="text-right">–Robert M. Fields, DDS, Paris, TN</h4>
                <hr>
            </div>
            <div class="col-12 testimonial">
                <P>"We enjoy working with Maverick Dental. They always do quality work and make our patients look great!"</P>
                <h4 class="text-right">–David E. Ijams, DDS, Cordova, TN</h4>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection