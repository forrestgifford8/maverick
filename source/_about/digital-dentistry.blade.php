@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Digital Dentistry',
    'meta_description' => 'Maverick Dental Laboratory accepts files from all major intraoral scanners and is fully digitally ready with the latest equipment. '
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
{{-- <section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Digtial Done Right'])
</section> --}}
<section id="digital-page-header"></section>
@endsection

@section('body')
<section id="di-page-header">
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>Maximize the results of your IOS investment with a lab partner that will take your  scans and create restorations exactly to your specifications, each and every time.</h2>
                    <a href="/send-case/new-doctor/" class="btn">Send a Case</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container di-thumbs">
        <div class="row">
            <div class="col-12 col-md-6 text-center">
                <img src="/img/capturing-your-preferences.png" alt="DI THumb">
            </div>
            <div class="col-12 col-md-6">
                <h1>Capturing Your Preferences</h1>
                <p>Our thorough on-boarding process for new digital scanning clients includes a calibration process that captures your preferences for contacts, occlusion, contour and characterization.</p>
            </div>
        </div>
    </div>
</section>
<section class="di-thumbs-2">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <h1>Success Through Collaboration</h1>
                <p>Through a series of quick discussions after your first cases are seated, we review and discuss mutual points for improvement as needed.</p>
            </div>
            <div class="col-12 col-md-6 text-center">
                <img src="/img/success-through-collaboration.png" alt="DI THumb">
            </div>
        </div>
    </div>
</section>
<section class="di-thumbs-3">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 text-center">
                <img src="/img/predictable-case-results.png" alt="DI THumb">
            </div>
            <div class="col-12 col-md-6">
                <h1>Predictable <br> Case Results</h1>
                <p>Our mission is to ultimately provide you with predictability on cases large and small. This is made possible by our investments in cutting-edge technology and our experienced CAD/CAM team.</p>
            </div>
        </div>
    </div>
</section>
<section id="dig-Thumbs">
   <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-3">
                <a class="prodLink text-center" href="/send-case/di-submission/">
                    <div class="prodThumb" style="margin: auto;margin-top: 2rem">
                        <img src="/img/3Shape-Trios-Pod-Scanner.png" alt="3Shape TRIOS®">
                    </div>
                    <div class="prodTxt">
                        <h3>3Shape TRIOS®</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <a class="prodLink text-center" href="/send-case/di-submission/">
                    <div class="prodThumb" style="margin: auto;margin-top: 2rem">
                        <img src="/img/Cerec-Sirona-Wand-Scanner.png" alt="CEREC® Scanner">
                    </div>
                    <div class="prodTxt">
                        <h3>CEREC® Scanner</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <a class="prodLink text-center" href="/send-case/di-submission/">
                    <div class="prodThumb" style="margin: auto;margin-top: 2rem">
                        <img src="/img/Carestream-3600-Wand-Scanner.png" alt="Carestream™ CS 3600">
                    </div>
                    <div class="prodTxt">
                        <h3>Carestream™ CS 3600</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <a class="prodLink text-center" href="/send-case/di-submission/">
                    <div class="prodThumb" style="margin: auto;margin-top: 2rem">
                        <img src="/img/iTero-Element2-Scanner.png" alt="iTero® Element 2">
                    </div>
                    <div class="prodTxt">
                        <h3>iTero® Element 2</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <a class="prodLink text-center" href="/send-case/di-submission/">
                    <div class="prodThumb" style="margin: auto;margin-top: 2rem">
                        <img src="/img/medit-i500-Scanner-wand.png" alt="Medit i500">
                    </div>
                    <div class="prodTxt">
                        <h3>Medit i500</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <a class="prodLink text-center" href="/send-case/di-submission/">
                    <div class="prodThumb" style="margin: auto;margin-top: 2rem">
                        <img src="/img/Planmeca-Emerald-Scanner.png" alt="Planmeca®">
                    </div>
                    <div class="prodTxt">
                        <h3>Planmeca®</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <a class="prodLink text-center" href="/send-case/di-submission/">
                    <div class="prodThumb" style="margin: auto;margin-top: 2rem">
                        <img src="/img/primescan_02.png" alt="Planmeca® Emerald™">
                    </div>
                    <div class="prodTxt">
                        <h3>Dentsply’s Prime Scan</h3>
                    </div>
                </a>
            </div>
        </div>
   </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection