@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Blog',
    'meta_description' => ''
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Blog'])
</section>
@endsection

@section('body')
<section class="pb-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img class="mb-5" src="/img/8365-Blog-Page-Header.png" alt="Blog Page Header">
            </div>
        </div>
    </div>
</section>
<section id="blog" class="pt-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="blog-list">
                    <h2>Changing Lives with Donated Dentistry</h2>
                    <div class="blog-img">
                        <img src="/img/Donated-Dentistry-Maverick-Dental-Laboratories-Mission-of-Mercy-Pittsburgh-538x218.png" alt="Changing Lives with Donated Dentistry">
                    </div>
                    <h5>July 11th</h5>
                    <p>In a single weekend over 1,000 individuals received charitable dental care at the PPG Paints Arena in Pittsburgh, PA. Over the course of two full days, dentists, hygienists, assistants and lab technicians volunteered their time to help those in need at the Mission of Mercy dental care event.</p>
                    <p>Over 1,000 patients were treated by 133 dentists and 1,200 volunteers.</p>
                    <p>Patients included working families without dental insurance and individuals hit by hard times, in serious need of dental attention. Mission of Mercy was...</p>
                    <a href="/changing-lives-with-donated-dentistry" class="btn">Read More&nbsp;&nbsp;<i class="fas fa-arrow-right"></i></a>
                </div>
                <hr>
            </div>
            <div class="col-12">
                <div class="blog-list">
                    <h2>Customers for life. One Dentist’s first patient will also be his last.</h2>
                    <div class="blog-img">
                        <img src="/img/Dental-Customers-For-Life-538x218.png" alt="Customers for life.">
                    </div>
                    <h5>Jun 7th</h5>
                    <p>The dentist-dental lab relationship is comprised of many moving parts and Maverick is constantly communicating with our clinicians. Our typical conversations range from case turnaround times and impression questions, to material considerations and technical evaluations.</p>
                    <p>Earlier this month Maverick got a special request from a long-time customer, Dr. Jack Spencer. He asked if we could do a quick turnaround time on a crown that would be his last to insert before retiring. We were honored to do this for him and...</p>
                    <a href="/one-dentists-first-patient-will-also-be-his-last" class="btn">Read More&nbsp;&nbsp;<i class="fas fa-arrow-right"></i></a>
                </div>
                <hr>
            </div>
            <div class="col-12">
                <div class="blog-list">
                    <h2>6 Techniques That Turn Dentists Into Shade Matching Masters</h2>
                    <div class="blog-img">
                        <img src="/img/Dental-Shade-Matching-Techniques.png" alt="6 Techniques That Turn Dentists Into Shade Matching Masters">
                    </div>
                    <h5>Jan 5th</h5>
                    <p>Each year, over half of the remake cases that come back to our laboratory are due to an improper shade match.  This is frustrating for you and your patients, and certainly is costly for all of us.  But we have good news! By adding a minimal amount of effort we can achieve a substantial improvement and drastically reduce these disappointments.</p>

                    <p>Color and Shade are subjective values and involve many variables. Shade eludes exactness due to its layered quality. There are essentially ...</p>
                    <a href="/dental-shade-matching-techniques/" class="btn">Read More&nbsp;&nbsp;<i class="fas fa-arrow-right"></i></a>
                </div>
                <hr>
            </div>
            <div class="col-12">
                <div class="blog-list">
                    <h2>Solving the open vs closed tray impression dilemma. When to use open and closed tray impressions</h2>
                    <div class="blog-img">
                        <img src="/img/Open-Closed-Tray-Impressions-538x218.png" alt="Solving the open vs closed tray impression dilemma.">
                    </div>
                    <h5>Dec 12th</h5>
                    <p>“Be a Dentist,” they said. “It’s easy. No stress at all…”

                    <p>Your patient is in the chair, tray in place, material surrounding the closed tray impression transfer posts. Time is up, material has set and now you attempt to dislodge the tray.  Great!  It’s mechanically locked into place.  Now your patient is wide eyed and you’re reaching for your handpiece.  “No stress at all.”   What happened?</p>

                    <p>The following is a quick reference on choosing when to use the Open Tray and Closed Tray ...</p>
                    <a href="/when-to-use-open-and-closed-tray-impressions/" class="btn">Read More&nbsp;&nbsp;<i class="fas fa-arrow-right"></i></a>
                </div>
                <hr>
            </div>
            <div class="col-12">
                <div class="blog-list">
                    <h2>Make Costly Chairside Adjustments a Thing of the Past with The Removable IvoBase System</h2>
                    <div class="blog-img">
                        <img src="/img/the-removable-ivobase-system.jpg" alt="Solving the open vs closed tray impression dilemma.">
                    </div>
                    <h5>Nov 28th</h5>
                    <p>Two years ago Maverick invested in Ivoclar’s Ivobase System for processing our Standard and Premium dentures. By integrating this system we were able to instantaneously improve our finished prosthetics in both fit and finish.</p>

                    <p>This improvement, coupled with our highly experienced staff (averaging 52 years old with 28 years of experience), has allowed us to become a trusted prosthetics partner for dentists nationwide.</p>
                    <a href="/the-removable-ivobase-system" class="btn">Read More&nbsp;&nbsp;<i class="fas fa-arrow-right"></i></a>
                </div>
                <hr>
            </div>
            <div class="col-12">
                <div class="blog-list">
                    <h2>Better, Cheaper, Faster. One-Piece Operative Carbides are all that.</h2>
                    <div class="blog-img">
                        <img src="/img/One-Piece-Operative-Carbides.png" alt="Solving the open vs closed tray impression dilemma." alt="Better, Cheaper, Faster. One-Piece Operative Carbides are all that.">
                    </div>
                    <h5>Nov 28</h5>
                    <p>Caught between an important end-of-the-day family commitment and the realization that you are half an hour behind, it just makes sense to focus, push through and catch up.  Your last patient has a large failing restoration to replace, when half way through the procedure, suddenly the business end of your 557 is gone, separated from the shank and there’s no telling where it went.  It’s frustrating, maddening and a touch worrisome as you conclude it disappeared with the suction.  Does this sound familiar?...</p>
                    <a href="/one-piece-operative-carbides-better-cheaper-faster/" class="btn">Read More&nbsp;&nbsp;<i class="fas fa-arrow-right"></i></a>
                </div>
                <hr>
            </div>
            <div class="col-12">
                <div class="blog-list">
                    <h2>Model-less Digital Restorations: Are they faster, easier, better, and preferably less expensive?</h2>
                    <div class="blog-img">
                        <img src="/img/Model-Less-Digital-Restorations.png" alt="Model Less Digital Restorations">
                    </div>
                    <h5>July 27</h5>
                    <p>Yes, yes, yes and yes</p>

                    <p>Purchasing is almost always a study in compromise. Rarely can you satisfy all four of the aforementioned criteria with any choice, however, model-less monolithic crowns manufactured from digital scans score well with each...</p>
                    <a href="/model-less-digital-restorations/" class="btn">Read More&nbsp;&nbsp;<i class="fas fa-arrow-right"></i></a>
                </div>
                <hr>
            </div>
            <div class="col-12">
                <div class="blog-list">
                    <h2>Dream Lab: Maverick Dental Transforms Car Dealership into Spacious, Modern Laboratory</h2>
                    <div class="blog-img">
                        <img src="/img/Maverick-Dental-2013-299-56-538x218.jpg" alt="Solving the open vs closed tray impression dilemma.">
                    </div>
                    <h5>Oct 16</h5>
                    <p>Joseph Fey and Larry Albensi, Co-Owners of Maverick Dental Laboratories in Monroeville, PA, never dreamed of moving their lab into a former car dealership, but as soon as they saw the space, they knew it could be repurposed into an ideal laboratory.</p>

                    <p>The 16,000-sq-ft facility offered four main areas to accommodate the various functions of the laboratory and provide ample room for future growth: a huge open garage that would be ideal as the main production room; a spacious showroom with floor-to-ceiling windows for the ceramics, quality control and sales/customer service departments; and management offices and a shipping/receiving area that would be used for those same functions....</p>
                    <a href="/dream-lab-maverick-dental-transforms-car-dealership-into-spacious-modern-laboratory/" class="btn">Read More&nbsp;&nbsp;<i class="fas fa-arrow-right"></i></a>
                </div>
                <hr>
            </div>
            <div class="col-12">
                <div class="blog-list">
                    <h2>Crowns for a Cure</h2>
                    <div class="blog-img">
                        <img src="/img/506afa4573f90-620_465-538x218.png" alt="Crowns for a Cure.">
                    </div>
                    <h5>Mar 7</h5>

                    <p>Maverick Dental Laboratories is Tickled Pink. Maverick is launching a “Crowns for a Cure” campaign during the month of October to help raise awareness and find a cure for breast cancer. As part of the campaign, Maverick has turned their marketing and operating materials pink to coincide with the campaign’s message. Prescriptions, case boxes, invoices, informative box stuffers and even delivery driver’s wardrobes have all transitioned to the familiar campaign color...</p>
                    <a href="/tickled-pink-crowns-for-a-cure/" class="btn">Read More&nbsp;&nbsp;<i class="fas fa-arrow-right"></i></a>
                </div>
                <hr>
            </div>
            <div class="col-12">
                <div class="blog-list">
                    <h2>Driving Patients & Sales</h2>
                    <div class="blog-img">
                        <img src="/img/car-fixed-538x218.png" alt=">Driving Patients & Sales">
                    </div>
                    <h5>Dec 7</h5>
                    <p>Last June when Maverick Dental Lab’s President and Co-Owner Joe Fey and CFO Dale Chandler were number crunching to cut costs, they realized the laboratory could save 20% per year by purchasing its own delivery cars rather than reimbursing its drivers for using their own cars. Owning their own vehicles also gave Chandler–who had a fresh “consumer” perspective thanks to his background in public relations and advertising–another idea. He saw an opportunity to create a marketing campaign that raises dental health awareness and drives patients to the lab’s clients. Through that, the “Follow Me to a Great Dentist” campaign was born...</p>
                    <a href="/driving-patients-sales" class="btn">Read More&nbsp;&nbsp;<i class="fas fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection