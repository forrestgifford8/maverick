@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Digital Dentistry',
    'meta_description' => 'Maverick Dental Laboratory accepts files from all major intraoral scanners and is fully digitally ready with the latest equipment. '
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Digital Dentistry'])
</section>
@endsection

@section('body')
<section id="digital-Dent">
    <div class="d-none d-sm-block dig-Dent-Thumb"></div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6"></div>
            <div class="col-12 col-sm-6 dig-Dent-Text">
                <h2 class="underline">Remote-In Digital Case Consulting</h2>
                <ul>
                    <li><i class="fas fa-check"></i>Dial-In Preferences</li>
                    <li><i class="fas fa-check"></i>Speak with Technician about Scans</li>
                    <li><i class="fas fa-check"></i>Achieve Success with your Scans!</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section id="dig-Perks">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center mb-3">
                <h2>Perks of Digital Impressions</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-6 text-center">
                <img src="/img/Discount.png" class="mb-3" alt="$20 Modeless Discount Icon">
                <h3>$20 Modeless Discount</h3>
            </div>
            <div class="col-12 col-sm-6 text-center">
                <img src="/img/Turnaround.png" class="mb-3" alt="5 Day Turnaround Icon">
                <h3>5 Day Turnaround</h3>
            </div>
        </div>
    </div>
</section>
<section id="dig-Thumbs">
   <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-3">
                <a class="prodLink text-center" href="/send-case/di-submission/">
                    <div class="prodThumb">
                        <img src="/img/3M-True-Definition-Mobile-Scanner.png" alt="3M™ True Definition">
                    </div>
                    <div class="prodTxt">
                        <h3>3M™ True Definition</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <a class="prodLink text-center" href="/send-case/di-submission/">
                    <div class="prodThumb">
                        <img src="/img/3Shape-Trios-Pod-Scanner.png" alt="3Shape TRIOS®">
                    </div>
                    <div class="prodTxt">
                        <h3>3Shape TRIOS®</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <a class="prodLink text-center" href="/send-case/di-submission/">
                    <div class="prodThumb">
                        <img src="/img/Cerec-Sirona-Wand-Scanner.png" alt="CEREC® Scanner">
                    </div>
                    <div class="prodTxt">
                        <h3>CEREC® Scanner</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <a class="prodLink text-center" href="/send-case/di-submission/">
                    <div class="prodThumb">
                        <img src="/img/Carestream-3600-Wand-Scanner.png" alt="Carestream™ CS 3600">
                    </div>
                    <div class="prodTxt">
                        <h3>Carestream™ CS 3600</h3>
                    </div>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-6 col-md-3 offset-md-2">
                <a class="prodLink text-center" href="/send-case/di-submission/">
                    <div class="prodThumb">
                        <img src="/img/iTero-Element2-Scanner.png" alt="iTero® Element 2">
                    </div>
                    <div class="prodTxt">
                        <h3>iTero® Element 2</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <a class="prodLink text-center" href="/send-case/di-submission/">
                    <div class="prodThumb">
                        <img src="/img/medit-i500-Scanner-wand.png" alt="Medit i500">
                    </div>
                    <div class="prodTxt">
                        <h3>Medit i500</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <a class="prodLink text-center" href="/send-case/di-submission/">
                    <div class="prodThumb">
                        <img src="/img/Planmeca-Emerald-Scanner.png" alt="Planmeca® Emerald™">
                    </div>
                    <div class="prodTxt">
                        <h3>Planmeca® Emerald™</h3>
                    </div>
                </a>
            </div>
        </div>
   </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection