@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Videos',
    'meta_description' => 'The Maverick team is all for transparency, which is why we have videos that can give you exclusive insight into how our lab operates. '
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Videos'])
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img class="mb-5" src="/img/8365-Contact-Us-Page-Header.png" alt="Videos Page Header">
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h2>Behind the Scenes at Maverick</h2>
                <p>The Maverick team is all for transparency, which is why we have videos that can give you exclusive insight into how our lab operates.</p> 
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h3>Life of a Case</h3>
               <iframe style="width: 100%;max-width: 640px;height: 360px;" frameborder="0" allow="autoplay; fullscreen" src="https://www.youtube.com/embed/piOGsgbimyk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class="col-md-6">
                <h3>Lab Overview</h3>
                <iframe style="width: 100%;max-width: 640px;height: 360px;" src="https://www.youtube.com/embed/BweRCueZy-s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection