@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Events',
    'meta_description' => 'Generally held in Pittsburgh, our CE events offer informational insight into the changing industry and a change to meet your yearly CE credits requirement. '
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Events'])
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img class="mb-5" src="/img/8365-Events-Page-Header.png" alt="Events Page Header">
            </div>
        </div>
        <div class="row">
            <div class="col-12 mb-5">
               <h2>Stay Up-To-Date with Maverick</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
            <p>Maverick Dental Laboratories is proud to host numerous CE events each year. Generally held in Pittsburgh, our CE events offer informational insight into the changing industry and a chance to meet your yearly CE credit requirement. </p>

            <p>Fill out the form below to join our email list and receive the latest information on upcoming events.</p>
            </div>
            <div class="col-12">
                <div class="form-container">
                    <form id="contact-form" action="">
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input id="first-name" class="form-control" placeholder="First Name" required="required" type="text" />
                        </div>
                         <div class="form-label-group mb-3" style="margin: auto;">
                            <input id="last-name" class="form-control" placeholder="Last Name" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input id="practice-name-contactform" class="form-control" placeholder="Practice Name" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input id="email-contactform" class="form-control" placeholder="Email Address" required="required" type="email" />
                        </div>
                        <input type="hidden" id="public_id" value="7804aba8ed8d4374bc9a997ee47ff53e" />
                        <div class="g-recaptcha" data-sitekey="6LcJX6YUAAAAAPZJgl_qtg2bxp1ZVdBHLzqugYYK"></div>
                        <div class="info-btn">
                            <button type="submit">
                                <div class="info-btn-bx"> @include('_partials.mail-icon')</div>
                                <div class="info-btn-txt">Submit Now</div>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="loader">Loading...</div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var formPending = false;
        $('#contact-form').submit(function(event) {
            event.preventDefault();
            if (formPending)
                return;
            formPending = true;
            $(this).hide();
            $('#contactForm .alert').remove();
            $('.loader').show();
            $.ajax({
                url: 'https://sheikah.amgservers.com/api/contact/7804aba8ed8d4374bc9a997ee47ff53e/188017b05bdd44b28edc66645e82be49',
                method: 'post',
                data: {
                    'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
                    id: $('#public_id').eq(0).val(),
                    practice: $('#practice-name-contactform').eq(0).val(),
                    first_name: $('#first-name').eq(0).val(),
                    Last_name: $('#last-name').eq(0).val(),
                    phone: $('#phone-contactform').eq(0).val(),
                    email: $('#email-contactform').eq(0).val(),
                    message: $('#message-contactform').eq(0).val()
                },
                success: function(data) {
                    $('.loader').hide();
                    $('#contact-form').after('<p>Thank you for contacting us! We\'ll get in touch with you as soon as possible!</p>');
                },
                error: function(data, status, err) {
                    $('.loader').hide();
                    $('#contact-form').show();
                    formPending = false;
                    $('#contact-form button[type="submit"]').before('<div class="alert alert-danger" role="alert">Please fill out all of the fields</div>');
                }
            });
        });
    });
</script>
@endsection