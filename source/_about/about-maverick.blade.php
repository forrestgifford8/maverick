@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'About Maverick',
    'meta_description' => 'Maverick Dental Laboratories was founded in 2003 by Joe Fey and Larry Albensi. As a full-service laboratory, Maverick is committed to providing superior products.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'A Few Words from Our Founders'])
</section>
@endsection

@section('body')
<section class="pt-0">
    <div class="container">
        <div class="row mb-5">
            <div class="col-12 col-sm-6 text-center">
                 <img class="w-100" style="max-width: 450px;" src="/img/Joe-Headshot.png" alt="Joe Fey Headshot">
                 <h3 class="mt-3">Joseph P. Fey</h3>
            </div>
            <div class="col-12 col-sm-6 text-center">
                 <img class="w-100" style="max-width: 450px;" src="/img/Larry-Headshot.png" alt="Larry Albensi Headshot">
                 <h3 class="mt-3">Lawrence Albensi</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p>Having met through mutual clients in the dental industry, Joe Fey and Larry Albensi sparked an uncommon friendship that would take their professional lives in an entirely new direction. At the time, Larry was a Co-Owner of an existing dental laboratory and Joe was a Regional Manager with Brasseler USA. In time, they discovered that they shared the same business values and long-term goals. Most importantly, they had a lot of fun together.</p>
             
                <p>Solid partnerships are hard to come by – and when matched with a timely business opportunity, they can be very successful. This is exactly what we found when we started Maverick Dental Laboratories in 2003. We had a vision to create a national laboratory that would leverage our long-lasting relationships with like-minded dentists to help them achieve success.</p>
                
                <p>Today our laboratory and products company is staffed with a tremendous team of experts and technicians supported by our brilliant customer service, logistics and accounting staff. Each member of our team has been carefully hand selected by the owners for their attitude and skill set. We are always interested in adding individuals who are not only talented, but curious and have a positive demeanor. The Maverick team is a very diverse group, yet each person is uniquely gifted and positioned to play their crucial role in the organization. We are indeed and interesting bunch!</p>
                
                <p>As the dental industry evolves and consolidates, we believe it is critical for dentists to select the right organizations to partner with. Having a great laboratory relationship can lessen your stress level and improve your profitability. We strive to always communicate clearly, resolve issues promptly and work closely with you and your staff to help you deliver the dentistry your patients know and trust.</p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12 col-sm-6 text-center">
                 <img class="w-100 mb-3" style="max-height: 277px;" src="/img/About-Us-Photo-1.png" alt="Joe Fey Headshot">
            </div>
            <div class="col-12 col-sm-6 text-center">
                 <img class="w-100 mb-3" style="max-height: 277px;" src="/img/About-Us-Photo-2.png" alt="Larry Albensi Headshot">
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection