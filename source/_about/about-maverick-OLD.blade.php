@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'About Maverick',
    'meta_description' => 'Maverick Dental Laboratories was founded in 2003 by Joe Fey and Larry Albensi. As a full-service laboratory, Maverick is committed to providing superior products.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'About Maverick'])
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img class="mb-5" src="/img/8365-About-Us-Page-Header.png" alt="About Maverick Page Header">
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h2>Working Together Through the Years</h2>
                <p>After meeting through mutual business in the dental industry, Joe Fey and Larry Albensi sparked a friendship and began referring clients to one another for the years to follow. At the time, Larry was co-owner of an existing dental laboratory and Joe was a regional manager with Brasseler USA. Through their ongoing collaboration, they eventually discovered that they shared the same business values and long-term goals. Most importantly, they had a lot of fun together.</p>
                <p>Solid partnerships are hard to come by – and when matched with business opportunity, they can be very successful. This is exactly what we found when we started Maverick Dental Laboratories in 2003. Our goal was to create a company that could establish long-lasting relationships with like-minded dentists to help them achieve success. With hard-earned savings and home equity loans, we went all in on our new laboratory and haven't looked back since.</p>
                <p>Today our laboratory serves a national clientele and is staffed with a tremendous team of technical experts in Fixed, Removable and Implant Prosthetics. Each member of our staff is carefully hand-selected for their attitude and skill set. Our customer service team is staffed with former dental assistants and office managers who communicate seamlessly with your staff. We dedicate our entire existence to providing excellent restorations to our clients and their patients. In addition to our core laboratory team, Maverick is also made up of our elite VIP Studio team, our MVP DSO team, and Maverick Rotary Instruments™. </p>    
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection