@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Removable',
    'meta_description' => 'Maverick Dental Laboratories offers VIP Services to all our clients who are faced with a case requiring unparalleled esthetics and attention.  '
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Removable'])
</section>
<section id="subNav">
    @include('_partials.removable-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p>Maverick Dental Laboratories offers a wide selection of prosthetics to keep your patient satisfied and your chair time at a minimum. Every single one of our removable prosthetics is fabricated using the latest digital dentistry innovations. We invite you to check out our comprehensive line of removable prosthetics, which includes appliances and nightguards. </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/products/removables/dentures">
                    <div class="prodThumb">
                        <img src="/img/Removable.png" alt="Dentures">
                    </div>
                    <div class="prodTxt">
                        <h3>Dentures</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/products/removables/flex-partials">
                    <div class="prodThumb">
                        <img src="/img/Duraflex-Partial.png" alt="Flexible Partials">
                    </div>
                    <div class="prodTxt">
                        <h3>Flexible Partials</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/products/removables/cast-partials">
                    <div class="prodThumb">
                        <img src="/img/Partial-With-Cast-Full-View.png" alt="Cast Metal Partials">
                    </div>
                    <div class="prodTxt">
                        <h3>Cast Metal Partials</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/products/removables/acrylic-partials">
                    <div class="prodThumb">
                        <img src="/img/FRS-Partial.png" alt="Acrylic Partials">
                    </div>
                    <div class="prodTxt">
                        <h3>Acrylic Partials</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/products/removables/nightguards">
                    <div class="prodThumb">
                        <img src="/img/NightGuard.png" alt="Appliances & Nightguards">
                    </div>
                    <div class="prodTxt">
                        <h3>Appliances & Nightguards</h3>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer-Removable')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection