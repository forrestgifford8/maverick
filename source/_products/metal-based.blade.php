@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Metal-Based',
    'meta_description' => 'Maverick Dental Laboratories offers VIP Services to all our clients who are faced with a case requiring unparalleled esthetics and attention.  '
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Fixed: Metal-Based'])
</section>
<section id="subNav">
    @include('_partials.metalBased-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p>The metal-based restorations from Maverick Dental Laboratories offer long-lasting results you have come to expect from industry standards like full cast and porcelain-fused-to-metal crowns and bridges. Our team utilizes the latest technology to ensure every metal-based restoration is highly precise and perfectly fits in with your patients oral landscape. Our team of technicians expertly design each restoration based off the traditional or digital impressions we receive from your practice. You can be confident that the end result will highly satisfy your patient and help them achieve enduring oral health.</p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/products/metal-based/pfm">
                    <div class="prodThumb">
                        <img src="/img/PFM_3-Unit.png" alt="Porcelain-Fused-to-Metal">
                    </div>
                    <div class="prodTxt">
                        <h3>Porcelain-Fused-to-Metal</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/products/metal-based/maryland-bridge">
                    <div class="prodThumb">
                        <img src="/img/Lava_maryland_bridge.png" alt="Maryland Bridge">
                    </div>
                    <div class="prodTxt">
                        <h3>Maryland Bridge</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/products/metal-based/full-cast">
                    <div class="prodThumb">
                        <img src="/img/FullCast_PosteriorCrown.png" alt="Full Cast">
                    </div>
                    <div class="prodTxt">
                        <h3>Full Cast</h3>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection