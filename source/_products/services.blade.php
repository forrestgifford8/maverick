@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Additional Services',
    'meta_description' => 'Maverick Dental Laboratories offers several value-added services to help you achieve the highest patient satisfaction possible.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Additional Services'])
</section>
<section id="subNav">
    @include('_partials.services-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p>Maverick Dental Laboratories offers several value-added services to help you achieve the highest patient satisfaction possible. Our friendly team of technicians and staff will gladly assist you with every case, because we are committed to remaining your practice's laboratory partner. </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/products/services/reline-repair/">
                    <div class="prodThumb">
                        <img src="/img/Complete-Denture.png" alt="Reline & Repair">
                    </div>
                    <div class="prodTxt">
                        <h3>Reline & Repair</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/products/services/shade-matching/">
                    <div class="prodThumb">
                        <img src="/img/shade-matching.png" alt="Shade Matching">
                    </div>
                    <div class="prodTxt">
                        <h3>Shade Matching</h3>
                    </div>
                </a>
            </div>
<!--
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/products/services/case-planning/">
                    <div class="prodThumb">
                        <img src="/img/Case-Planning.png" alt="Case Planning">
                    </div>
                    <div class="prodTxt">
                        <h3>Case Planning</h3>
                    </div>
                </a>
            </div>
-->
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection