@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'All-Ceramic',
    'meta_description' => 'Maverick Dental Laboratories utilizes their years of expert knowledge to craft high quality all-ceramic restorations.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Fixed: All-Ceramic'])
</section>
<section id="subNav">
    @include('_partials.allCeramic-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p>Maverick Dental Laboratories utilizes their years of expert knowledge to craft high quality all-ceramic restorations. Our all-ceramics are either hand-waxed and pressed in-lab or fabricated using the latest in CAD/CAM technology. The demand for more advanced levels of strength and esthetics in all-ceramic restorations continues to grow. The Maverick team is confident in our ability to continue to meet all of your patient's dental needs with the latest innovations available.</p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/products/all-ceramic/ez-anterior-zirconia/">
                    <div class="prodThumb">
                        <img src="/img/EZ_Anterior-Single.png" alt="EZ Esthetic Anterior Zirconia">
                    </div>
                    <div class="prodTxt">
                        <h3>EZ Esthetic Anterior Zirconia</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/products/all-ceramic/pz-posterior-zirconia/">
                    <div class="prodThumb">
                        <img src="/img/PZ_PosteriorCrown.png" alt="PZ Posterior Zirconia">
                    </div>
                    <div class="prodTxt">
                        <h3>PZ Posterior Zirconia</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/products/all-ceramic/pfz/">
                    <div class="prodThumb">
                        <img src="/img/PFZ.png" alt="Porcelain-Fused-to-Zirconia">
                    </div>
                    <div class="prodTxt">
                        <h3>Porcelain-Fused-to-Zirconia</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/products/all-ceramic/ips/">
                    <div class="prodThumb">
                        <img src="/img/IPS-emax-Anterior.png" alt="IPS e.max®">
                    </div>
                    <div class="prodTxt">
                        <h3>IPS e.max®</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/products/all-ceramic/veneers">
                    <div class="prodThumb">
                        <img src="/img/Veneer.png" alt="Veneers">
                    </div>
                    <div class="prodTxt">
                        <h3>Veneers</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/products/all-ceramic/composite-resin">
                    <div class="prodThumb">
                        <img src="/img/composit-resin.png" alt="Sinfony Composite Resin">
                    </div>
                    <div class="prodTxt">
                        <h3>Sinfony Composite Resin</h3>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection