@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Implants',
    'meta_description' => 'We utilize digital dentistry for every implant restoration we fabricate to ensure every need of your patient is met.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Implant'])
</section>
<section id="subNav">
    @include('_partials.implants-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p>The Maverick implant department offers elegant solutions for the complete range of your implant needs. By sourcing abutments from superior manufacturers, we are able to offer peace of mind, compatibility, and longevity.  Our experienced technicians help you to craft the ideal abutments to achieve proper emergence profile, angulation correction, and aesthetics.   From single-unit screw retained crowns to over-denture cases, we can provide the flexibility and expertise to help you succeed. </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/products/implants/cad-cam">
                    <div class="prodThumb">
                        <img src="/img/Implants.png" alt="Implant Solutions">
                    </div>
                    <div class="prodTxt">
                        <h3>Implant Solutions</h3>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <a class="prodLink" href="/products/implants/src">
                    <div class="prodThumb">
                        <img src="/img/Screw-Retained-Implant-Solution.png" alt="Screw-Retained Crown">
                    </div>
                    <div class="prodTxt">
                        <h3>Screw-Retained Crown</h3>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection