@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Careers',
    'meta_description' => 'Maverick Dental Laboratories is staffed by customer service professionals who will gladly answer any question concerning your case.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Careers'])
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img class="mb-5" src="/img/8365-Careers-Page-Header.png" alt="Contact Us Page Header">
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h2>Join the Maverick Dental Team</h2>
                <p>Dental technology is a perfect blend of art and science. We look to our team to manufacture patient-specific dental prosthetics that ensure satisfaction from both our clients and their patients.</p> 

                <p>We are always looking to grow our fixed, removable, and implant departments. Additionally, we are always looking for the next great technician who can embrace the requirements of the job. The biggest factors we look for in a member of our team are a positive attitude, sound professional ethics, and a curiosity about the dental laboratory profession and industry. Dentistry and technology are rapidly changing; we invite you to grow with us.</p>
                
                <h3>Employee Benefits:</h3>
                <ul>
                    <li>Major Medical and Dental Insurance, Short- and Long-Term Disability, Life Insurance, and Other Voluntary</li>
                    <li>Plans, Including Long-Term Care Insurance</li>
                    <li>Paid Time Off for Full-Time Employees Which Increases with Tenure</li>
                    <li>Career Advancement - We Promote from Within</li>
                    <li>401(k)/Profit-Sharing Plan</li>
                    <li>Clean, Spacious Facility with Cutting-Edge Equipment</li>
                    <li>On-Site Continuous Education Available for Technical Growth</li>
                    <li>Certified Dental Technician (CDT) Expenses Reimbursed</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h3>Apply Today!</h3>
                <p>Maverick Dental Laboratories exclusively posts all dental lab jobs on Indeed.com.</p>
                <a href="https://www.indeed.com/cmp/Maverick-Dental-Laboratories,-LLC/jobs" class="btn" target="_blank">See Job Opportunities</a>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection