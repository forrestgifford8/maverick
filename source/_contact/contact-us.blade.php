@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Contact Us',
    'meta_description' => 'Maverick Dental Laboratories is staffed by customer service professionals who will gladly answer any question concerning your case.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Contact Us'])
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img class="mb-5" src="/img/8365-Contact-Us-Page-Header.png" alt="Contact Us Page Header">
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p>Maverick Dental Laboratories is staffed by customer service professionals who will gladly answer any question concerning your case. Please contact our team through any of the methods below, or fill out the form and a member of our staff will reach out to you.</p>
                <hr>
                <h4 style="color: red;">Maverick Phone Notice</h4>
                <p style="color: red;">Global hacking and ransoms of companies are becoming more prevalent every day. Recently our phone provider (one of the largest in the world) experienced an attack that caused phone issues on a global scale.</p>
                <p style="color: red;">In the event you cannot reach us with our toll free number, please contact us on this non-VoIP number as a backup: <a href="tel:724-396-1750">724-396-1750</a> or email: <a href="mailto:lynn@maverickdental.com">lynn@maverickdental.com</a>.</p>
                <p style="color: red;">Thank you for your patience and understanding.</p>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h4>Maverick Dental Laboratories, LLC</h4>
            </div>
            <div class="col-12 col-md-5">
                <div class="d-flex">
                    <div>
                        <img style="width: 35px;" src="/img/Location-Icon.svg" alt="Location Icon">
                    </div>
                    <div>
                        <p class="ml-2">1615 Golden Mile Highway <br>
                        Monroeville, PA 15146</p>
                    </div>
                </div>
                
                <div class="d-flex">
                    <div>
                        <img style="width: 35px;" src="/img/phone-Icon.svg" alt="Phone Icon">
                    </div>
                    <div>
                        <p class="ml-2"><span class="bold">Phone:</span> 866-294-7444 <br>
                        <span class="bold">Local:</span> 724-733-7444 <br>
                        <span class="bold">Fax:</span> 724-733-7445</p>
                    </div>
                </div>

                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12147.321279059286!2d-79.715104!3d40.434756!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc218ef203ae96306!2sMaverick+Dental+Laboratories!5e0!3m2!1sen!2sus!4v1559240773971!5m2!1sen!2sus" style="width: 100%;max-width: 400px;height: 350px;" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="col-12 col-md-7">
                <div class="form-container">
                    <h4>Contact Form</h4>
                    <form id="contact-form" action="">
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input id="name-contactform" class="form-control" placeholder="Your Name" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input id="practice-name-contactform" class="form-control" placeholder="Practice Name" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input id="phone-contactform" class="form-control" placeholder="Phone Number" required="required" type="tel" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input id="email-contactform" class="form-control" placeholder="Email Address" required="required" type="email" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <textarea id="message-contactform" class="form-control" placeholder="What can we do for you?"></textarea>
                        </div>
                        <input type="hidden" id="public_id" value="7804aba8ed8d4374bc9a997ee47ff53e" />
                        <div class="g-recaptcha" data-sitekey="6LcJX6YUAAAAAPZJgl_qtg2bxp1ZVdBHLzqugYYK"></div>
                        <div class="info-btn">
                            <button type="submit">
                                <div class="info-btn-bx"> @include('_partials.mail-icon')</div>
                                <div class="info-btn-txt">Submit Now</div>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="loader">Loading...</div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var formPending = false;
        $('#contact-form').submit(function(event) {
            event.preventDefault();
            if (formPending)
                return;
            formPending = true;
            $(this).hide();
            $('#contactForm .alert').remove();
            $('.loader').show();
            $.ajax({
                url: 'https://sheikah.amgservers.com/api/contact/7804aba8ed8d4374bc9a997ee47ff53e/137e29d9a81a466f82d7f1a3e666c3e7',
                method: 'post',
                data: {
                    'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
                    id: $('#public_id').eq(0).val(),
                    practice: $('#practice-name-contactform').eq(0).val(),
                    name: $('#name-contactform').eq(0).val(),
                    phone: $('#phone-contactform').eq(0).val(),
                    email: $('#email-contactform').eq(0).val(),
                    message: $('#message-contactform').eq(0).val()
                },
                success: function(data) {
                    $('.loader').hide();
                    $('#contact-form').after('<p>Thank you for contacting us! We\'ll get in touch with you as soon as possible!</p>');
                },
                error: function(data, status, err) {
                    $('.loader').hide();
                    $('#contact-form').show();
                    formPending = false;
                    $('#contact-form button[type="submit"]').before('<div class="alert alert-danger" role="alert">Please fill out all of the fields</div>');
                }
            });
        });
    });
</script>
@endsection