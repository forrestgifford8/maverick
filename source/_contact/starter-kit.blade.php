@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Welcome Kit',
    'meta_description' => 'Are you ready to get started with Maverick Dental Laboratories? Our Welcome Kit contains everything you need to begin building a relationship with our team. '
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Welcome Kit'])
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img class="mb-5" src="/img/8365-Contact-Us-Page-Header.png" alt="Contact Us Page Header">
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p>Are you ready to get started with Maverick Dental Laboratories? Our Welcome Kit contains everything you need to begin building a relationship with our team. The Welcome Kit includes two case boxes, a turn time calendar, Rx forms, and more!</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-container">
                    <h4>Request Welcome Kit Form</h4>
                    <form id="local-pickup-form" action="">
                        <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="doctor-name-pickupform" placeholder="Doctor's Name" required="required" type="text" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="practice-name-pickupform" placeholder="Practice Name" required="required" type="text" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                            <input id="email-contactform" class="form-control" placeholder="Email Address" required="required" type="email" />
                        </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="phone-pickupform" placeholder="Phone Number" required="required" type="text" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="address-pickupform" placeholder="Address" required="required" type="text" />
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-label-group mb-3" style="margin: auto;">
                                        <input class="form-control" id="city-pickupform" placeholder="City" required="required" type="text" />
                                    </div>
                                </div>
                                <div class="col-lg-2 px-lg-0">
                                    <div class="form-label-group mb-3" style="margin: auto;">
                                        <input class="form-control" id="state-pickupform" placeholder="State" required="required" type="text" />
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-label-group mb-3" style="margin: auto;">
                                        <input class="form-control" id="zip-pickupform" placeholder="Zipcode" required="required" type="text" />
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="public_id" value="7804aba8ed8d4374bc9a997ee47ff53e" />
                            <div class="g-recaptcha" data-sitekey="6LcJX6YUAAAAAPZJgl_qtg2bxp1ZVdBHLzqugYYK"></div>
                            <div class="info-btn">
                                <button type="submit">
                                    <div class="info-btn-bx"> @include('_partials.mail-icon')</div>
                                    <div class="info-btn-txt">Submit Now</div>
                                </button>
                            </div>
                        </form>
                    </div>
                <div class="loader">Loading...</div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#local-pickup-form').submit(function(e) {
            e.preventDefault();
            $(this).hide();
            $('#contactForm .alert').remove();
            $('#local-pickup-form ~ .loader').show();
            $.ajax({
                method: 'POST',
                url: 'https://sheikah.amgservers.com/api/contact/7804aba8ed8d4374bc9a997ee47ff53e/bb41150accdd4fa6a276339453aad530',
                data: {
                    'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
                    id: $('#public_id').eq(0).val(),
                    practice: $('#practice-name-pickupform').eq(0).val(),
                    name: $('#doctor-name-pickupform').eq(0).val(),
                    address: $('#address-pickupform').val(),
                    city: $('#city-pickupform').eq(0).val(),
                    state: $('#state-pickupform').eq(0).val(),
                    zip: $('#zip-pickupform').eq(0).val(),
                    phone: $('#phone-pickupform').eq(0).val(),
                    email: $('#email-contactform').eq(0).val()
                },
                success: function(data) {
                    $('#local-pickup-form ~ .loader').hide();
                    $('#btn-delivery-next').show();
                    $('#local-pickup-form').after('<p>Thanks for your request, we will be in touch with you shortly.</p>');
                }, 
                error: function() {

                }
            });
        });
    });
</script>
@endsection