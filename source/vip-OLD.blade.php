@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'VIP Studio',
    'meta_description' => 'Maverick Dental Laboratories offers VIP Services to all our clients who are faced with a case requiring unparalleled esthetics and attention.  '
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'VIP Studio'])
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img class="mb-5" src="/img/8365-VIP-Studio-Page-Header.png" alt="VIP Studio Page Header">
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p>Maverick Dental Laboratories has assembled a team of the most experienced and talented technicians to satisfy the most demanding patient or the most intricate of cases.</p>

                <p>Blending science and artistry, our VIP technicians will collaborate closely with you on each case. We seek to clearly understand your vision of the outcome and will consult closely with you regarding the outcome and obstacles presented in these more challenging cases.</p>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-12 col-md-4">
                <h2>Very Important Patient:</h2> 
                <p>Every Clinician has VIP patients. Family members, other dentists, local celebrities, these are all VIP's that inherently elevate the intensity of treatment. Partner with our most-talented technicians to get it right, the first time. </p>
            </div>
            <div class="col-12 col-md-4">
                <h2>Very Important Prosthetic:</h2> 
                <p>Some patients just do not present with a textbook-like case. Complications can lead to a very challenging path forward for both the clinician and the technician. Whether you are faced with a poorly placed implant, extreme malocclusions, severely worn dentition, or trauma patients, our VIP Studio team has your back. With decades of experience our master technicians can work through the intricacies and options with each case to find the optimal path forward for a successful finish.</p>
            </div>
            <div class="col-12 col-md-4">
                <h2>Very Important Practice:</h2>
                <p>We have clients that use our VIP services for all of their cases. They prefer the enhanced service, interaction and overall synergy brought to all of their cases. These clients like the boutique quality and feel of working with 2-3 technicians all the time. VIP becomes a close partner with them in delivering the consistent quality they demand.</p>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-12 col-md-6">
                <h4>Check List of Items Needed to Complete a Cosmetic Case: </h4>
                <ol>
                    <li>Photos of Pre-Op Teeth with Shade Guide from Different Angles</li>
                    <li>Photos of Preps with Stump Shade Tabs Visible in the Photo</li>
                    <li>Photos of Temporaries, Full Face Including Eyes</li>
                    <li>Photos of Temporaries, Eye to Chin Relaxed Lip Position</li>
                    <li>Model of Pre-Op</li>
                    <li>Model of Temporaries</li>
                    <li>Bite Registration</li>
                    <li>Full Arch Impression and Poured Opposing Model</li>
                    <li>Stick Bite (Vertical and Horizontal Plane Parallel to Interpupillary Line) </li>
                    <li>Detailed Lab Slip Expressing Goals for Your Patient</li>
                    <li>Indicate the Desired Length of Final Restorations</li>
                </ol>
            </div>
            <div class="col-12 col-md-6">
                <h4>For Best Results, Please Include the Following with Your Case: </h4>
                <ul>
                    <li>Both Maxillary and Mandibular Full-arch Preoperative Casts</li>
                    <li>Diagnostic Wax-Ups</li>
                    <li>A Smile Design from a Personalized Smile or a Smile Catalog</li>
                    <li>Bite Registration</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection