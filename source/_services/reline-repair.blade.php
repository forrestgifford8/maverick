@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Reline & Repair',
    'meta_description' => 'Our removable department team utilizes their expert craftsmanship and talent to efficiently reline and repair removable prosthetics.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Additional Services'])
</section>
<section id="subNav">
    @include('_partials.services-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/Complete-Denture.png" alt="Reline & Repair">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 mt-3">
                <h1>Reline & Repair</h1>
                <p>Maverick Dental Laboratories makes it easy to keep your patients comfort and confidence high. Our removable department team utilizes their expert craftsmanship and talent to efficiently reline and repair removable prosthetics to eliminate slippage and discomfort. If bad luck strikes and your patient comes to you with a broken removable, send it to our team to get it quickly repaired. </p>
                <div class="row">
                    <div class="col-12">
                        <a href="/products/removables/" class="btn">View Removable Restorations</a>
                    </div>
                    <div class="col-12">
                        <a href="/contact/contact-us" class="btn">Contact us for Reline & Repair</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $page = window.location.pathname;
        // console.log('Current Page = ' + $page);
        if(!$page) {
            $page = 'index.html';
        }
        $('#productNav-menu div a').each(function(){
            var $href = $(this).attr('href');
            // console.log('URL = ' + $href);
            if ( ($href == $page) || ($href == '') ) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    });
</script>
@endsection