@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Shade Matching',
    'meta_description' => 'Our removable department team utilizes their expert craftsmanship and talent to efficiently reline and repair removable prosthetics.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Additional Services'])
</section>
<section id="subNav">
    @include('_partials.services-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/shade-matching.png" alt="Shade Matching">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 mt-3">
                <h1>Shade Matching</h1>
                <p>The Maverick team is proud to offer highly accurate shade matching for any restoration. Never risk patient satisfaction by guessing the correct shade, instead send your patient to our team for an efficient and comfortable shade match appointment. We offer in-lab shade matching that pairs the artistic eye of our team members with the intricate requirements for unparalleled shades. Our team is committed to being your practice's laboratory partner and we are happy to provide professional and friendly shade matching services to all your patients.</p>
                <img src="/img/Custom-Shade-Room.png" alt="Custom Shade Room">
                <div class="row">
                    <div class="col-12">
                        <a href="/products/all-ceramic/" class="btn">View Our All-Ceramic Restorations</a>
                    </div>
                    <div class="col-12">
                        <a href="/contact/contact-us" class="btn">Contact us for an Appointment</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $page = window.location.pathname;
        // console.log('Current Page = ' + $page);
        if(!$page) {
            $page = 'index.html';
        }
        $('#productNav-menu div a').each(function(){
            var $href = $(this).attr('href');
            // console.log('URL = ' + $href);
            if ( ($href == $page) || ($href == '') ) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    });
</script>
@endsection