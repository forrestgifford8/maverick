@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Case Planning',
    'meta_description' => 'Maverick Dental Laboratories offers in-depth case planning for a wide variety of implant cases.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Additional Services'])
</section>
<section id="subNav">
    @include('_partials.services-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/Case-Planning.png" alt="Case Planning">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 mt-3">
                <h1>Case Planning</h1>
                <p>Maverick Dental Laboratories offers in-depth case planning for a wide variety of implant cases. We understand the importance of being fully informed prior to entering the operatory, which is why we utilize the latest digital advancements to give you valuable insight into your patient's complex case. Our case planning service is paired with a patient-specific surgical guide that ensures predictable, precise, and efficient placement of implants in the operatory. During difficult implant cases, we want to serve as an extension of your practice, so you can rely on the Maverick team throughout the entire planning and execution of the procedure. </p>
                <div class="row">
                    <div class="col-12">
                        <a href="/products/implants/" class="btn">View Our Implant Restorations</a>
                    </div>
                    <div class="col-12">
                        <a href="/contact/contact-us" class="btn">Contact us to Schedule an Appointment</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $page = window.location.pathname;
        // console.log('Current Page = ' + $page);
        if(!$page) {
            $page = 'index.html';
        }
        $('#productNav-menu div a').each(function(){
            var $href = $(this).attr('href');
            // console.log('URL = ' + $href);
            if ( ($href == $page) || ($href == '') ) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    });
</script>
@endsection