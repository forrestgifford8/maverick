@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Maverick is a Preferred Laboratory of Synergy Dental Partners',
    'meta_description' => 'Located in Pittsburgh, PA, Maverick Dental Laboratories has become one of the largest privately-held labs in the nation. With deep capabilities in fixed, removable, implant and cosmetic prosthetics, we are a complete solution for your restorative needs.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Maverick is a Preferred Laboratory of Synergy Dental Partners'])
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img class="mb-5" src="/img/DDS-Synergy-header.png" alt="Maverick DDS Synergy Header">
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="text-center">
                    <h3>Fill out the form below to redeem a FREE PERSONALIZED PREP KIT!</h3>
                    <img src="/img/bur-block-image.png" alt="Bur Block" style="max-width: 400px;margin: 1rem 0;">
                </div>
                <p>This quarter only, Synergy members get a FREE Personalized Crown Prep Kit with their first case sent to Maverick. Simply fill out the form below and you will receive your kit with your completed first case.</p>

                <p>Offer valid through March 31, 2021. For NEW Maverick customers only. One free kit per doctor. Offer can only be redeemed if form is submitted.</p>

                <p>Located in Pittsburgh, PA, Maverick Dental Laboratories has grown to become one of the largest privately-held labs in the nation. With deep capabilities in fixed, removable, implant and cosmetic prosthetics, we are a complete solution for your restorative needs.</p>

                <p>Maverick is excited for the opportunity to work with your practice. We are committed to providing you with seamless communication and exceptional results so that you can succeed with your patients.</p>

                <ul>
                    <li>Average savings of 15-20%</li>
                    <li>Free shipping when you send more than one case in a box</li>
                    <li>Dedicated Customer Service team</li>
                    <li>Rush case capabilities: 3 & 5 day options</li>
                    <li>Wide-ranging implant system acceptance</li>
                    <li>Acceptance of files from all major intra-oral scanners</li>
                </ul>

                <p>Please give our Customer Service team a call at 866-294-7444 if you have any questions or requests.</p>
                
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-container">
                    <h4>Fill out the form below to receive a FREE personalized crown prep kit with your first case:</h4>
                    <form id="local-pickup-form" action="">
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="doctor-name-pickupform" placeholder="Doctor Name" required="required" type="text" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="practice-name-pickupform" placeholder="Practice Name" required="required" type="text" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="address-pickupform" placeholder="Address" required="required" type="text" />
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-label-group mb-3" style="margin: auto;">
                                        <input class="form-control" id="city-pickupform" placeholder="City" required="required" type="text" />
                                    </div>
                                </div>
                                <div class="col-lg-2 px-lg-0">
                                    <div class="form-label-group mb-3" style="margin: auto;">
                                        <input class="form-control" id="state-pickupform" placeholder="State" required="required" type="text" />
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-label-group mb-3" style="margin: auto;">
                                        <input class="form-control" id="zip-pickupform" placeholder="Zipcode" required="required" type="text" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input id="email-contactform" class="form-control" placeholder="Email Address" required="required" type="email" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="phone-pickupform" placeholder="Phone Number" required="required" type="text" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="DI-system-pickupform" placeholder="Digital Impression System" type="text" />
                            </div>
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="engraving" placeholder="How Engraving Should Appear" type="text" />
                            </div>
                            <div class="form-label-group mb-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="sendStarter" id="sendStarter" value="Please send me a starter kit" />
                                    <label class="form-check-label" for="sendStarter">
                                        Please send me a starter kit with supplies to send a case
                                    </label>
                                </div>
                            </div>
                            <input type="hidden" id="public_id" value="7804aba8ed8d4374bc9a997ee47ff53e" />
                            <div class="g-recaptcha" data-sitekey="6LcJX6YUAAAAAPZJgl_qtg2bxp1ZVdBHLzqugYYK"></div>
                            <div class="info-btn">
                                <button type="submit">
                                    <div class="info-btn-bx"> @include('_partials.mail-icon')</div>
                                    <div class="info-btn-txt">Submit Now</div>
                                </button>
                            </div>
                        </form>
                    </div>
                <div class="loader">Loading...</div>
            </div>
        </div>
    </div>
</section>
<div id="sub-footer" style="margin-top: 3rem !important">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 text-center">
                <a href="/send-case/new-doctor/" class="btn-white">Send a Case</a>
            </div>
            <div class="col-12 col-md-6 text-center">
                <a href="/send-case/di-submission/" class="btn-white">Digital Dentistry</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#local-pickup-form').submit(function(e) {
            e.preventDefault();
            $(this).hide();
            $('#contactForm .alert').remove();
            $('#local-pickup-form ~ .loader').show();
            $.ajax({
                method: 'POST',
                url: 'https://sheikah.amgservers.com/api/contact/7804aba8ed8d4374bc9a997ee47ff53e/f33acf8192e149ba9040a82c153cfb46',
                data: {
                    'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
                    id: $('#public_id').eq(0).val(),
                    practice: $('#practice-name-pickupform').eq(0).val(),
                    name: $('#doctor-name-pickupform').eq(0).val(),
                    address: $('#address-pickupform').val(),
                    engraving: $('#engraving').val(),
                    diSystem: $('#DI-system-pickupform').val(),
                    city: $('#city-pickupform').eq(0).val(),
                    state: $('#state-pickupform').eq(0).val(),
                    zip: $('#zip-pickupform').eq(0).val(),
                    phone: $('#phone-pickupform').eq(0).val(),
                    email: $('#email-contactform').eq(0).val(),
                    sendStarter: $('input[name="sendStarter"]').eq(0).val()
                },
                success: function(data) {
                    $('#local-pickup-form ~ .loader').hide();
                    $('#btn-delivery-next').show();
                    $('#local-pickup-form').after('<p>Thanks for your request, we will be in touch with you shortly.</p>');
                }, 
                error: function() {

                }
            });
        });
    });
</script>
@endsection