@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Porcelain-Fused-to-Metal',
    'meta_description' => 'The Maverick team fabricates high-quality porcelain-fused-to-metal restorations through the use of the latest CAD/CAM technology.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Metal-Based'])
</section>
<section id="subNav">
    @include('_partials.metalBased-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/PFM_3-Unit.png" alt="Porcelain-Fused-to-Metal">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 align-self-center">
                <h2>Porcelain-Fused-to-Metal</h2>
                <p>The Maverick team fabricates high-quality porcelain-fused-to-metal restorations through the use of the latest CAD/CAM technology. Crafted by carefully layering porcelain to an underlying substructure of quality semi-precious, white high noble, or yellow noble alloy, we expertly bond the two layers together to completely eliminate the chance of unflattering discoloration. The metal provides stability and strength, which makes it a strong contender against crowns made from alternative materials. The porcelain provides the restored tooth with a lifelike appearance. These crowns are still a popular option due to their high-rate of clinical success and their biocompatibility that helps ensure continued periodontal health. PFMs are also gentle on opposing dentition. The use of CAD/CAM technology allows our techs to fabricate this traditional solution with extreme precision.</p>
                <h3>Features:</h3>
                <ul>
                    <li>Gentle on Opposing Dentition</li>
                    <li>Biocompatible</li>
                    <li>Bonded Porcelain Improves Esthetics</li>
                    <li>Expertly Designed</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div id="accordion">
                    <h3>Indications</h3>
                    <div>
                        <p>Our PFMs can be used for crowns and bridges (up to fourteen units). PFMs can be manufactured to nonprecious, semiprecious, and yellow high noble copings and can be used in conjunction with cast partials and implants.</p>
                    </div>
                    <h3>Contraindications</h3>
                    <div>
                        <p>Contraindicated when the patient has a metal allergy or when the size of the tooth pulp is negligibly smaller, thus compromising the tooth preparation process. It is also contraindicated when the clinical tooth crown is very short and lacks the required stability including retention that is enough to provide the space for porcelain and metal.</p>
                    </div>
                    <h3>Preparation</h3>
                    <div>
                        <p>The ideal preparation for PFMs is a chamfer margin preparation. If a porcelain labial margin is prescribed, then a shoulder margin preparation is required.</p>
                    </div>
                    <h3>Cementation</h3>
                    <div>
                        <ul>
                            <li>Panavia 21 – tin plated</li>
                            <li>Glass ionomer cement (GC Fuji, GC America)</li>
                            <li>Zinc Phosphate Polycarboxylate</li>
                            <li>Resin Ionomer cement (RelyX, 3M ESPE)</li>
                        </ul>
                    </div>
                    <h3>Tech Notes</h3>
                    <div>
                        <p>If an adjustment is required on the ceramic, use a fine diamond with water and air to keep the crown cool. To contour the ceramic, polish with a pink rubber wheel and diamond polishing paste (Maverick Rotary).</p>
                    </div>
                    <h3>Codes</h3>
                    <div>
                        <ul>
                            <li>D2750 Crown Porcelain fused to high noble</li>
                            <li>D2751 Crown Porcelain fused to nonprecious</li>
                            <li>D2752 Crown Porcelain fused to semiprecious</li>
                            <li>D6750 Crown Porcelain fused to high noble (bridge units)</li>
                            <li>D6751 Crown Porcelain fused to nonprecious (bridge units)</li>
                            <li>D6752 Crown Porcelain fused to semiprecious (bridge units)</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $page = window.location.pathname;
        // console.log('Current Page = ' + $page);
        if(!$page) {
            $page = 'index.html';
        }
        $('#productNav-menu div a').each(function(){
            var $href = $(this).attr('href');
            // console.log('URL = ' + $href);
            if ( ($href == $page) || ($href == '') ) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    });
</script>
@endsection