@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Full Cast',
    'meta_description' => 'The Maverick team fabricates high-quality Full Cast restorations through the use of the latest CAD/CAM technology.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Metal-Based'])
</section>
<section id="subNav">
    @include('_partials.metalBased-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                        <img src="/img/FullCast_PosteriorCrown.png" alt="Full Cast">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 align-self-center">
                <h2>Full Cast</h2>
                <p>Maverick Dental Laboratories offers full cast crowns, bridges, inlays, and onlays that exhibit wonderful contours and exquisite attention to detail. Our cast restorations are available in high-quality and biocompatible alloys that help preserve the oral health of your patient. Thanks to their clinical success, biocompatibility, and ease of use, these restorations have withstood the test of time and still remain a popular choice. Through the use of CAD/CAM technology, our technicians are able to fabricate these industry staples with superior fit and finish than before. They are durable, but remain gentle on opposing dentition, which makes these an excellent option for bruxers.</p>
                <h3>Features:</h3>
                <ul>
                    <li>Biocompatible</li>
                    <li>Easy to Use</li>
                    <li>Gentle on Opposing Dentition</li>
                    <li>Ideal for Posterior Placements</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div id="accordion">
                    <h3>Indications</h3>
                    <div>
                        <p>Full-cast gold crowns are indicated for crowns, veneers, inlays, onlays, and bridges.</p>
                    </div>
                    <h3>Contraindications</h3>
                    <div>
                        <p>Full-cast gold crowns are contraindicated for partials and implants.</p>
                    </div>
                    <h3>Preparation</h3>
                    <div>
                        <p>Inlays and onlays can also be fabricated as a full-cast restoration. Feather-edge margin preparations are indicated for full-cast restorations, but any margin preparation may be used.</p>
                    </div>
                    <h3>Cementation</h3>
                    <div>
                        <ul>
                            <li>Panavia 21 (Must be tin plated if precious metal is used)</li>
                            <li>Glass ionomer cement (GC Fuji, GC America)</li>
                            <li>Zinc Phosphate Polycarboxylate Resin Ionomer cement (RelyX, 3M ESPE)</li>
                        </ul>
                    </div>
                    <h3>Tech Notes</h3>
                    <div>
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <p>All castings are made with a metal alloy, be it non-precious, semi-precious or precious metals. Alloys are classified by their content.</p>
                                <ul>
                                    <li>Base – contents include non-precious, Chrome Cobalt or Titanium</li>
                                    <li>Noble – consists of 25 percent precious alloy</li>
                                    <li>High Noble – consists of 60 percent precious metal with at least 40 percent being gold</li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6">
                                <p>Alloy type refers to the hardness and/or softness of the material.</p>
                                <ul>
                                    <li>Type I – Extra soft</li>
                                    <li>Type II – Soft</li>
                                    <li>Type III– Hard</li>
                                    <li>Type IV – Extra Hard (Rigid)</li>
                                    <li>Non-Precious, Noble 20, White High Noble – Type IV – Very hard and rigid. These crowns are more difficult to adjust and re-polish than alloys with a high gold content.</li>
                                    <li>Full Cast 40 – Type III – Yellow high noble alloy. Brand name currently used is Argenco 40 HN.</li>
                                    <li>Full Cast 52 HN – Type III – Yellow high noble alloy. Brand name currently used is Argenco 52.</li>
                                    <li>Full Cast 75- Type III – Yellow high noble and is an upgrade from full cast 52. The gold is slightly more yellow in color. Brand name currently used Argenco 75.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <h3>Codes</h3>
                    <div>
                        <ul>
                            <li>D2790 Crown Full-Cast High Noble Metal</li>
                            <li>D2791 Crown Full-Cast Predominantly Base Metal</li>
                            <li>D2792 Crown Full-Cast Noble Metal</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $page = window.location.pathname;
        // console.log('Current Page = ' + $page);
        if(!$page) {
            $page = 'index.html';
        }
        $('#productNav-menu div a').each(function(){
            var $href = $(this).attr('href');
            // console.log('URL = ' + $href);
            if ( ($href == $page) || ($href == '') ) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    });
</script>
@endsection