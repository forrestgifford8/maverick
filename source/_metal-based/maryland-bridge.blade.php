@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Maryland Bridge',
    'meta_description' => 'Fabricated with the same level of expertise and focus as our PFM restorations, Maryland bridges offer high strength and improved esthetics.'
    ])
@endsection

@section('header')
<section id="defualt-header">
    @include('_partials.nav-menu')
</section>
<section id="page-header">
    @include('_partials.page-header', ['page_title' => 'Metal-Based'])
</section>
<section id="subNav">
    @include('_partials.metalBased-Menu')
</section>
@endsection

@section('body')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="prodPage">
                    <div class="prodThumb">
                    <img src="/img/Lava_maryland_bridge.png" alt="Maryland Bridge">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 align-self-center">
                <h2>Maryland Bridge</h2>
                <p>The Maryland bridge restoration available from Maverick Dental Laboratories offers a variety of benefits. Fabricated with the same level of expertise and focus as our porcelain-fused-to-metal restorations, these bridges offer high strength and improved esthetics. We utilize the latest CAD/CAM technology to ensure the bridge will fit perfectly in your patient's mouth. Compared to alternative bridge solutions, the Maryland bridge reduces the impact of the placement on the adjacent teeth. Instead of affixing the pontic to two crowns that will go over healthy teeth on either side, this solution utilizes wings that are bonded to the teeth.</p>
                <h3>Benefits:</h3>
                <ul>
                    <li>Also Available as a PFZ or Zinfony™ Composite-Reinforced</li>
                    <li>Reduced Interference with Adjacent Teeth</li>
                    <li>High Strength</li>
                    <li>Economical Compared to Alternatives</li>
                </ul>
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-12">
                <div id="accordion">
                    <h3>Indications</h3>
                    <div>
                        <p>Our PFMs can be used for crowns and bridges (up to fourteen units). PFMs can be manufactured to nonprecious, semiprecious, and yellow high noble copings and can be used in conjunction with cast partials and implants.</p>
                    </div>
                    <h3>Contraindications</h3>
                    <div>
                        <p>Contraindicated when the patient has a metal allergy or when the size of the tooth pulp is negligibly smaller, thus compromising the tooth preparation process. It is also contraindicated when the clinical tooth crown is very short and lacks the required stability including retention that is enough to provide the space for porcelain and metal.</p>
                    </div>
                    <h3>Preparation</h3>
                    <div>
                        <p>The ideal preparation for PFMs is a chamfer margin preparation. If a porcelain labial margin is prescribed, then a shoulder margin preparation is required.</p>
                    </div>
                    <h3>Cementation</h3>
                    <div>
                        <ul>
                            <li>Panavia 21 – tin plated</li>
                            <li>Glass ionomer cement (GC Fuji, GC America)</li>
                            <li>Zinc Phosphate Polycarboxylate</li>
                            <li>Resin Ionomer cement (RelyX, 3M ESPE)</li>
                        </ul>
                    </div>
                    <h3>Tech Notes</h3>
                    <div>
                        <p>If an adjustment is required on the ceramic, use a fine diamond with water and air to keep the crown cool. To contour the ceramic, polish with a pink rubber wheel and diamond polishing paste (Brasseler, Shofu, Vident).</p>
                    </div>
                    <h3>Codes</h3>
                    <div>
                        <ul>
                            <li>D2750 Crown Porcelain fused to high noble</li>
                            <li>D2751 Crown Porcelain fused to nonprecious</li>
                            <li>D2752 Crown Porcelain fused to semiprecious</li>
                            <li>D6750 Crown Porcelain fused to high noble (bridge units)</li>
                            <li>D6751 Crown Porcelain fused to nonprecious (bridge units)</li>
                            <li>D6752 Crown Porcelain fused to semiprecious (bridge units)</li>
                        </ul>
                    </div>
                </div>
            </div> 
        </div> -->
    </div>
</section>
@include('_partials.sub-footer')
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $page = window.location.pathname;
        // console.log('Current Page = ' + $page);
        if(!$page) {
            $page = 'index.html';
        }
        $('#productNav-menu div a').each(function(){
            var $href = $(this).attr('href');
            // console.log('URL = ' + $href);
            if ( ($href == $page) || ($href == '') ) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    });
</script>
@endsection