<div class="container">
    <div id="productNav">
        <div id="productNav-menu" class="row">
            <div class="col-12 col-sm-6 text-center"><a href="/products/implants/cad-cam/">Implant Solutions</a></div>
            <div class="col-12 col-sm-6 text-center"><a href="/products/implants/src/">Screw-Retained Crown</a></div>
        </div>
    </div>
    <div class="row"><div class="col-12"><div class="subBorder"></div></div></div>
</div>