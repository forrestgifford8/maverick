<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 72 72" style="enable-background:new 0 0 72 72;" xml:space="preserve">
<g>
	<g class="stepsIcon" transform="translate(50 50) scale(0.69 0.69) rotate(0) translate(-50 -50)">
		<path class="st0" d="M29.7-18.1c-26.3,0-47.8,21.5-47.8,47.8S3.4,77.5,29.7,77.5s47.8-21.5,47.8-47.8S56.1-18.1,29.7-18.1z
			 M27.9,50.4L-0.7,39.1V23.3l28.6,11.5C27.9,34.8,27.9,50.4,27.9,50.4z M29.3,32.9L0.5,21.2L29.3,9.6l28.8,11.7L29.3,32.9z
			 M59.2,39L30.6,50.4V34.8l28.7-11.5V39z"/>
	</g>
</g>
</svg>