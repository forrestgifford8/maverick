<div id="sub-footer" class="p-4">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <img src="/img/8365-Fixed-CTA-Pg-Footer-Thumb-V1.png" alt="Shade Restoration CTA">
            </div>
            <div class="col-12 col-md-6 d-flex">
                <div class="align-self-center">
                    <h1>Interested in an Expertly Shaded Restoration?</h1>
                    <p>We offer in-office and in-lab shade matching services to all of our local clinicians. Ensure patient satisfaction with every smile you place. </p>
                    <a href="/products/services/shade-matching/" class="btn-white">Learn More</a>
                </div>
            </div>
        </div>
    </div>
</div>