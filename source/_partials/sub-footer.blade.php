<div id="sub-footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7">
                <h1>Need Supplies?</h1>
                <p>Our dental lab supplies include dental lab prescription forms, bio-bags, UPS labels, and case boxes.</p>
            </div>
            <div class="col-12 col-md-5 d-flex">
                <div class="align-self-center">
                    <a href="/send-case/request-supplies/" class="btn-white">ORDER NOW</a>
                </div>
            </div>
        </div>
    </div>
</div>