<div id="sub-footer" class="p-4">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <img src="/img/8365-Implants-CTA-Pg-Footer-Thumb-V1.png" alt="Implants CTA">
            </div>
            <div class="col-12 col-md-6 d-flex">
                <div class="align-self-center">
                    <h1>Interested in a Predictable Implant Surgery?</h1>
                    <p>Learn more about our surgical implant guide and case planning service, which helps ensure every implant surgery is predictable and efficient.</p>
                    <a href="/products/services/case-planning/" class="btn-white">Learn More</a>
                </div>
            </div>
        </div>
    </div>
</div>