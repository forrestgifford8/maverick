<div id="sub-footer" class="p-4">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <img src="/img/8365-Removable-CTA-Pg-Footer-Thumb-V1.png" alt="Removable CTA">
            </div>
            <div class="col-12 col-md-6 d-flex">
                <div class="align-self-center">
                    <h1>Are You Faced with an Ill-Fitting or Broken Denture?</h1>
                    <p>The Maverick team is happy to provide quick and efficient reline or repair services to all our clinicians. Get you patient smiling again today!</p>
                    <a href="/products/services/reline-repair/" class="btn-white">Learn More</a>
                </div>
            </div>
        </div>
    </div>
</div>