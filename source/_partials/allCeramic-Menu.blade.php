<div class="container">
    <div id="productNav" style="width: 100%;">
        <div id="productNav-menu" class="row">
            <div class="col-12 col-sm-2 text-center"><a href="/products/all-ceramic/ez-anterior-zirconia/">EZ Esthetic Anterior Zirconia</a></div>
            <div class="col-12 col-sm-2 text-center"><a href="/products/all-ceramic/pz-posterior-zirconia/">PZ Posterior Zirconia</a></div>
            <div class="col-12 col-sm-2 text-center"><a href="/products/all-ceramic/pfz/">Porcelain-Fused-to-Zirconia</a></div>
            <div class="col-12 col-sm-2 text-center"><a href="/products/all-ceramic/ips/">IPS e.max®</a></div>
            <div class="col-12 col-sm-2 text-center"><a href="/products/all-ceramic/veneers/">Veneers</a></div>
            <div class="col-12 col-sm-2 text-center"><a href="/products/all-ceramic/composite-resin/">Sinfony Composite Resin</a></div>
        </div>
    </div>
    <div class="row"><div class="col-12"><div class="subBorder"></div></div></div>
</div>