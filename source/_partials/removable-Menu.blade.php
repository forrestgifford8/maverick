<div class="container">
    <div id="productNav" style="width: 100%;">
        <div id="productNav-menu" class="row">
            <div class="col-12 offset-md-1 col-sm-2 text-center"><a href="/products/removables/dentures/">Dentures</a></div>
            <div class="col-12 col-sm-2 text-center"><a href="/products/removables/flex-partials/">Flexible Partials</a></div>
            <div class="col-12 col-sm-2 text-center"><a href="/products/removables/cast-partials/">Cast Metal Partials</a></div>
            <div class="col-12 col-sm-2 text-center"><a href="/products/removables/acrylic-partials/">Acrylic Partials</a></div>
            <div class="col-12 col-sm-2 text-center"><a href="/products/removables/nightguards/">Appliances & Nightguards</a></div>
        </div>
    </div>
    <div class="row"><div class="col-12"><div class="subBorder"></div></div></div>
</div>