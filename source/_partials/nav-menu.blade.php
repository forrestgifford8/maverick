<div id="top-Nav" class="top-nav">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex justify-content-end">
                <a href="/send-case/new-doctor">
                    <div class="d-flex">
                        <div>@include('_partials.newDoc-icon')</div>
                        <div class="align-self-center d-none d-sm-block">New Doctor</div>
                    </div>
                </a>
                <a href="/about/digital-dentistry/">
                    <div class="d-flex">
                        <div>@include('_partials.digDentistry-Icon')</div>
                        <div class="align-self-center d-none d-sm-block">Digital Dentistry</div>
                    </div>
                </a>
                <a href="/send-case/request-supplies/">
                    <div class="d-flex">
                        <div>@include('_partials.upload-icon')</div>
                        <div class="align-self-center d-none d-sm-block">Request Supplies</div>
                    </div>
                </a>
<!--
                <a href="">
                    <div class="d-flex">
                        <div>@include('_partials.drLogin-icon')</div>
                        <div class="align-self-center d-none d-sm-block">Doctor Login</div>
                    </div>
                </a>
-->
            </div>
        </div>
    </div>
</div>
<div class="primary-nav">
    <div class="container">
        <div class="row">
            <nav id="primary-nav" class="col-12 d-flex justify-content-between">
                <div id="header-logo">
                    <a href="/"><img src="/img/Mav_White_Gold_logo.svg" alt="Maverick Dental Laboratory Logo"></a>
                </div>
                <ul class="primary-nav-menu list-unstyled justify-content-around align-self-center d-none d-lg-flex">
                    <li>
                       <a href=""  data-target="#about-collapse" aria-expanded="false" data-toggle="collapse"><span class="h-effect"></span><span class="nav-link-txt">About</span><div class="sub-tri"></div></a>
                        <ul class="collapse sub-menu" id="about-collapse" data-parent="#primary-nav">
                            <li><a href="/about/about-maverick">About Maverick</a></li>
                            <li><a href="/about/digital-dentistry">Digital Dentistry</a></li>
                            <li><a href="/about/testimonials">Testimonials</a></li>
                            <li><a href="/about/blog">Blog</a></li>
                            <li><a href="/about/events">Events</a></li>
                            <li><a href="/about/videos">Videos</a></li>
                        </ul>  
                    </li>
                    <li>
                        <a href=""  data-target="#products-collapse" aria-expanded="false" data-toggle="collapse"><span class="h-effect"></span><span class="nav-link-txt">Products & Services</span><div class="sub-tri"></div></a>
                        <ul class="collapse sub-menu" id="products-collapse" data-parent="#primary-nav">
                            <li><a href="/products/metal-based">Fixed: Metal-Based</a></li>
                            <li><a href="/products/all-ceramic">Fixed: All-Ceramic</a></li>
                            <li><a href="/products/removables">Removable</a></li>
                            <li><a href="/products/implants">Implant</a></li>
                            <li><a href="/products/services">Additional Services</a></li>
                            <li><a href="/about/digital-dentistry/">Digital Dentistry</a></li>
                        </ul> 
                    </li>
                    <li><a href="/vip"><span class="h-effect"></span><span class="nav-link-txt">VIP Studio</span></a></li>
                    <li><a href="/rotary-instruments"><span class="h-effect"></span><span class="nav-link-txt">Rotary Instruments</span></a></li>
                    <li><a href="/dso-services"><span class="h-effect"></span><span class="nav-link-txt">DSO Services</span></a></li>
                    <li>
                        <a href=""  data-target="#send-case-collapse" aria-expanded="false" data-toggle="collapse"><span class="h-effect"></span><span class="nav-link-txt">Send a Case</span><div class="sub-tri"></div></a>
                        <ul class="collapse sub-menu" id="send-case-collapse" data-parent="#primary-nav">
                            <li><a href="/send-case/new-doctor">New Doctor</a></li>
                            <li><a href="/send-case/di-submission">Send a Digital Case</a></li>
                            <li><a href="/resources/">Rx Forms</a></li>
                            <li><a href="/send-case/upload-file">Upload Case Photos</a></li>
                            <li><a href="/send-case/local-pickup">Local Pickup</a></li>
                            <li><a href="/send-case/print-ups-label">Shipping Label Generator</a></li>
<!--                            <li><a href="/send-case/case-scheduler">Case Scheduler</a></li>-->
                            <li><a href="/send-case/request-supplies">Request Supplies</a></li>
                        </ul>
                    </li>
                    <li><a href="/resources"><span class="h-effect"></span><span class="nav-link-txt">Resources</span></a></li>
                    <li>
                        <a href="" class="pr-0" data-target="#contact-collapse" aria-expanded="false" data-toggle="collapse"><span class="h-effect"></span><span class="nav-link-txt">Contact</span><div class="sub-tri"></div></a>
                        <ul class="collapse sub-menu" id="contact-collapse" data-parent="#primary-nav">
                            <li><a href="/contact/contact-us">Contact Us</a></li>
                            <li><a href="/contact/starter-kit">Welcome Kit</a></li>
                            <li><a href="/contact/careers">Careers</a></li>
                        </ul>
                    </li>
                </ul>
                <div class="d-flex d-lg-none">
                    <img id="mobile-nav-icon" src="/img/gold-hamburger-Icon.png" alt="Hamburger Mobile Icon">
                </div>
            </nav>
        </div>
    </div>
</div>
<div id="primary-nav-mobile" class="d-lg-none">
    <div id="mobile-close-icon"><span>X</span></div>
    <ul id="primary-nav-mobile-menu" class="list-unstyled justify-content-around align-self-center d-block d-lg-none">
        <li>
            <a href=""  data-target="#about-collapse-m" aria-expanded="false" data-toggle="collapse"><span class="h-effect"></span><span class="nav-link-txt">About</span><div class="sub-tri"></div></a>
            <ul class="collapse sub-menu" id="about-collapse-m" data-parent="#primary-nav-mobile-menu">
                <li><a href="/about/about-maverick">About Maverick</a></li>
                <li><a href="/about/digital-dentistry">Digital Dentistry</a></li>
                <li><a href="/about/testimonials">Testimonials</a></li>
                <li><a href="/about/blog">Blog</a></li>
                <li><a href="/about/events">Events</a></li>
                <li><a href="/about/videos">Videos</a></li>
            </ul>  
        </li>
        <li>
            <a href=""  data-target="#products-collapse-m" aria-expanded="false" data-toggle="collapse"><span class="h-effect"></span><span class="nav-link-txt">Products & Services</span><div class="sub-tri"></div></a>
            <ul class="collapse sub-menu" id="products-collapse-m" data-parent="#primary-nav-mobile-menu">
                <li><a href="/products/metal-based">Fixed: Metal-Based</a></li>
                <li><a href="/products/all-ceramic">Fixed: All-Ceramic</a></li>
                <li><a href="/products/removables">Removable</a></li>
                <li><a href="/products/implants">Implant</a></li>
                <li><a href="/products/services">Additional Services</a></li>
            </ul> 
        </li>
        <li><a href="/vip"><span class="h-effect"></span><span class="nav-link-txt">VIP Studio</span></a></li>
        <li><a href="https://maverickrotary.com/" target="_blank"><span class="h-effect"></span><span class="nav-link-txt">Rotary Instruments</span></a></li>
        <li><a href="/dso-services"><span class="h-effect"></span><span class="nav-link-txt">DSO Services</span></a></li>
        <li>
            <a href=""  data-target="#send-case-collapse-m" aria-expanded="false" data-toggle="collapse"><span class="h-effect"></span><span class="nav-link-txt">Send a Case</span><div class="sub-tri"></div></a>
            <ul class="collapse sub-menu" id="send-case-collapse-m" data-parent="#primary-nav-mobile-menu">
                <li><a href="/send-case/new-doctor">New Doctor</a></li>
                <li><a href="/send-case/di-submission">Send a Digital Case</a></li>
                <li><a href="/resources/">Rx Forms</a></li>
                <li><a href="/send-case/upload-file">Upload a File</a></li>
                <li><a href="/send-case/local-pickup">Local Pickup</a></li>
                <li><a href="/send-case/print-ups-label">Shipping Label Generator</a></li>
<!--                <li><a href="/send-case/case-scheduler">Case Scheduler</a></li>-->
                <li><a href="/send-case/request-supplies">Request Supplies</a></li>
            </ul>
        </li>
        <li><a href="/resources"><span class="h-effect"></span><span class="nav-link-txt">Resources</span></a></li>
        <li>
            <a href="" class="pr-0" data-target="#contact-collapse-m" aria-expanded="false" data-toggle="collapse"><span class="h-effect"></span><span class="nav-link-txt">Contact</span><div class="sub-tri"></div></a>
            <ul class="collapse sub-menu" id="contact-collapse-m" data-parent="#primary-nav-mobile-menu">
                <li><a href="/contact/contact-us">Contact Us</a></li>
                <li><a href="/contact/starter-kit">Starter Kit</a></li>
                <li><a href="/contact/careers">Careers</a></li>
            </ul>
        </li>
    </ul>
</div>