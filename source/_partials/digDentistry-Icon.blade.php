<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 72 72" style="enable-background:new 0 0 72 72;" xml:space="preserve">
<g>
	<g>
		<circle class="top-nav-icon" cx="25.4" cy="49.2" r="4"/>
		<circle class="top-nav-icon" cx="25.4" cy="22.8" r="4"/>
		<circle class="top-nav-icon" cx="46.5" cy="22.8" r="4"/>
		<path class="top-nav-icon" d="M36,3C17.8,3,3,17.8,3,36s14.8,33,33,33s33-14.8,33-33S54.2,3,36,3z M32,49.2c0,3.6-3,6.6-6.6,6.6
			s-6.6-3-6.6-6.6c0-3.2,2.3-5.9,5.3-6.5V29.3c-3-0.6-5.3-3.3-5.3-6.5c0-3.6,3-6.6,6.6-6.6s6.6,3,6.6,6.6c0,3.2-2.3,5.9-5.3,6.5
			v13.5C29.7,43.4,32,46,32,49.2z M46.5,29.4c-1.3,0-2.6-0.4-3.6-1.1l-5.6,5.6v7.4c0,0.7-0.6,1.3-1.3,1.3s-1.3-0.6-1.3-1.3v-7.9
			c0-0.3,0.1-0.7,0.4-0.9l6-6c-0.7-1-1.1-2.3-1.1-3.6c0-3.6,3-6.6,6.6-6.6s6.6,3,6.6,6.6S50.2,29.4,46.5,29.4z"/>
	</g>
</g>
</svg>