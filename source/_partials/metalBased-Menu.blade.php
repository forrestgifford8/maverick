<div class="container">
    <div id="productNav">
        <div id="productNav-menu" class="row">
            <div class="col-12 col-sm-4 text-center"><a href="/products/metal-based/pfm/">Porcelain-Fused-to-Metal</a></div>
            <div class="col-12 col-sm-4 text-center"><a href="/products/metal-based/maryland-bridge/">Maryland Bridge</a></div>
            <div class="col-12 col-sm-4 text-center"><a href="/products/metal-based/full-cast/">Full Cast</a></div>
        </div>
    </div>
    <div class="row"><div class="col-12"><div class="subBorder"></div></div></div>
</div>