<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 72 72" style="enable-background:new 0 0 72 72;" xml:space="preserve">
<g>
	<g transform="translate(50 50) scale(0.69 0.69) rotate(0) translate(-50 -50)">
		<g>
			<path class="top-nav-icon" d="M29.7-18.1c-26.4,0-47.8,21.4-47.8,47.8S3.3,77.5,29.7,77.5s47.8-21.4,47.8-47.8S56.1-18.1,29.7-18.1z
				 M48.4,33.3h-15v15h-7.3v-15h-15v-7.3h15v-15h7.3v15h15V33.3z"/>
		</g>
	</g>
</g>
</svg>