<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        @yield('meta')
        <meta name="robots" content="noydir" />
        <meta http-equiv="Cache-Control" CONTENT="no-cache">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
        <link rel="icon" type="image/png" href="/img/8365-Favicon.png">
        <link rel="stylesheet" href="/assets/css/main.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet"> 
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    </head>
    <body id="body" class="p-{{ $page->getFilename() }}">
        <div id="body-wrap">
            <header>
                @yield('header')
            </header>
            <main role="main">
                @yield('body')
            </main>
            <footer>
                <div class="container">
                    <div class="row mb-3">
                        <div class="col-md-6 col-lg-4 col-xl-3">
                            <a href="/#">
                                <img class="footer-logo" src="/img/Mav_White_Gold_logo.svg" alt="Maverick Dental Laboratory Logo">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-8 col-xl-9 align-self-center">
                            <div class="foot-line"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-3">
                            <address>
                                1615 Golden Mile Highway <br>
                                Monroeville, PA 15146 <br>
                                Phone: 866-294-7444  |  724-733-7444 <br>
                                Fax: 724-733-7445 
                            </address>
                            <div class="d-flex">
                                <div class="footIcons">
                                    <a href="https://www.facebook.com/MaverickDentalLab/" target="_blank">
                                        @include('_partials.fb-icon')
                                    </a>
                                </div>
                                <div class="footIcons">
                                    <a href="https://www.linkedin.com/company/maverick-dental-laboratories" target="_blank">
                                        @include('_partials.linked-icon')
                                    </a>
                                </div>
                                <div class="footIcons">
                                    <a href="https://www.instagram.com/maverickdentallab/" target="_blank">
                                        @include('_partials.insta-icon')
                                    </a>
                                </div>
                                <!-- <div class="footIcons">
                                    <a href="https://www.instagram.com/maverickdentallab/" target="_blank">
                                        @include('_partials.youtube-icon')
                                    </a>
                                </div>     -->
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-3">
                            <ul>
                                <li><a href="/send-case/new-doctor/">New Doctor</a></li>
                                <li><a href="/send-case/di-submission/">Submit a Digital Case</a></li>
                                <li><a href="/send-case/local-pickup/">Request Local Pickup</a></li>
                                <li><a href="/send-case/request-supplies/">Request Supplies</a></li>
                                <li><a href="/send-case/upload-file/">Upload Case Photos</a></li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-6 col-md-3">
                            <ul>
                                <li><a href="/img/Maverick-Combined-RX.pdf" target="_blank">Rx Form</a></li>
                                <li><a href="/img/Maverick-VIP-RX.pdf" target="_blank">VIP Rx Form</a></li>
                                <li><a href="/resources/">Turnaround Calendar</a></li>
                                <li><a href="/img/cementation-guide.pdf" target="_blank">Cementation Guide</a></li>
                                <li><a href="/img/DI-Comparison-Chart.pdf" target="_blank">Digital Impression Guide</a></li>
                                <li><a href="/img/Torque-Spec-Sheet.pdf" target="_blank">Implant Torque Guide</a></li>
                                <li><a href="/img/ADA-Code-List-Product-Breakdown.pdf" target="_blank">ADA Codes </a></li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-6 col-md-3">
                            <ul>
                                <li><a href="/products/metal-based/">Metal Based </a></li>
                                <li><a href="/products/all-ceramic/">All-Ceramic</a></li>
                                <li><a href="/products/removables/">Removables </a></li>
                                <li><a href="/products/implants/">Implants</a></li>
                                <li><a href="/products/services/">Additional Services</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
            <div id="copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-12 d-flex justify-content-between">
                            <div>
                                <span>Copyright &copy; <?php echo date("Y"); ?> Maverick Dental Laboratory</span>
                            </div>
                            <div>
                                <a href="https://amgci.com/?cref=1" target="_blank"><img src="/img/DesignedBy-AMGLogo.svg" alt="Designed By AMG Creative, Inc."></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
        <script src="/assets/js/script.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script> 
        @yield('scripts')

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-141115743-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-141115743-1');
        </script>

        <!-- Browser Compatibility Check -->
        <script> 
        var $buoop = {required:{e:-0.01,i:999,f:-1,o:0,s:0,c:-1},insecure:true,unsupported:true,api:2018.09,reminder:0,reminderClosed:1,no_permanent_hide:true }; 
        function $buo_f(){ 
         var e = document.createElement("script"); 
         e.src = "//browser-update.org/update.min.js"; 
         document.body.appendChild(e);
        };
        try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
        catch(e){window.attachEvent("onload", $buo_f)}
        </script>
    </body>
</html>
